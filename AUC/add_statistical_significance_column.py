import pandas as pd
import sys
import os


workfile = sys.argv[-1]
wordir = os.path.dirname(workfile)
workfilename = os.path.splitext(os.path.basename(workfile))[0]


symulacje = os.path.join(os.path.dirname(__file__), "AUC_symulacje_full.csv")

symulacje = pd.read_csv(symulacje)

n = symulacje['n']
thr05 = symulacje['0.05']
thr01 = symulacje['0.01']
thr05 = {int(i[0]): i[1] for i in zip(n, thr05)}
thr01 = {int(i[0]): i[1] for i in zip(n, thr01)}


database = pd.read_csv(workfile)
database = database.drop(u'Unnamed: 0', 1)

thr01values = []
thr05values = []


for measure, value, n in zip(database['measure'], database['value'],database['N_avr_targets'] ):
    if measure == "AUC":
        try:
            if value > thr01[n]:
                thr01values.append(1)
            else:
                thr01values.append(0)

            if value > thr05[n]:
                thr05values.append(1)
            else:
                thr05values.append(0)

        except KeyError:
            if n>max(thr01.keys()):
                if value > thr01[max(thr01.keys())]:
                    thr01values.append(0.5)
                else:
                    thr01values.append(0)

                if n > max(thr05.keys()):
                    if value > thr05[max(thr05.keys())]:
                        thr05values.append(0.5)
                    else:
                        thr05values.append(0)
            else:

                thr01values.append(None)
                thr05values.append(None)

    else:
        thr01values.append(None)
        thr05values.append(None)

database['simulation0.05'] = thr05values
database['simulation0.01'] = thr01values

database.to_csv(os.path.join(wordir, workfilename+'_statistical_significance.csv'))