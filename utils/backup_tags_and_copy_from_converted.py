#!/usr/bin/python
# coding: utf-8
# ten skrypt wyszukuje pliki tagów i zmienia im rozszerzenie z .tag na .tag.bak
# pod warunkiem, że nie jest to katalog, który ma w nazwie "_converted"
# następnie kopiuje na ich miejsce plik z tagami z odpowiadającego katalogu "_converted"
import os,sys,shutil

for root,dirs,files in os.walk(sys.argv[1]):
    for filename in files:
        if "obci.tag" in filename and not "_converted" in root and not ".bak" in filename:
            path=os.path.join(root,filename)
            if not os.path.exists(path+".bak"):
                try: shutil.move(path,path+".bak")
                except:
                    print "PERMISION DENIED:", path
                    continue

            converted_path = os.path.join(root,"..",os.path.basename(root)+"_converted",os.path.basename(path))
            print converted_path
            print path
            print converted_path
            try: shutil.copy(converted_path,path)
            except:
                print "no converted catalog to copy from", path
                continue
