from sys import argv
import os.path
import glob
import datetime
from xml.etree import ElementTree

def get_first_sample_timestamp(f):
    RS = '{http://signalml.org/rawsignal}'
    xml_spec = ElementTree.parse(f).getroot()
    return float(xml_spec.find(RS + 'firstSampleTimestamp').text)

def get_near_tags_timestamps(realf, ts, delta=60):
    d = os.path.dirname(realf)
    fname = os.path.basename(realf)
    data = []
    tagfiles = glob.glob(d+'/data/*.tag')
    for tagfile in tagfiles:
        date = ' '.join(tagfile.split('.tag')[0].rsplit('_', 4)[1:])
        tagsdt = datetime.datetime.strptime(date, '%Y %b %d %H%M')
        tagstamp = float(tagsdt.strftime('%s'))
        if abs(tagstamp-ts)<=delta:
            data.append([tagfile, tagstamp])
    return data

def user_select(xmlname, data):
    print '\nFound nearest tagfile for xml:', xmlname, get_first_sample_timestamp(xmlname)
    for nr, d in enumerate(data):
        reld = os.path.relpath(d[0], os.path.dirname(xmlname))
        print 'NR:',nr, 'file:', reld, 'file_ts', d[1]
    selection = True
    while selection:
        try: 
            selected = int(input('select best matching (nr): '))
            selection = False
        except Exception:
            pass
    return data[selected]

def offset_tags(tagfile, firstsamplets):
    RS = '{http://signalml.org/rawsignal}'
    el_tree = ElementTree.parse(tagfile)
    root = el_tree.getroot()
    for tags in root.iter('tags'):
        for tag in tags.iter('tag'):
            print tag
            print tag.get('position')
            tag.set('position', str(float(tag.get('position'))-firstsamplets))
    return el_tree
if __name__ == '__main__':
    
    files = argv[1:]
    if files:
        for f in files:
            realf = os.path.realpath(f)
            ts = get_first_sample_timestamp(realf)
            data = get_near_tags_timestamps(realf, ts)
            tagfile, tagstamp = user_select(realf, data)
            tags_with_offset = offset_tags(tagfile, ts)
            out_tagname = os.path.basename(realf)[:-3]+'tag'
            outdir = os.path.dirname(realf)
            tags_with_offset.write(os.path.join(outdir, out_tagname), encoding = 'utf8', xml_declaration=True)
            
            
    else:
        print('usage: psychopy_tags_to_obci.py recording.obci.xml')
