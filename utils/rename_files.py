#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys
import shutil as sh



# wersja manualna (zamiana podanej części nazwy na podany tekst)
# użycie:
# ./rename_files.py old_part new_part /katalogA/katalogB/.../KATALOG/*
# czyli ostatnim argumentem jest plik lub lista plikow do zmiany
###########################################################################
#old_part = sys.argv[1]
#new_part = sys.argv[2]
#filelist = sys.argv[3:]

#if not filelist:
#    print "Provide a path to data file(s) which cache will be deleted."

#for filename in filelist:
#    if old_part in filename:
#        newname = filename.replace(old_part,new_part)
#        sh.move(filename, newname)
#         print "rename",filename,"->",newname



# wersja automatyczna (zamiana podanej części nazwy na nazwę folderu)
# użycie:
# ./rename_files.py old_part /katalogA/katalogB/.../KATALOG
# czyli ostatnim argumentem jest ścieżka do katalogu, w którym nazwy wszystkich plików zostaną zmienione
# (old_part zostanie zamienione na KATALOG)
##############################################################################

old_part = sys.argv[1]
DIR = sys.argv[2]

for root,dirs,files in os.walk(DIR):
    for filename in files:
        if old_part in filename:
            new_part = os.path.split(root)[-1]
            newname = filename.replace(old_part,new_part)
            
            filepath = os.path.join(root,filename)
            newpath = os.path.join(root,newname)
            
            sh.move(filepath, newpath)
            
            print "rename",filename,"->",newname
