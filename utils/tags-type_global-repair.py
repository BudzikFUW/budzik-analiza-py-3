#!/usr/bin/python

import os
import sys
import shutil

for root, dirs, files in os.walk(sys.argv[1]):
    for filename in files:
        if "obci.tag" in filename and "lobal" in filename and "tag.bak" not in filename and ".etr." not in filename:
            path = root + "/" + filename
            if not os.path.exists(path + ".bak2"):
                try:
                    shutil.copy(path, path + ".bak2")
                except:
                    print "PERMISION DENIED:", path
                    continue
            print path
            tag_file = open(path, 'r')
            lines = []
            for LINE in tag_file.readlines():
                LINE = LINE.replace("</tag>", "</tag>\n")
                LINE = LINE.replace("</tag>\n\n", "</tag>\n")
                LINE = LINE.replace(" />", " />\n")
                LINE = LINE.replace(" />\n\n", " />\n")
                for line in LINE.split("\n"):
                    if "global-local_1" in filename or "lobal-local1" in filename or "lobal_local_blok_1" in filename:
                        if 'name="AA.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>nontarget</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>standard</type_local>")
                        if 'name="AB.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>target</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>dewiant</type_local>")

                    if "global-local_2" in filename or "lobal-local2" in filename or "lobal_local_blok_2" in filename:
                        if 'name="BB.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>nontarget</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>standard</type_local>")
                        if 'name="BA.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>target</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>dewiant</type_local>")

                    if "global-local_3" in filename or "lobal-local3" in filename or "lobal_local_blok_3" in filename:
                        if 'name="BA.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>nontarget</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>dewiant</type_local>")
                        if 'name="BB.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>target</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>standard</type_local>")

                    if "global-local_4" in filename or "lobal-local4" in filename or "lobal_local_blok_4" in filename:
                        if 'name="AB.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>nontarget</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>dewiant</type_local>")
                        if 'name="AA.wav"' in line:
                            line = line.replace("<type_global>type_global</type_global>",
                                                "<type_global>target</type_global>")
                            line = line.replace("<type_local>type_local</type_local>",
                                                "<type_local>standard</type_local>")

                    line = line.replace("</tag>", "</tag>\n")
                    line = line.replace("</tag>\n\n", "</tag>\n")
                    line = line.replace(" />", " />\n")
                    line = line.replace(" />\n\n", " />\n")

                    lines.append(line)
            tag_file = open(path, 'w')
            tag_file.writelines(lines)

