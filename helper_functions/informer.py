import time

def informer(outdir, info_name = "!info!", file_info={}, sig_info={}, an_info={}):
    '''
    Tworzy plik !info!.txt w sciezce outdir. Plik bedzie zawieral informacje, ktore zostana wpisane do 3 powyzszych slownikow.

    file_info -> informacje o pliku (nazwa pliku raw, sciezka do pliku raw itd)
    sig_info -> informacje o sygnale z pliku raw (kanaly, probkowanie, dlugosc itd.)
    an_info -> informacje o analizie pliku (wszystkie informacje o analizie)

    Pierwotny cel: dodatkowa informacja w folderach tworzonych przez pliki *make_epofifs.py
    '''
    file = open(f"{outdir}/{info_name}.txt", "w")
    file.write("~~~~~~FILE INFO~~~~~~~~ \n\n")
    file.write("Date of analysis: ".ljust(25, " ") + time.strftime("%a, %d %b %Y %H:%M", time.localtime()) + "\n")

    for info in file_info:
        if isinstance(file_info[info], list) or isinstance(file_info[info], dict) and len(file_info[info]) > 1:
            file.write(info + "\n" + nice_pointer(file_info[info], len(info)))
            continue
        file.write(info.ljust(25, " ") + str(file_info[info]) + "\n")

    file.write("\n~~~~~~SIGNAL INFO~~~~~~ \n\n")

    for info in sorted(sig_info):
        if isinstance(sig_info[info], list) or isinstance(sig_info[info], dict) and len(sig_info[info]) > 1:
            file.write(info + "\n" + nice_pointer(sig_info[info], len(info)))
            continue
        file.write(info.ljust(25, " ") + str(sig_info[info]) + "\n")

    file.write("\n~~~~~~ANALYSE INFO~~~~~~ \n\n")

    for info in sorted(an_info):
        if isinstance(an_info[info], list) or isinstance(an_info[info], list) and len(an_info[info]) > 1:
            file.write(info + "\n" + nice_pointer(an_info[info], len(info)))
            continue
        file.write(info.ljust(25, " ") + str(an_info[info]) + "\n")

    file.close()

def nice_pointer(points, spaces=4):
    nice_points = ""

    if isinstance(points, list):
        for point in points:
            front = " "*spaces
            nice_points = nice_points + front.ljust(25, " ") + str(point) + "\n"

    if isinstance(points, dict):
        for point in points:
            front = " "*spaces
            nice_points = nice_points + front.ljust(25, " ") + point.ljust(10, " ") + str(points[point]) + "\n"

    return nice_points

