# coding:utf-8

import numpy as np
from scipy.signal import filtfilt
import connectivipy
from connectivipy.mvarmodel import Mvar, mvar_gen
import matplotlib.pyplot as plt
import numpy.random as rnd

def apply_filters(x, filters):
    result = x.copy()-np.mean(x)
    for f in filters:
        result = filtfilt(f[0], f[1], result, axis=1)
    return result


def fit_ar_to_data(data, order = 0, method = "yw", plot_dtf = False, fs = 1024):
    # method = "yw" "ns" "vm"
    if order == 0:
        order, crit = Mvar.order_akaike(data, 55, method = method)
        plt.plot(1 + np.arange(len(crit)), crit, 'g')
        plt.show()

    av, vf = Mvar.fit(data, order, method=method)

    if plot_dtf:
        dtf = connectivipy.conn.DTF()
        dtfval = dtf.calculate(av, vf, fs)
        connectivipy.plot_conn(dtfval, 'DTF values', fs)

    return av


def generate_fake_eeg_using_ar(av, length = 1024):
    order = av.shape[1]
    return mvar_gen(av, length, omit = 500*order)


def cut_eeg_by_tags(eeg, tags, tag_len):
    epochs = []
    for tag in tags:
        epochs.append(eeg[:, tag:tag+tag_len])
    return np.array(epochs)


def generate_fake_tags(tag_len, targets_no, nontargets_no, fs):
    target_tags = []
    nontarget_tags = []
    
    tag_type = "target"
    tag = 10  # s
    tag_sep = (.5, 1)  # s
    for _ in range(targets_no + nontargets_no):
        if len(target_tags) == targets_no:
            tag_type = "nontarget"
        elif len(nontarget_tags) == nontargets_no:
            tag_type = "target"
        elif tag_type == "target":
            tag_type = "nontarget"
        elif rnd.random() < 0.25:
            tag_type = "target"
        else:
            tag_type = "nontarget"
        
        tag += tag_len + rnd.uniform(*tag_sep)
        
        if tag_type == "target":
            target_tags.append(int(tag * fs))
        else:
            nontarget_tags.append(int(tag * fs))

    return target_tags, nontarget_tags
