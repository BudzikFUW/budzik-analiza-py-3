#-*- coding: utf-8 -*-
# © Marian Dovgialo
from __future__ import print_function
from lxml import etree
import os
from matplotlib import rc
from matplotlib import rcParams
import scipy.integrate as si
import numpy as np
import pylab as pb
import pprint
import scipy.signal as ss
import scipy.stats as st
import time
import scipy, random, sys
import sys, os
from matplotlib.mlab import specgram, psd
from mpl_toolkits.mplot3d.axes3d import Axes3D
from book_importer import read_book, reconstruct_signal
import matplotlib
import math

matplotlib.use('GTK3Cairo')

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 8}

matplotlib.rc('font', **font)

NATOMS = 12


#~ pb.rc('text', usetex=True)
#pb.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#~ rcParams['text.latex.unicode']=True
#~ rcParams['text.latex.preamble']='\usepackage[OT4]{fontenc}'

def perio(s,w,Fs):
   # print np.sum(w**2)/Fs # debug symbols
    fft=np.fft.fft(s*w)
    fr=np.fft.fftfreq(len(fft),1./Fs)
    fft=fft.conj()*fft
    fft=fft.real/Fs/np.sum(w**2) # normalizacja przez energie okienka
    return np.fft.fftshift(fr),np.fft.fftshift(fft)

def gabor(epoch_s,Fs,amplitude,position,scale,afrequency,phase):
    time = np.linspace(0, epoch_s/Fs, epoch_s)
   # time = np.arange(0, epoch_s)
    
    width = scale
    frequency = afrequency*2*np.pi
    #frequency = afrequency
    #print('position, width, frequency, amplitude, phase', position, width, frequency, amplitude, phase)
    signal = amplitude*np.exp(-np.pi*((time-position)/width)**2)*np.sin(frequency*(time-position) + phase)
    return signal
    

class Eeg(object):
    def load_xml_data(self, pathtofile):
        tree = etree.parse(pathtofile)
        root = tree.getroot()
        channelLabels = []
        channelGain = []
        channelOffset = []
        prfx='{http://signalml.org/rawsignal}'
        for i in root.iterchildren():
            if i.tag == prfx+'samplingFrequency':
                Fs = float(i.text)
            if i.tag == prfx+'channelCount':
                chan_cnt = float(i.text)
            if i.tag == prfx+'sampleCount':
                sample_cnt = float(i.text)
            if i.tag == prfx+'channelLabels':
                for label in i:
                    channelLabels.append(label.text.strip())
            if i.tag == prfx+'calibrationGain':
                for label in i:
                    channelGain.append(float(label.text.strip()))
            if i.tag == prfx+'calibrationOffset':
                for label in i:
                    channelOffset.append(float(label.text.strip()))
            if i.tag == prfx+'firstSampleTimestamp':
                t_0 = float(i.text)
        #print ('load_xml_data')
        return Fs, chan_cnt, sample_cnt, channelLabels, t_0, channelGain
    
    def __init__(self, plik, resample_to_Fs = None):
        if os.path.splitext(plik)[1] == '.raw':
            plik = plik[:-4]
        (self.Fs, self.chan_cnt, self.sample_cnt, 
        self.channelLabels, self.t_0, self.channelGain) = self.load_xml_data(plik+'.xml')
        self.data = np.reshape(np.fromfile(plik+'.raw', dtype='float32')[:self.sample_cnt*self.chan_cnt], (-1, int(self.chan_cnt)))
        
        if resample_to_Fs:
            #self.sample_cnt = self.sample_cnt*resample_to_Fs/self.Fs
            print('resampled length:', self.sample_cnt)
            decimator = int(self.Fs/resample_to_Fs)
            self.sample_cnt = len(ss.decimate(self.data[:,0], decimator))
            self.Fs = resample_to_Fs
            try:
                resampled_filename = plik+'resampled_to_{}_Hz'.format(resample_to_Fs)+'.raw'
                print('loading preresampled')
                self.data = np.reshape(np.fromfile(resampled_filename, dtype='float32')[:self.sample_cnt*self.chan_cnt], (-1, int(self.chan_cnt)))
            
            except IOError:
                print('loading failed, resampling')
                data_resampled = np.zeros((self.sample_cnt, int(self.chan_cnt)))
                for i in range(len(self.channelLabels)):
                    print('resampling chnl {}'.format(i))
                    #~ data_resampled[:,i] = ss.resample(self.data[:,i], self.sample_cnt)
                    data_resampled[:,i] = ss.decimate(self.data[:,i], decimator)
                self.data = data_resampled
                self.data.astype(np.float32).tofile(resampled_filename)
        self.t = np.arange(0, self.sample_cnt/self.Fs, 1./self.Fs)
        
        self.filename = plik
        #~ self.m1020 = [None,  0 , None,  1 , None ,
                        #~ 2 ,  3 ,  4 ,  5 ,  6  ,
                      #~ 7 , 8 , 9 , 10 , 11  ,
                      #~ 12 , 13 , 14 , 15 , 16  ,
                     #~ None, 17 , None,  18 , None ]
                     
        self.m1020 = [None, None, 'Fp1', 'Fpz', 'Fp2', None,None,
                      None, None, None, 'AFz', None,   None,None,
                      None, 'F7', 'F3', 'Fz', 'F4',    'F8',None,
                        None, None, None, 'FCz', None, None,None,
                        'A1', 'T3', 'C3', 'Cz', 'C4', 'T4', 'A2',
                        None, 'T5', 'P3', 'Pz', 'P4', 'T6',None,
                        None, None, 'O1', 'Oz', 'O2', None, None,]
        self.m1020l = 7

                    # macierz topograficzna
        _asciify_pl_tab = (u'ĄĆĘŁŃÓŚŻŹąćęłńóśżź ', u'ACELNOSZZacelnoszz_')
        self.asciify_pl_tab = dict()
        for a, b in zip(*_asciify_pl_tab):
            self.asciify_pl_tab[ord(a)] = b
            
    def _get_chnl_ind(self, chnls):
        chnls1=chnls[::]
        for ind, chnl in enumerate(chnls):
            if chnl in self.channelLabels:
                chnls1[ind] = self.channelLabels.index(chnl)
            elif type(chnl) == type('a'):
                raise Exception('próbujesz obejrzeć nieistniejący kanał '+ chnl)
                
        return chnls1
        
    def _get_chnl_labels(self, chnls):
        chnls1=chnls[::]
        for ind, chnl in enumerate(chnls):
            if chnl in self.channelLabels:
                pass
            elif chnl == None:
                chnls1[ind] = None
            else:
                chnls1[ind] = self.channelLabels[chnl]
        return chnls1
        
  
    def filtrowanie(self, sygnal, filtr='filterek',low=0.01, typ='highpass', rzad=1, drawchar=False):
        if filtr == 'butter':
            print('filtr default')
            (b,a) = ss.butter(rzad, low/(self.Fs/2), btype=typ)

        elif filtr == '130':
            print('filtr 130')
            wp = [1/self.Fs*2, 30./self.Fs*2]
            ws = [0.01/self.Fs*2, 32./self.Fs*2]
            gpass = 5
            gstop = 20
            [n,Wn]=ss.cheb1ord(wp, ws, gpass, gstop, analog=0);
            [b,a]=ss.cheby1(n, gpass, Wn, btype='bandpass', analog=0, output='ba')

        elif filtr == 'filterek':
            print('filtr 0.1 - 20 Hz chebby')
            wp = [0.015/(self.Fs/2), 19./(self.Fs/2)]
            ws = [0.01/(self.Fs/2), 20./(self.Fs/2)]
            gpass = 1
            gstop = 5
            [n,Wn]=ss.cheb1ord(wp, ws, gpass, gstop, analog=0);
            Wn = [0.1/(self.Fs/2), 20./(self.Fs/2)]
            rzad = 3
            [b,a]=ss.cheby1(rzad, 1, Wn, btype='bandpass', analog=0, output='ba')

            
        else:
            raise Exception('nie znam takiego filtra '+filtr)
        #s_f =ss.lfilter(b,a,sygnal)
        #~ f_bs_ch=50.
        #~ Fn=self.Fs/2.
        #~ rz=1
        #~ 
        #~ b,a = ss.butter(1, 0.1/256, btype='high')
        #typ = ss.lfilter
        typ = ss.filtfilt
        s_f = typ(b,a, sygnal)
        
        ## charakterystyka #######################
        if drawchar:
            l = 12800
            imp = np.zeros(l)
            imp[l/2] = 1
            fig = pb.figure(999)
            ax1 = fig.add_subplot(221)
            ax3 = fig.add_subplot(222)
            
            ax4 = fig.add_subplot(224)
            ax2 = fig.add_subplot(223)
            odp = typ(b, a, imp)
            ax1.plot(odp, label=str(len(b)))
            ax1.set_title(' filtrowany impuls')
            ax3.plot(odp, label=str(len(b)))
            ax3.set_title(' filtrowany impuls')
            
            f_imp, fft_imp = perio(imp, np.ones(len(imp)), Fs=self.Fs)
            f, fft = perio(odp, np.ones(len(odp)), Fs=self.Fs)
            ax1.set_xlabel(u'nr próbki')
            ax1.set_ylabel(u'wartosc funkcji')
            #ax2.plot(f[len(f)/2:], fft[len(f)/2:])#20*np.log10(fft[len(f)/2:]), label=str(len(b)))
            ax2.plot(f_imp[len(f)/2:], 20*np.log10(fft[len(f)/2:]/fft_imp[len(f)/2:]))#20*np.log10(fft[len(f)/2:]), label=str(len(b)))
            ax2.set_xlabel(u'częstość')
            ax2.set_ylabel(u'moc, dB')
            ax4.plot(f_imp[len(f)/2:], 20*np.log10(fft[len(f)/2:]/fft_imp[len(f)/2:]))#20*np.log10(fft[len(f)/2:]), label=str(len(b)))
            ax4.set_xlabel(u'częstość')
            ax4.set_ylabel(u'moc, dB')
            ax4.grid(True)
            #~ pb.show()
        ###3#######################
        #~ 
        #~ b,a = ss.cheby2(rz, 10., [(f_bs_ch-0.1)/Fn,(f_bs_ch+0.1)/Fn], btype='bandstop', analog=False, output='ba')
        #~ s_f =ss.lfilter(b,a,s_f)
        #~ b,a = ss.butter(1, 20./256, btype='lowpass')
        #~ s_f =ss.lfilter(b,a,s_f)
        return s_f
    
        
    def showfft(self, s, nazwa, nazwa_art, rozmiar=(10,10)):
        ''' musimy podac sygnal dla ktorego ma byc rysowana transformata,
         nazwe kanalu dla ktorego ma byc robiony wykres-> dodawane
          do nazwy wykresu, oraz ewentualnie rozmiar obrazka'''
        fft_fig = pb.figure(figsize=rozmiar)
        pb.title(u'Widmo mocy sygnału z kanału {0} dla artefaktu: {1}.'.format(nazwa, nazwa_art))
        pb.xlabel(u'Częstość, [Hz]')
        pb.ylabel('Moc [uV/Hz]')
        T=len(s)/self.Fs
        freq=np.concatenate((np.arange(0, self.Fs/2., 1./T ),np.arange(-self.Fs/2.,0, 1./T ))) #generacja osi czestosci
        w_moc=(np.fft.fft(s)*np.fft.fft(s).conj()/len(s)).real #normalizacja kazdej przez energie kwadratowego okna
        #print(len(freq), len(w_moc))
        pb.plot(freq[:len(freq)/2], w_moc[:len(freq)/2])
        #print ('showfft')
        return fft_fig
    
        
    def montaz(self, chnls, fftchnls = [], ref='avr', slice_l = 0, 
                slice_h = -1, filt=False, diody=False, norm_syg=False, nazwa_wykresu='',
                 savefigs=False, draw=False, resample_to_Fs=None):
                    
                    
        chnls = self._get_chnl_ind(chnls)

            
        print(chnls)
                
        fftchnls = self._get_chnl_ind(fftchnls)
        if ref=='avr':
            ref = np.dot(np.array(self.channelGain, ndmin=2).T, np.ones((1, self.sample_cnt))).T*self.data

            
            ref = np.mean(ref[:,:-1], axis=1) # -1 bo kanal SAW
            print ('len ref', len(ref))
            
        elif ref =='linkedEars':
            ref_chnl = self._get_chnl_ind(['A1', 'A2'])
            #~ print(ref_chnl)
            #~ sys.exit()
            ref = np.dot(np.array(self.channelGain, ndmin=2).T, np.ones((1, self.sample_cnt))).T*self.data
            ref = np.mean(ref[:,ref_chnl], axis=1)
        elif ref =='raw':
            ref = 0
        else:
            ref_chnl = self._get_chnl_ind(ref)
            print(ref_chnl)
            ref = np.dot(np.array(self.channelGain, ndmin=2).T, np.ones((1, self.sample_cnt))).T*self.data
            ref = np.mean(ref[:,ref_chnl], axis=1)
        #ref = (self.channelGain[self.channelLabels.index('Ref')]*self.data[:, self.channelLabels.index('Ref')])
        #ref = (self.channelGain[self.channelLabels.index('Cz')]*self.data[:, self.channelLabels.index('Cz')])
        
        if draw:
            syg = pb.figure(figsize=(15,10))
            ax = pb.subplot(1,1,1)
            pb.title(u'{}'.format(nazwa_wykresu))
            pb.xlabel('Time, [s]')
        plots = []
        chnldatas = []
        for nr, chnl in enumerate(chnls):
            #~ if resample_to_Fs:
                #~ decimator = 
                #~ chnldata = ss.decimateself.channelGain[chnl]*self.data[:, chnl]-ref
            chnldata = self.channelGain[chnl]*self.data[:, chnl]-ref
            if filt == True:
                print('zastosowano filtr')
                chnldata=self.filtrowanie(chnldata)
            elif filt == False:
                print('no filtr')
                pass
            else:
                print('filtr present')
                chnldata=self.filtrowanie(chnldata, filtr=filt)
                # zakładamy,że elementy listy napisane są w nastepującej kolejności: cz_pocz,cz_kon,rzad,type
            chnldata = chnldata[slice_l:slice_h]
            if norm_syg:
                chnldata = chnldata/np.std(chnldata)
            if draw:#tu zakladamy, że rysujemy cały sygna, lub jego wycinki
                ax.plot(self.t[slice_l:slice_h], 
                        chnldata, label=str(self.channelLabels[chnl]))
            if chnl in fftchnls:
                sygfft = self.showfft(chnldata, str(self.channelLabels[chnl]),nazwa_artefaktu)
                if savefigs:
                    sygfft.savefig(u'widmo_{0}_kanal{1}.png'.format(nazwa_wykresu,str(channelLabels[chnl])).translate(self.asciify_pl_tab))
            chnldatas.append(chnldata)
        if diody:
            diody = ["Dioda1","Dioda2"]
            dchanel = [20,21]
            for di, dch in zip(diody, dchanel):
                chnldata = (self.channelGain[self.channelLabels.index(di)]*self.data[:, self.channelLabels.index(di)][slice_l:slice_h])
                nr += 1
                if filt:
                    chnldata=self.filtrowanie(chnldata)
                ax.plot(self.t[slice_l:slice_h], 
                    chnldata/10000-nr*150, label=str(self.channelLabels[dch]))
                
            
        if draw:
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles, labels)
            ax.plot((self.t[slice_l]+0.5, self.t[slice_l]+0.5) ,(100,0), linewidth=4, color = 'k')
            ax.annotate('100uV', xy = (self.t[slice_l]+0.5, 0), xycoords='data', horizontalalignment='right')
            
            ax.axes.get_yaxis().set_visible(False)
        if savefigs:
            syg.savefig(u'{}.png'.format(nazwa_wykresu).translate(self.asciify_pl_tab))
        return np.array(chnldatas)
    
 
        
    #def obserwacja_blyskajacej_diody(self, chnl, slice_l, slice_h,
                    #before_event, after_event, events, filt=False,
                    #nazwa_wykresu='',
                    #jump = 0.030):
        #''' podajemy prócz standardowych danych dla montazu, podajemy
         #w próbkach górny i dolny zakres przedziału dla pojedynczego
        #błysku, event channels - wektor sygnału ze zdarzeniami, unormowany
        #o długości, jump w sekundach, wszystko inne w probkach'''
        
        ##~ events = self.montaz(event_chnls, fftchnls = [], slice_l = slice_l, 
                ##~ slice_h = slice_h, filt=False, diody=False, norm_syg=False,
                 ##~ savefigs=False, draw=False)

        #sample = slice_l #próbka która aktualnie w while będziemy sprawdzać
        ##~ events = [i/np.max(i) for i in events]#diody[0]/np.max(diody[0])
        #syg = self.montaz(chnl, fftchnls = [], slice_l=slice_l,
                 #slice_h=slice_h , filt=filt, diody=False, norm_syg=False,
                 #savefigs=False, draw=False)
                 
        ## DEMONSTRACJA DZIALANIA FILTROW **********8
        ##~ pb.figure()
        ##~ for nr, i in enumerate(syg):
            ##~ 
            ##~ pb.subplot(len(syg),1,nr+1)
            ##~ pb.plot(i)
        ##********************************************
        ##pb.show()
        ##sample-before_event sample+after_event
        #cuts = []
        
        #while sample<len(events)-jump*self.Fs:
            #if events[sample]>0.5:
                ##print(sample, len(events))
                #cut = []
                #for i in syg:
                    #cut.append(i[sample-before_event:sample+after_event])
                ##~ cut = [i[sample-before_event:sample+after_event] for i in syg]
                
                #for ch in cut:
               
                    #if np.max(np.abs(ch)) > 75: # limit sygnalu
                        #dirty = True
                        ##print('dirty')
                        ##print(ch)
                        #break
                    #dirty = False
                #if not dirty:
                    #cuts.append(cut)
                #else:
                    #cuts.append([np.ones((before_event+after_event))*np.nan]*len(chnl))
                        
                        
                #sample += int(self.Fs*jump)# poniewż odstęþy pomiędzy kolejnymi blyskami sa 0,020 s
            #else:
                #sample += 1
        #cuts_number = len(cuts)
        
        #return cuts, chnl
        
    def cuts_from_tags(self, chnls,
                    before_event, after_event, type='correct', filt=False,
                    ref = 'avr', nazwa_wykresu='',
                    jump = 0.030, noreject=False, trsh = 75, point='pos', draw=False):
        ''' podajemy prócz standardowych danych dla montazu, podajemy
         w próbkach górny i dolny zakres przedziału dla pojedynczego
        błysku, event channels - wektor sygnału ze zdarzeniami, unormowany
        o długości, jump w sekundach, wszystko inne w probkach'''
        
        tagfile = self.filename[:-5]+'.psychopy.tag'
        syg = self.montaz(chnls, fftchnls = [], slice_l=0,
                 slice_h=-1 , filt=filt, diody=False, norm_syg=False,
                 savefigs=False, draw=False, ref=ref)
        tree = etree.parse(tagfile)
        root = tree.getroot()
        tagData = root.find('tagData')

#~ 
        #~ # DEMONSTRACJA SYGNAŁU który kroimy**********8
        if draw:
            pb.figure()
            for nr, i in enumerate(syg):
                
                pb.subplot(len(syg),1,nr+1)
                pb.plot(i)
                pb.title(self._get_chnl_labels([chnls[nr],])[0])
                
        #********************************************
        tags = tagData.find('tags')
        cuts = []
        for tag in tags:
            corr = tag.find('correct').text
            buttons = tag.find('buttons').text
            reaction_t = float(eval(tag.find('reaction_time').text)[0])#
            pos = float(tag.get('position'))
            
            if point=='pos':
                sample = int((pos)*self.Fs) # point of cut
            elif point=='pos-reaction':
                sample = int((pos-reaction_t)*self.Fs) # point of cut
            elif point=='pos+reaction':
                sample = int((pos+reaction_t)*self.Fs) # point of cut
            
            if sample-before_event<1 or sample+after_event>len(syg[0,:]):
                print('Zignorowano eventy, których granica wycinania jest poza sygnałem, (może zbyt szeroki timescale?)')
                print (sample-before_event, sample+after_event, len(syg[0,:]))
                continue
            cut = syg[:, sample-before_event:sample+after_event]
            mean = np.dot(np.mean(cut, axis=1)[:,None], np.ones((1,cut.shape[1])), )
            
            if noreject or np.max(np.abs(cut-mean))<trsh: 
                #print(corr, buttons)
                if type == 'correct' and (corr in buttons) :
                    #print('correct')
                    cuts.append(cut)
                elif type =='incorrect' and (corr not in buttons):
                    #print('incorrect')
                    cuts.append(cut)
            else:
                print(np.max(np.abs(cut-np.mean(cut))))
                print('rejection')
        return cuts, chnls
        
        
    def cuts_from_textfile(self, chnls,
                    before_event, after_event, type='correct', filt=False,
                    ref = 'avr', nazwa_wykresu='',
                    jump = 0.030, noreject=False, trsh = 75, point='pos', draw=False):
        ''' podajemy prócz standardowych danych dla montazu, podajemy
         w próbkach górny i dolny zakres przedziału dla pojedynczego
        błysku, event channels - wektor sygnału ze zdarzeniami, unormowany
        o długości, jump w sekundach, wszystko inne w probkach'''
        
        tagfile = self.filename+'.trg'
        syg = self.montaz(chnls, fftchnls = [], slice_l=0,
                 slice_h=-1 , filt=filt, diody=False, norm_syg=False,
                 savefigs=False, draw=False, ref=ref)
        

#~ 
        #~ # DEMONSTRACJA SYGNAŁU który kroimy**********8
        if draw:
            pb.figure()
            for nr, i in enumerate(syg):
                
                pb.subplot(len(syg),1,nr+1)
                pb.plot(i)
                pb.title(self._get_chnl_labels([chnls[nr],])[0])
            #~ pb.show()
        #********************************************
        tags = np.loadtxt(tagfile)
        cuts = []
        for tag in tags:
            
            corr = tag[1]
            
            pos = tag[0]-self.t_0-2
            
            if point=='pos':
                sample = int((pos)*self.Fs) # point of cut
                
            elif point=='pos-reaction':
                sample = int((pos-reaction_t)*self.Fs) # point of cut
            elif point=='pos+reaction':
                sample = int((pos+reaction_t)*self.Fs) # point of cut
            
            
            cut = syg[:, sample-before_event:sample+after_event]
            
            mean = np.dot(np.mean(cut, axis=1)[:,None], np.ones((1,cut.shape[1])), )
            
            if noreject or np.max(np.abs(cut-mean))<trsh: 
                if type == 'correct' and (corr >0.1):
                    print('corrie')
                    print('corrie')
                    cuts.append(cut)
                elif type =='incorrect' and (corr <0.1):
                    print('incorrie')
                    cuts.append(cut)
            else:
                print(np.max(np.abs(cut-np.mean(cut))))
                print('rejection')
        return cuts, chnls
        
        
    def cuts_from_textfile_audio(self, chnls,
                    before_event, after_event, type='correct', filt=False,
                    ref = 'avr', nazwa_wykresu='',
                    jump = 0.030, noreject=False, trsh = 75, point='pos', tagchnl = 'Trigger', draw=False):
        ''' podajemy prócz standardowych danych dla montazu, podajemy
         w próbkach górny i dolny zakres przedziału dla pojedynczego
        błysku, event channels - wektor sygnału ze zdarzeniami, unormowany
        o długości, jump w sekundach, wszystko inne w probkach'''
        try:
            dataFileName = os.path.basename(self.filename)
            cut_cache_path = self.filename+'_cut_cache/'
            cut_filename = cut_cache_path+'cuts_hash_'+str(hash('cuts_be_{}_ae_t_{}_filt_{}_ref_{}_noreject_{}_trsh_{}_type_{}_chnls_{}_fs_{}'.format(
                before_event, after_event, filt, ref, noreject, trsh, type, chnls, self.Fs)))+'.npy'
            print("loading cached cuts", cut_filename)
            cuts = np.load(cut_filename)
        except IOError:
            print('failed, recalculating')
            tagfile = self.filename+'.trg'
            syg = self.montaz(chnls, fftchnls = [], slice_l=0,
                     slice_h=-1 , filt=filt, diody=False, norm_syg=False,
                     savefigs=False, draw=False, ref=ref)
    
     
            
            eventsig = self.data[:, self._get_chnl_ind([tagchnl,])[0]]**2
            #~ print (eventsig.max())
            if draw:
                pb.figure()
                pb.plot(eventsig[range(0,len(eventsig),4)])
            #~ pb.show()
            #~ sys.exit()
    
    #~ 
            #~ # DEMONSTRACJA SYGNAŁU który kroimy**********8
            if draw:
                pb.figure()
                for nr, i in enumerate(syg):
                    
                    pb.subplot(len(syg),1,nr+1)
                    pb.plot(i)
                    pb.title(self._get_chnl_labels([chnls[nr],])[0])
                    #~ pb.show()
            #~ #********************************************
            tags = np.loadtxt(tagfile)
            cuts = []
            
            try:
                tagsamples = np.load(tagfile+'_fs_{}_samples.npy'.format(self.Fs))
                print('found additional tagfile with precision samples: {}'.format(tagfile+'samples.npy'))
            except IOError:
                print('creating additional tagfile with precision samples: {}'.format(tagfile+'samples.npy'))
                sample = 0
                tagnr = 0
                jump = self.Fs*0.7
                event_thr = np.var(self.data[:, self._get_chnl_ind([tagchnl,])[0]])*3#400000#0.5e11
                tagsamples = []
                while sample<len(eventsig)-1-jump:
                    if eventsig[sample]>event_thr:
                        #print(sample, eventsig[sample],tagnr, 'sample')
                        corr = tags[tagnr][1]
                        tagsamples.append(sample)
                        sample = sample+jump
                        tagnr = tagnr+1
                    sample+=1
                np.save(tagfile+'samples.npy', np.array(tagsamples))
                 
            for tagnr, sample in enumerate(tagsamples):
                corr = tags[tagnr][1]
                    
                    
                cut = syg[:, sample-before_event:sample+after_event]
                mean = np.dot(np.mean(cut, axis=1)[:,None], np.ones((1,cut.shape[1])), )
                if noreject or np.max(np.abs(cut-mean))<trsh: 
                    if type == 'correct' and (corr >0.1):
                        print('corrie')
                        print('corrie')
                        cuts.append(cut)
                    elif type =='incorrect' and (corr <0.1):
                        print('incorrie')
                        cuts.append(cut)
                else:
                    print(np.max(np.abs(cut-np.mean(cut))))
                    print('rejection')
                    
                    
            print('saving cuts cache', cut_filename)
            #~ sys.exit()
            if not os.path.exists(cut_cache_path):
                os.makedirs(cut_cache_path)
            np.save(cut_filename, cuts)
            
            
        return cuts, chnls
    def cuts_rm_baseline(self, cuts, chnls_n, timescale, baseline='auto'):
        '''remove baseline from cuts, baseline - list of 2 times in seconds
    to use as baseline, timescalse - list of starting point of timescale
     and endpoint, in seconds'''
        
        print('removing baseline')
        #print('timescale', timescale[0], timescale[1])
        t = np.linspace(timescale[0], timescale[1], cuts[0][0].shape[0])
        t0 = np.argmin(t**2)
        #print(t0, np.abs(t))
        if baseline=='auto':
            basel = 0
            baseh= np.abs(t).argmin()
        elif baseline==None:
            pass
        else:
            basel = int(baseline[0]*self.Fs+t0)
            baseh = int(baseline[1]*self.Fs+t0)
       # print(timescale, cuts[0][0].shape[0], t0)
       # print('baseline', basel, baseh, t[basel], t[baseh])
        #~ cuts_bs1 = cut[n,:]-np.mean(cut[n,:][basel:baseh])
        cuts_bs = []
        for cut in cuts:
            chnl_bs = []
            for n, i in enumerate(chnls_n):
                chnl_bs.append(cut[n,:]-np.mean(cut[n,:][basel:baseh]))
            cuts_bs.append(np.array(chnl_bs))
        #~ for n, i in enumerate(chnls_n):
            #~ chnl_bs = []
            #~ for cut in cuts:
                #~ 
                #~ #pb.plot(t, cut[n]-np.mean(cut[n][:baselinemax]))
                #~ if baseline:
                    #~ chnl_bs.append(cut[n,:]-np.mean(cut[n,:][basel:baseh]))
                #~ print(len(chnl_bs))
            #~ cuts_bs.append(np.array(chnl_bs).T)
        return np.array(cuts_bs), chnls_n
    def cuts_welchs_t_test(self, cutsa, cutsb, chnls_n, timescale, draw=False):
        '''timescalse - list of starting point of timescale
     and endpoint, in seconds. cuts, must have the same channels
     quantity and order and be of the same length. Returns t_test score 
     and probability for the null hypothesis that 2 independent cuts 
     have identical 
      average (expected) values, for multichannel and timescale'''
        print('welch_t_test')
        timescale = np.linspace(timescale[0], timescale[1], cutsa[0][0].shape[0])
        t0 = np.abs(timescale).argmin()
        cutsa_arr = np.array(cutsa)

        print(cutsa_arr.shape)
        
        cutsb_arr = np.array(cutsb)
        if draw:
            figpear = pb.figure(2000)
            for nr, channel in enumerate(chnls_n):
                k2, p, = st.mstats.normaltest(cutsa_arr[:,nr,:], axis = 0)
                print(p.shape)
                
                
                axpear = figpear.add_subplot(len(chnls_n), 1, nr+1)
                axpear.set_title(u'normality test D’Agostino and Pearson’s, corr')
                axpear.plot(timescale, p)
                axpear.set_ylim([0,1])
            
            fig_shapiro = pb.figure(2003)
            fig_hist = pb.figure(2004)
            for nr, channel in enumerate(chnls_n):
                p_shapiro = np.zeros(cutsa_arr.shape[2])
                for i in xrange(cutsa_arr.shape[2]):
                    W, psh, = st.shapiro(cutsa_arr[:,nr,i])
                    p_shapiro[i] = psh
                print(p.shape)
                print('shapiro', nr, np.median(p_shapiro), np.mean(p_shapiro))
                
                axshap = fig_shapiro.add_subplot(len(chnls_n), 1, nr+1)
                axhist = fig_hist.add_subplot(len(chnls_n), 1, nr+1)
                axhist.hist(cutsa_arr[:,nr,int(1.87*self.Fs+t0)].flatten(), bins = 25)
                axshap.set_title(u'normality testShapiro, corr')
                axshap.plot(timescale, p_shapiro)
                axshap.set_ylim([0,1])
            fig_shapiro.tight_layout(pad=2)
        #sys.exit()
        t_score, p, = st.ttest_ind(cutsa_arr, cutsb_arr, axis=0, equal_var=False)
        
        return t_score, p, timescale
        
    def usrednianie(self, cuts, chnls_n, draw=False, t=[-0.300, 0.600], baseline=None, y_lims='auto', print_q=False, name='', newfig=True, n2_p2_stamp=False,
                    electrode_matrix=False, color='black', point = '',):
        '''t - lista dwuelementowa z czasem w sekundach przed zdarzeniem i po,
        wycinanym przed, baseline-wektor dwóch czasów w sekundach - dla baseline'u '''
        avr_cuts = []
        avr_stds = []
        n2_p2s = []
        p2amp = []
        T = np.linspace(t[0], t[1], cuts[0][0].shape[0])
        t0 = np.abs(T).argmin()
        if baseline=='auto':
            basel = 0
            baseh= np.abs(T).argmin()
        elif baseline==None:
            pass
        else:
            basel = int(baseline[0]*self.Fs+t0)
            baseh = int(baseline[1]*self.Fs+t0)
        #~ print('baseline', basel, baseh)
        for n, i in enumerate(chnls_n):
            avr = []
            for cut in cuts:
                
                #pb.plot(t, cut[n]-np.mean(cut[n][:baselinemax]))
                if baseline:
                    avr.append(cut[n,:]-np.mean(cut[n,:][basel:baseh]))
                else:
                    avr.append(cut[n,:])
            #~ a=scipy.stats.stats.nanmean(np.array(avr), axis=0)
            #~ std = scipy.stats.nanstd(np.array(avr), axis=0)
            a=np.mean(np.array(avr), axis=0)
            std = np.std(np.array(avr), axis=0)
            #n2_p2s.append(self.finding_n2_p2(a,t, draw=False))
            #p2amp.append(n2_p2s[-1][1]-n2_p2s[-1][0])
            #~ print ('len cuts for std', len(cuts))
            avr_stds.append(std/np.sqrt(len(cuts)))
            #print(np.array(avr_stds).shape)
            avr_cuts.append(a)

        
        
        if draw:
            if newfig==True:
                if print_q:
                    fig = pb.figure(figsize=(10, 15), dpi=300)
                else:
                    fig = pb.figure()
            else:
                fig = newfig
            chnls_n = self._get_chnl_labels(chnls_n)
            for nr, acut in enumerate (avr_cuts):
                if not electrode_matrix:  
                    ax = fig.add_subplot(len(avr_cuts), 1, nr+1)
                else:
                    pos = self.m1020.index(self._get_chnl_labels(chnls_n)[nr])+1
                    ax = fig.add_subplot(len(self.m1020)/self.m1020l, self.m1020l, pos)
                    if pos == 11:
                        ax.set_ylabel(u'Napięcie, μV]')
                        
                        #### rysowanie w macierzy topograficznej
                
                #print(len(t), 'acut', len(acut))
                #na =str(name)[9:15]
                na = name
                #print(na)
                #na = 'test_test'

                ax.plot(t, acut, color=color, label = na)
                ax.plot(t, acut+avr_stds[nr], '-.', linewidth=0.5, color=color,label = na+'_std')
                ax.plot(t, acut-avr_stds[nr], '-.', linewidth=0.5, color=color,label = na+'_std')
                ax.grid(True)
                handles, labels = ax.get_legend_handles_labels()
                if nr==0:
                    fig.legend(handles, labels, loc=2)
                #~ if n2_p2_stamp:
                    #~ n2,p2,id_n2,id_p2=n2_p2s[nr]
                    #~ ax.plot([(id_n2-1)/self.Fs+t[0], (id_n2-1)/self.Fs+t[0]], [0,n2])
                    #~ ax.plot([(id_p2-1)/self.Fs+t[0], (id_p2-1)/self.Fs+t[0]], [0,p2])
                ax.set_title(chnls_n[nr])
                if y_lims == 'auto':
                    maxl = np.amax(avr_cuts)
                    minl = np.amin(avr_cuts)
                    ax.set_ylim([minl, maxl]) #limity
                else:
                    ax.set_ylim(y_lims)
                if nr+1 == len(avr_cuts):
                    if not electrode_matrix:
                        ax.set_ylabel(u'Napięcie, μV]')
                    ax.set_xlabel('Czas, [s]')
                    
            if print_q:
                fig.tight_layout(pad=1)
                fig.savefig('r_'+str(ref)+'_p_{}_'.format(point)+os.path.split(self.filename)[1]+'.png', dpi=150)
                #pb.close(fig)
        return avr_cuts, t, p2amp
        
    def cuts_avr(self, cuts, chnls_n, draw=False, t=[-0.300, 0.600], baseline=None):#, y_lims='auto', print_q=False, name='', newfig=True, n2_p2_stamp=False,
                    #electrode_matrix=False, color='black', point = '',):
        '''t - lista dwuelementowa z czasem w sekundach przed zdarzeniem i po,
        wycinanym przed, baseline-wektor dwóch czasów w sekundach - dla baseline'u '''
        avr_cuts = []
        avr_stds = []
        n2_p2s = []
        p2amp = []
        T = np.linspace(t[0], t[1], cuts[0][0].shape[0])
        t0 = np.abs(T).argmin()
        if baseline=='auto':
            basel = 0
            baseh= np.abs(T).argmin()
        elif baseline==None:
            pass
        else:
            basel = int(baseline[0]*self.Fs+t0)
            baseh = int(baseline[1]*self.Fs+t0)
        #~ print('baseline', basel, baseh)
        for n, i in enumerate(chnls_n):
            avr = []
            for cut in cuts:
                
                #pb.plot(t, cut[n]-np.mean(cut[n][:baselinemax]))
                if baseline:
                    avr.append(cut[n,:]-np.mean(cut[n,:][basel:baseh]))
                avr.append(cut[n,:])
            #~ a=scipy.stats.stats.nanmean(np.array(avr), axis=0)
            #~ std = scipy.stats.nanstd(np.array(avr), axis=0)
            a=np.mean(np.array(avr), axis=0)
            std = np.std(np.array(avr), axis=0)
            #n2_p2s.append(self.finding_n2_p2(a,t, draw=False))
            #p2amp.append(n2_p2s[-1][1]-n2_p2s[-1][0])
            #~ print ('len cuts for std', len(cuts))
            avr_stds.append(std/np.sqrt(len(cuts)))
            #print(np.array(avr_stds).shape)
            avr_cuts.append(a)

        return avr_cuts, avr_stds, t,
        
    def cuts_draw(self, avr_cuts, avr_stds, chnls_n, y_lims='auto', print_q=False, name='', newfig=True, n2_p2_stamp=False,
                    electrode_matrix=False, color='black', point = '', test_t_score=None, t=[-0.300, 0.600], p_thresh=0.05):

        
        legend2 = True
        t = np.linspace(t[0], t[1], avr_cuts[0].shape[0])
        t0 = np.abs(t).argmin()
        if newfig==True:
            if print_q:
                fig = pb.figure(figsize=(10, 15), dpi=300)
            else:
                fig = pb.figure()
        else:
            fig = newfig
        chnls_n = self._get_chnl_labels(chnls_n)
        for nr, acut in enumerate (avr_cuts):
            if not electrode_matrix:  
                ax = fig.add_subplot(len(avr_cuts), 1, nr+1)
            else:
                pos = self.m1020.index(self._get_chnl_labels(chnls_n)[nr])+1
                ax = fig.add_subplot(len(self.m1020)/self.m1020l, self.m1020l, pos)
                
                if pos == 11:
                    ax.set_ylabel(u'Napięcie, μV]')
                    
                    #### rysowanie w macierzy topograficznej
            if test_t_score != None:
                ax_double = ax.twinx()
                #print(test_t_score.shape)

                ax_double.plot(t, test_t_score[nr, :], color='black',alpha=0.2, label = 'H0, probability')

                #print(t[test_t_score[nr, :]<0.01])
                highlits = t[test_t_score[nr, :]<p_thresh]

                
                # grouping highlits for speed
                index = 0
                boxes=[]
                l_l = None
                l_h = None
                if len(highlits)>0:
                    l_l = highlits[index]
                    while index < len(highlits)-1:
                        #~ print(t[1]-t[0])
                        if highlits[index+1]-highlits[index]>1.6/self.Fs:
                            l_h = highlits[index]
                            boxes.append([l_l, l_h])
                            l_l = highlits[index+1]
                            l_h = None
                            
                        index+=1
                    if l_h == None:
                        boxes.append([l_l, highlits[-1]])
                    #print(boxes)
                    #painting highlits
                    for k in boxes:
                        if legend2:
                            ax_double.axvspan(k[0]-0.5/self.Fs, k[1]+0.5/self.Fs, color='green', alpha=0.5, label = 'H0, probability<{}'.format(p_thresh))
                            h2, l2 = ax_double.get_legend_handles_labels()
                            legend2 = False
                        else:
                            ax_double.axvspan(k[0]-0.5/self.Fs, k[1]+0.5/self.Fs, color='green', alpha=0.5)
                #~ for k in t[test_t_score[nr, :]<p_thresh]:
                    #~ ax.axvspan(k-0.5/self.Fs, k+0.5/self.Fs, color='orange', alpha=0.5, label = 'H0, probability<{}'.format(p_thresh))

                   
            #print(len(t), 'acut', len(acut))
            #na =str(name)[9:15]
            na = name
            #print(na)
            #na = 'test_test'
            
            

            ax.plot(t, acut, color=color, label = na)
            if avr_stds:
                ax.plot(t, acut+avr_stds[nr], '-.', linewidth=0.5, color=color,label = na+'_std')
                ax.plot(t, acut-avr_stds[nr], '-.', linewidth=0.5, color=color,label = na+'_std')
            
            ax.grid(True)
            ax.xaxis.grid(True, which='major')
            ax.xaxis.grid(True, which='minor') 
            handles, labels = ax.get_legend_handles_labels()
            
                
                
            #~ if test_t_score != None:
                #~ handles_d, labels_d = ax_double.get_legend_handles_labels()
                #~ handles += handles_d
                #~ labels += labels_d
                #~ 
            if nr==len(avr_cuts)-1:
                fig.legend(handles, labels, loc=2)
                if test_t_score != None:
                    try:
                        fig.legend(h2, l2, loc=0)
                    except Exception:
                        h2, l2 = ax_double.get_legend_handles_labels()
                        fig.legend(h2, l2, loc=0)
            #~ if n2_p2_stamp:
                #~ n2,p2,id_n2,id_p2=n2_p2s[nr]
                #~ ax.plot([(id_n2-1)/self.Fs+t[0], (id_n2-1)/self.Fs+t[0]], [0,n2])
                #~ ax.plot([(id_p2-1)/self.Fs+t[0], (id_p2-1)/self.Fs+t[0]], [0,p2])
            ax.set_title(chnls_n[nr])
            if y_lims == 'auto':
                maxl = np.amax(avr_cuts)
                minl = np.amin(avr_cuts)
                ax.set_ylim([minl, maxl]) #limity
            else:
                ax.set_ylim(y_lims)
            ax.set_xlim(t[0], t[-1])
            if nr+1 == len(avr_cuts):
                if not electrode_matrix:
                    ax.set_ylabel(u'Napięcie, μV]')
                ax.set_xlabel('Czas, [s]')
                
        if print_q:
            fig.tight_layout(pad=2)
            fig.savefig('r_'+str(ref)+'_trg_{}_H0_p_{}_'.format(point, p_thresh)+os.path.split(self.filename)[1]+'.png', dpi=150)
            #pb.close(fig)
                
    def usrednianie_specgram(self, cuts, chnls_n, draw=False, t=[-0.300, 0.600], print_q=False, name='', newfig=True,
                    electrode_matrix=False, point = '', scale='log', baseline=None,):
        '''t - lista dwuelementowa z czasem w sekundach przed zdarzeniem i po,
        wycinanym przed, baseline-wektor dwóch czasów w sekundach - dla baseline'u, scale='log', 'lin', 'sqrt' '''
        avr_cuts = []
        avr_stds = []
        n2_p2s = []
        p2amp = []
        decimate_to = 1024
        NFFT =  128
        noverlap = 127 
        timescale_original = np.linspace(t[0], t[1], cuts[0][0].shape[0])
        cut_resampled, timescale_resampled = ss.resample(cuts[0][0], decimate_to, timescale_original)
        #~ print(timescale_original, timescale_resampled)
        P, freqs, T, = specgram(cut_resampled, NFFT=NFFT, Fs=self.Fs*(1.*decimate_to/len(timescale_original)), noverlap=noverlap,)
        t_czasu = T+timescale_resampled[0]+NFFT/2/(self.Fs*(1.*decimate_to/len(timescale_original)))
        #~ print(t_czasu, 't czasu')
        
        t0 = np.abs(t_czasu).argmin()
            
        
        for n, i in enumerate(chnls_n):
            avr = []
        
            for cut in cuts:
                cut, timescale_resampled = ss.resample(cut[n,:], decimate_to, timescale_original)
                P, freqs, T, = specgram(cut, NFFT=NFFT, Fs=self.Fs*(1.*decimate_to/len(timescale_original)), noverlap=noverlap,)
                if scale=='log':
                    P = np.log(P)
                elif scale=='sqrt':
                    P = np.sqrt(P)
                elif scale=='lin':
                    P = P
                else:
                    raise Exception('nieznana skala')
                if baseline:
                    bmin_spec = abs(t_czasu-baseline[0]).argmin()
                    bmax_spec = abs(t_czasu-baseline[1]).argmin()
                    P = P-np.mean(P[:,bmin_spec:bmax_spec], axis=1)[:, None]
                avr.append(P)#-np.dot(np.mean(P[basel:baseh,:, axis=1])[:,None],np.ones(1,P.shape[1])))

            a=np.mean(np.array(avr), axis=0)
            std = np.std(np.array(avr), axis=0)
            


            avr_cuts.append(a)

        
        
        if draw:
            if newfig==True:
                if print_q:
                    fig = pb.figure(figsize=(20, 15), dpi=300)
                else:
                    fig = pb.figure()
            else:
                fig = newfig
            chnls_n = self._get_chnl_labels(chnls_n)
            
            
            
            for nr, acut in enumerate (avr_cuts):
                if not electrode_matrix:  
                    ax = fig.add_subplot(len(avr_cuts), 1, nr+1)#, projection='3d')
                else:
                    pos = self.m1020.index(self._get_chnl_labels(chnls_n)[nr])+1
                    ax = fig.add_subplot(len(self.m1020)/self.m1020l, self.m1020l, pos)
                    if pos == 11:
                        ax.set_ylabel('colormap{} {} uV**2'.format(['relative gain of', ''][baseline==None], scale))
                        
                        #### rysowanie w macierzy topograficznej
                
                #print(len(t), 'acut', len(acut))
                #na =str(name)[9:15]
                na = name
                #print(na)
                #na = 'test_test'
                #~ if ref_zmiany:
                    #~ print(acut.shape)
                    #~ print(basel, baseh)
                    #~ print(acut[:,basel:baseh])
                    #~ P_base = np.mean(acut[:,basel:baseh], axis=1)
                    #~ print(P_base)
                    #~ print(P_base[:, None].shape, acut.shape, 'pbase')
                #~ 
                    #~ acut=acut-P_base[:, None]
                
                
                #~ print (t_czasu.shape, freqs.shape, acut.shape)
                #print(freqs, 'freqs')
                refreqs = freqs[freqs<20]
                #print(refreqs, 'freqs')
                #DRAW AT LAST
                pc = ax.pcolorfast(t_czasu, refreqs, acut[:len(refreqs)-1,:], label = na)#,vmin=-1, vmax=1)
                #~ pc = ax.plot_surface(t_czasu, refreqs, acut[:len(refreqs)-1,:], label = na)
                ax.set_ylim([0,20])
                ax.set_xlim([t_czasu[0],t_czasu[-1]])
                ax.grid(True)
                fig.colorbar(pc, ax = ax)
                handles, labels = ax.get_legend_handles_labels()
                if nr==0:
                    fig.legend(handles, labels, loc=2)

                ax.set_title(chnls_n[nr]+'_'+name)
                #~ ax.set_ylabel('colormap{} {} uV**2/Hz'.format(['relative gain of', ''][ref==None], scale))
                if nr+1 == len(avr_cuts):
                    if not electrode_matrix:
                        ax.set_ylabel('colormap {} {} uV**2'.format(['relative gain from {} of'.format(baseline), ''][baseline==None], scale))
                    ax.set_xlabel('Czas, [s]')
                    
            if print_q:
                fig.tight_layout(pad=1)
                fig.savefig('spectrogram_r_'+str(ref)+'_p_{}_name_{}_'.format(point,na )+os.path.split(self.filename)[1]+'.png', dpi=150)
                #pb.close(fig)
        return avr_cuts, t,
        
    def cuts_matching_pursuit(self, cuts, chnls, type='incor', mmp=False):
        if not mmp:
            mmp = 'SMP'
        else:
            mmp = mmp
        print(chnls)
        #sys.exit()
        old_cwd = os.getcwd()
        
        N_realisations = len(cuts)
        len_cut = len(cuts[0][0])
        cuts_arr = np.array(cuts)
        dataFileName = os.path.basename(self.filename)
        
        mp_cache_path = './'+os.path.basename(self.filename)+'_mp/mp_cache/'
        mp_out_path = '../mp_output/'
        os.chdir(os.path.dirname(self.filename))
        
        
        
        if not os.path.exists(mp_cache_path):
                    os.makedirs(mp_cache_path)
        os.chdir(mp_cache_path)
        if not os.path.exists(mp_out_path):
                    os.makedirs(mp_out_path)
        
        
        
        #~os.chdir(os.path.dirname(self.filename)+mp_cache_path[1:])
        print(os.getcwd())
        for nr, chnl in enumerate(chnls):
            cache_mp_filename = 'mp_cache_'+'type_{}_chnl_{}'.format( type, chnl)+'.bin'
            flatten = np.resize(cuts_arr[:,nr,:].T, (len_cut*N_realisations, 1))

            
            flatten.astype(np.float32).tofile(cache_mp_filename)
            mp_config = '''# OBLIGATORY PARAMETERS
nameOfDataFile         {}
nameOfOutputDirectory  {}
writingMode            CREATE
samplingFrequency      {}
numberOfChannels       {}
selectedChannels       1-{}
numberOfSamplesInEpoch {}
selectedEpochs         1
typeOfDictionary       OCTAVE_STOCH
energyError         0.1 90.00
randomSeed             auto
reinitDictionary       NO_REINIT_AT_ALL
maximalNumberOfIterations {}
energyPercent             95.0
MP                        {}
scaleToPeriodFactor       1.0
pointsPerMicrovolt        1.0

# ADDITIONAL PARAMETERS

diracInDictionary         NO
gaussInDictionary         NO
sinCosInDictionary        NO
gaborInDictionary         YES
progressBar               ON
'''.format(cache_mp_filename, mp_out_path, self.Fs, int(N_realisations), int(N_realisations), len_cut,NATOMS, mmp)
            config_filename= 'mp_config.txt'
            configfile = open(config_filename, 'w')
            configfile.write(mp_config)
            configfile.close()
            
            os.system('pwd')
            print(chnl)
            comm = "mp5-amd64 -f {}".format(config_filename)
            print(comm)
            os.system(comm)
        os.chdir(old_cwd)
        return '/'+mp_cache_path+'/'+mp_out_path+cache_mp_filename
        
def group_cuts_from_files(ref):
    chnls = ['FCz','O1', ]
    chnls = ['FCz', ]
    #~ chnls = ['Fp1', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3', 'Cz', 'C4', 'T4', 'T5', 'P3', 'Pz', 'P4', 'T6', 'O1', 'O2', 'AFz', 'FCz', 'A1', 'A2',]

   # chnls = ['Fz']
    cuts_grand_corr = []
    cuts_grand_incorr = []
    t_lim_h = 0.5# dodatnia liczba, po 0
    t_lim_l = 0# dodatnia liczba, przed 0
    t = [-t_lim_l, t_lim_h]
    baseline=[0., 0.5]
    baseline_spec =  None #[-3.9, -3.8]
    noreject = False
    trsh = 20
    filtr = 'filterek'
    electrode_matrix = False 
    pos = 'pos'
    fig = pb.figure(figsize=(20,12))
    for file in sys.argv[1:]:
        eeg= Eeg(file, resample_to_Fs = 128.)
        cuts_corr, chnl = eeg.cuts_from_textfile_audio(chnls,
                            before_event=int(t_lim_l*eeg.Fs), type='correct', after_event=int(t_lim_h*eeg.Fs),
                            filt=filtr ,nazwa_wykresu='',
                            jump = 0.030, ref=ref, noreject=noreject, trsh=trsh, point=pos, draw=False)
        
                            
        cuts_incorr, chnl = eeg.cuts_from_textfile_audio(chnls,
                            before_event=int(t_lim_l*eeg.Fs), type='incorrect', after_event=int(t_lim_h*eeg.Fs),
                            filt=filtr ,nazwa_wykresu='', ref=ref,
                            jump = 0.030, noreject=noreject, trsh=trsh, point = pos, draw=False)
        cuts_corr, chnl = eeg.cuts_rm_baseline(cuts_corr, chnl, 
                baseline=baseline, timescale=t,)
                
        cuts_incorr, chnl = eeg.cuts_rm_baseline(cuts_incorr, chnl, 
                baseline=baseline, timescale=t,)
        try:
            cuts_grand_corr = np.concatenate((cuts_grand_corr, cuts_corr))
            cuts_grand_incorr = np.concatenate((cuts_grand_incorr, cuts_incorr))
        except Exception as a:
            print(a)
            cuts_grand_corr =  cuts_corr
            cuts_grand_incorr = cuts_incorr
    mmp = 'MMP3'
    #~ 
    #~ booksc = eeg.cuts_matching_pursuit(cuts_grand_corr, chnl, type='cor',mmp = mmp)
    
    #~ booksi = eeg.cuts_matching_pursuit(cuts_grand_incorr, chnl, type='incor', mmp = mmp)

    
    
    avr_cuts_corr, avt_cuts_corr_std, t, = eeg.cuts_avr(cuts_grand_corr, chnl, 
                        t=[-t_lim_l, t_lim_h], baseline=None)
    avr_cuts_incorr, avt_cuts_incorr_std, t, = eeg.cuts_avr(cuts_grand_incorr, chnl,
                    t=[-t_lim_l, t_lim_h], baseline=None)
                    #~ 
    t_score, p, timescale, = eeg.cuts_welchs_t_test(cuts_grand_corr, cuts_grand_incorr, chnl, t)

    print(len(cuts_grand_corr), len(cuts_grand_incorr))
    eeg.cuts_draw(avr_cuts_corr, avt_cuts_corr_std, chnl,
                y_lims='auto', print_q=False, 
                name=str(len(cuts_grand_corr))+' correct_'+pos, 
                newfig=fig, n2_p2_stamp=False,
                electrode_matrix=electrode_matrix, color='blue', 
                point = pos, test_t_score=None, t=[-t_lim_l, t_lim_h])
    #~ 
#~ 
    eeg.cuts_draw(avr_cuts_incorr, avt_cuts_incorr_std, chnl, 
            y_lims=[-5, 10], print_q=True, 
            name=str(len(cuts_grand_incorr))+' incorrect_'+pos, 
            newfig=fig, n2_p2_stamp=False,
            electrode_matrix=electrode_matrix, color='red',
             point = pos, test_t_score=p, t=[-t_lim_l, t_lim_h],
             p_thresh=0.01,)
    return cuts_grand_incorr, cuts_grand_corr, chnl, eeg, p
    
def optimal_prod(cut, atoms, Fs, ptspmV, epoch_s, amp=True, ph=False, draw=False, drawmean=None, saveto=None):
    atoms_opt_parameters = []
    cut_or = cut.copy()
    time = np.linspace(0, epoch_s/Fs, epoch_s)
    #dopasowanie
    amps = []
    phases = []
    param3 = []
    param4 = []
    for nr, atom in enumerate(atoms[13]):
        position  = atom['t']/Fs
        width     = atom['scale']/Fs
        frequency = atom['f']*Fs/2
        #~ print('atom nr', nr)
        #~ print('position, width, frequency', position, width, frequency)
        if ph:
            C = gabor(epoch_s,Fs,1,position,width,frequency,0,)
            S =  gabor(epoch_s,Fs,1,position,width,frequency,np.pi/2,)
            phase = np.arctan((np.dot(cut, S)/np.dot(S,S))/(np.dot(cut, C)/np.dot(C,C)))
            #~ phase = np.linspace(-2*np.pi, 2*np.pi, 10000)[:,None]
            #~ gs = gabor(epoch_s,Fs,1,position,width,frequency,phase,)
            #~ phase = phase[np.sum((gs-cut)**2, axis=1).argmin()]
        else:
            phase = atom['phase']
            #~ phases.append(phase/(2*np.pi*frequency)+position)
        
        
        if amp:

            
            g = gabor(epoch_s,Fs,1,position,width,frequency,phase,)
            amplitude = 1/np.sum(g**2)
            #~ print('amp', amplitude)
            g = gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
            amplitude = np.dot(cut, g)
            #~ 
            
            #~ amplitude = np.linspace(0, np.max(np.abs(cut))*1.5, 1000)[:,None]
            #~ gs = gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
            #~ amplitude = amplitude[np.sum((gs-cut)**2, axis=1).argmin()]
        else:
            amplitude = atom['amplitude']
        print(amplitude)
        amps.append(amplitude)
        #~ phases.append(phase)
        phases.append(phase)
        param3.append(frequency)
        param4.append(width)
        
        cut -= gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
        atoms_opt_parameters.append({'amplitude':amplitude, 'phase':phase})
    # now let's recontruct and dot
    N = len(atoms[13])
    #~ atoms_to_draw=
    for nr, atom in list(enumerate(atoms[13]))[0:2]:
        position  = atom['t']/Fs
        width     = atom['scale']/Fs
        frequency = atom['f']*Fs/2
        phase = atoms_opt_parameters[nr]['phase']
        amplitude = atoms_opt_parameters[nr]['amplitude']
        try:
            template += gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
        except Exception:
            template = gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
    print('optimal product')
    opt_prod = np.dot(cut_or, template)#-np.sum((cut_or-template)**2)**0.5
    print(opt_prod)
    if draw:
        t = np.arange(0, epoch_s/Fs, 1./Fs)
        fig = pb.figure(figsize=(2.5, 2.5))
        ax = fig.add_subplot(111)
        ax.plot(t,cut_or, 'b', label=u'sygnał')
        ax.plot(t,template, '--', color='g', label=u'rekonstrukcja')
        if not (drawmean is None):
            print('drawmean', drawmean.shape)
            ax.plot(t,drawmean[0], 'k', alpha =0.5, label=u'średni sygnał')
        #~ ax.legend()
        ax.set_xlabel(u'Czas, [s]')
        ax.set_ylabel(ur'Napięcie [$\mu$V]')
        ax.set_ylim([-15, 15])
        if not (saveto is None):
            fig.tight_layout()
            fig.savefig(saveto, dpi=300)
            pb.close(fig)
        #~ pb.show()
    return opt_prod, amps, phases, param3, param4
        

def hist_statistic(cuts_cor, cuts_incor, chnl, eeg):



    N_to_train = len(cuts_incorr)/2
    N_to_train = 15 #ala
    #~ N_corr_train = len(cuts_cor)/4
    N_corr_train = 200
    np.random.shuffle(cuts_incor)
    cuts_learn = cuts_incor[:N_to_train]
    cuts_test = cuts_incor[N_to_train:]

    np.random.shuffle(cuts_cor)
    cuts_cor_train = cuts_cor[:N_corr_train]
    cuts_cor_test = cuts_cor[N_corr_train:]
    #~ [[np.mean(cuts_learn, axis=0)[0]],]
    mmp = 'MMP1'
    book_learn = eeg.cuts_matching_pursuit(cuts_learn, chnl, type='learn', mmp=mmp)
    opt_amp = True
    opt_ph = False
    draw_pictures = False
    path_to_book = os.path.abspath(os.path.dirname(sys.argv[1])+book_learn[2:-4]+'_mmp.b')
    data, signals, atoms, epoch_s = read_book(path_to_book)

    #~ get_template_distr(cuts_learn, atoms, epoch_s, data, draw=False, mmp='MMP3', type='train_incor', opt_amp=True, opt_ph=True)
    hist_inc_test, amph_inc_test = get_template_distr(cuts_test, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp, type='test_incor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_inc_learn, amph_inc_learn = get_template_distr(cuts_learn, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp, type='train_incor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_corr_train, amph_corr_train = get_template_distr(cuts_cor_train, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp, type='train_cor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_corr_test, amph_corr_test =  get_template_distr(cuts_cor_test, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp, type='test_cor', opt_amp=opt_amp, opt_ph=opt_ph)

    figsize = (6,6)
    fig = pb.figure(figsize=figsize, dpi=300)
    #~ ax = fig.add_subplot(111, projection='3d')
    X, Y = (5, 3)
    hfig = pb.figure(figsize=figsize, dpi=300)
    hpfig = pb.figure(figsize=figsize, dpi=300)
    mark = [ 7 , 4 , 5 , 6 , 'o' , 'D' , 'h' , 'H' , '_' , '8' , 'p' , ',' , '+' , '.' , 's' , '*' , 'd' , 3 , 0 , 1 , 2 , '1' , '3' , '4' , '2' , 'v' , '<' , '>' , '^']
    atoms_to_use = []
    for m, at in zip(mark[:NATOMS], xrange(NATOMS)): #,
        ax = fig.add_subplot(Y,X,at+1)
        hax = hfig.add_subplot(Y,X,at+1)
        hpax = hpfig.add_subplot(Y,X,at+1)
        #~ sys.exit()
        #~ pb.figure()
        #~ pb.title('testing, atom {}'.format(at))
        hax.set_title( 'Atom {}'.format(at))
        hpax.set_title( 'Atom {}'.format(at))

        ax.scatter(amph_corr_test[:,0, at], amph_corr_test[:, 1, at], color='b')# ,label=u'Testowe poprawne, atom {}'.format(at))
        ax.scatter(amph_inc_test[:,0, at], amph_inc_test[:, 1,at],  color='r',)# label=u'Testowe błędne, atom'.format(at))

        hax.hist(np.concatenate((amph_inc_test[:,0, at], amph_inc_learn[:,0, at])),  color='r', normed=True, alpha=0.5,)#,label=u'Testowe poprawne, atom {}'.format(at))
        hax.hist(np.concatenate((amph_corr_test[:,0, at], amph_corr_train[:,0, at])), color='b', alpha = 0.3, normed=True, )#label=u'Testowe błędne, atom'.format(at))
        hpax.hist(np.concatenate((amph_inc_test[:,1, at], amph_inc_learn[:,1, at])),  color='r', normed=True, alpha=0.5,)#,label=u'Testowe poprawne, atom {}'.format(at))
        hpax.hist(np.concatenate((amph_corr_test[:,1, at], amph_corr_train[:,1, at])), color='b', alpha = 0.3, normed=True, )#label=u'Testowe błędne, atom'.format(at))

        print('Atom {}, amplitudes, {} F-Value, {} p, ANOVA'.format(at, *st.f_oneway(np.concatenate((amph_inc_test[:,0, at], amph_inc_learn[:,0, at])), np.concatenate((amph_corr_test[:,0, at], amph_corr_train[:,0, at])))))
        print('Atom {}, Phases, {} F-Value, {} p, ANOVA'.format(at, *st.f_oneway(np.concatenate((amph_inc_test[:,1, at], amph_inc_learn[:,1, at])), np.concatenate((amph_corr_test[:,1, at], amph_corr_train[:,1, at])))))
        Hvalue, pvalue = st.kruskal(np.concatenate((amph_inc_test[:,0, at], amph_inc_learn[:,0, at])), np.concatenate((amph_corr_test[:,0, at], amph_corr_train[:,0, at])))
        if pvalue<0.05:
            atoms_to_use.append(at)

        print('Atom {}, amplitudes, {} H-Value, {} p, kruskal'.format(at, Hvalue, pvalue))
        #~ print('Atom {}, Phases, {} H-Value, {} p, kruskal'.format(at, *st.kruskal(np.concatenate((amph_inc_test[:,1, at], amph_inc_learn[:,1, at])), np.concatenate((amph_corr_test[:,1, at], amph_corr_train[:,1, at])))))
        print('\n')


        #~ ax.scatter(amph_corr_test[:,0, at]**2, amph_corr_test[:, 1, at], color='b',  marker=m,label='test correct trials atom {}'.format(at))
        #~ ax.scatter(amph_inc_test[:,0, at]**2, amph_inc_test[:, 1,at], color='r', marker=m, label='test incorrect trials, atom {}'.format(at))

        ax.set_xlabel(r'Amplituda, [$\mu$V]')
        hax.set_xlabel(r'Amplituda, [$\mu$V]')
        hpax.set_xlabel(r'Faza')
        hax.set_xlim([-15, 15])
        ax.set_ylabel(r'Faza')
        ax.set_title( 'Atom {}'.format(at))

        #~ ax.set_title('testing, atom {}'.format(at))
    fig.tight_layout()
    hfig.tight_layout()
    fig.tight_layout(pad=10)
    hfig.tight_layout(pad=10)
    hpfig.tight_layout(pad=10)
    fig.savefig('./scatter_{}/testing.png'.format(mmp), dpi=300)
    hfig.savefig('./scatter_{}/razem_hist.png'.format(mmp), dpi=300)
    hpfig.savefig('./scatter_{}/razem_hist_faze.png'.format(mmp), dpi=300)
    ax.legend()
    hax.legend()






    fig = pb.figure(figsize=figsize, dpi=300)
    hfig = pb.figure(figsize=figsize, dpi=300)
    hpfig = pb.figure(figsize=figsize, dpi=300)
    for m, at in zip(mark[:NATOMS], xrange(NATOMS)): #,
        hax = hfig.add_subplot(Y,X,at+1)
        hpax = hpfig.add_subplot(Y,X,at+1)
        hax.hist(amph_inc_learn[:,0, at],  color='r', alpha = 0.5, normed=True)# label=u'Uczące błędne, atom {}'.format(at))
        hax.hist(amph_corr_train[:,0, at], color='b', alpha = 0.3, normed=True)#,label=u'Uczące poprawne, atom {}'.format(at))
        hpax.hist(amph_inc_learn[:,1, at],  color='r', alpha = 0.5, normed=True)# label=u'Uczące błędne, atom {}'.format(at))
        hpax.hist(amph_corr_train[:,1, at], color='b', alpha = 0.3, normed=True)#,label=u'Uczące poprawne, atom {}'.format(at))
        hpax.set_title( 'Atom {}'.format(at))
        hax.set_title( 'Atom {}'.format(at))
        hax.set_xlabel(r'Amplituda, [$\mu$V]')
        hpax.set_xlabel(r'Faza')
        hpax.set_xticks([-1.6, 0, 1.6])
        ax = fig.add_subplot(Y,X,at+1)
        #~ sys.exit()
        #~ pb.figure()
        #~ pb.title('training, atom {}'.format(at))
        #~ ax.scatter(amph_inc_learn[:,0, at]**2, amph_inc_learn[:, 1,at], amph_inc_learn[:, 2,at],  marker='v', color='r', label='test incorrect trials, atom {}'.format(at))
        #~ ax.scatter(amph_corr_train[:,0, at]**2, amph_corr_train[:, 1, at], amph_corr_train[:, 2, at],  marker = 'o', color='b', alpha=0.5,label='test correct trials atom {}'.format(at))
        #~ ax.scatter(amph_inc_learn[:,0, at]**2, amph_inc_learn[:, 1,at],  marker=m, color='r', label='test incorrect trials, atom {}'.format(at))
        #~ ax.scatter(amph_corr_train[:,0, at]**2, amph_corr_train[:, 1, at], marker = m, color='b', alpha=0.5,label='test correct trials atom {}'.format(at))
        ax.scatter(amph_corr_train[:,0, at], amph_corr_train[:, 1, at], color='b', alpha=0.5,)#label=u'Uczące poprawne, atom {}'.format(at))
        ax.scatter(amph_inc_learn[:,0, at], amph_inc_learn[:, 1,at], color='r',)# label=u'Uczące błędne, atom {}'.format(at))
        ax.set_title( 'Atom {}'.format(at))
        ax.set_xlabel(r'Amplituda, [$\mu$V]')
        ax.set_ylabel(r'Faza$')
        hax.set_xlim([-15, 15])

        #~ ax.set_title('training, atom {}'.format(at))
    ax.legend()

    hax.legend()
    fig.tight_layout(pad=10)
    hfig.tight_layout(pad=10)
    hpfig.tight_layout(pad=10)
    fig.savefig('./scatter_{}/training.png'.format(mmp), dpi=300)
    hfig.savefig('./scatter_{}/training_hist.png'.format(mmp), dpi=300)
    hpfig.savefig('./scatter_{}/training_hist_faza.png'.format(mmp), dpi=300)
    #~ pb.show()
    print(cuts_cor_train.shape)
    fig = pb.figure(figsize=(7,7), dpi = 300)
    ax = fig.add_subplot(111)

    #~ ax.plot(*siec_tries_timeline(cuts_cor_train, cuts_learn, cuts_cor, cuts_incor), color = 'blue', ls='-',label=u'Sieć dziedzina czasu')
    #~ print('net timeline\n\n')
    #~ ax.plot(*siec_tries(amph_corr_train, amph_inc_learn, np.concatenate((amph_corr_test, amph_corr_train)), np.concatenate((amph_inc_test, amph_inc_learn)),atoms_to_use=atoms_to_use), color = 'green', ls='-',label=u'Sieć dziedzina atomów')
    #~ print('net_atoms \n\n')
    ax.plot(*lda_test(amph_corr_train[:,0, atoms_to_use], amph_inc_learn[:,0, atoms_to_use], np.concatenate((amph_corr_test[:,0, atoms_to_use], amph_corr_train[:,0, atoms_to_use])), np.concatenate((amph_inc_test[:,0, atoms_to_use], amph_inc_learn[:,0, atoms_to_use]))), color = 'red', ls='-',label=u'LDA, amplitudy atomów')
    print('lda_atoms\n\n')
    ax.plot(*testuj_rozklad(hist_inc_learn, hist_corr_train, np.concatenate((hist_inc_test, hist_inc_learn)), np.concatenate((hist_corr_test, hist_corr_train))), color = 'cyan', ls='--',label=u'Rozkład, iloczynów skalarnych, punkt odcięcia')
    print('rozklad_il_skalarny\n\n')
    ax.plot(*testuj_rozklad(amph_inc_learn[:,0, atoms_to_use[0]]**2, amph_corr_train[:,0, atoms_to_use[0]]**2, np.concatenate((amph_inc_test[:,0, atoms_to_use[0]]**2, amph_inc_learn[:,0, atoms_to_use[0]]**2)), np.concatenate((amph_corr_test[:,0, atoms_to_use[0]]**2, amph_corr_train[:,0, atoms_to_use[0]]**2))), color = 'magenta', ls= '--', label=u'Rozkład kwadratów amplitud pierwszego atomu, punkt odcięcia')
    print('rozklad_pierwszy_dobry_atom\n\n')
    ax.plot(*bayes_class(amph_corr_train[:,0, atoms_to_use[0]], amph_inc_learn[:,0, atoms_to_use[0]], np.concatenate((amph_corr_test[:,0, atoms_to_use[0]], amph_corr_train[:,0, atoms_to_use[0]])), np.concatenate((amph_inc_test[:,0, atoms_to_use[0]], amph_inc_learn[:,0, atoms_to_use[0]])) ), color = 'yellow', ls=':',label=u'Bayes, amplitudy pierwszego atomu')
    print('bayes 1 dobry atom\n\n')
    ax.plot(*bayes_class(hist_corr_train, hist_inc_learn, np.concatenate((hist_corr_test, hist_corr_train)), np.concatenate((hist_inc_test, hist_inc_learn))), color = 'black', ls=':',label=u'Bayes, iloczyn skalarny wycinka ze wzorcem')
    print('bayes il skalarny\n\n')
    ax.plot(np.linspace(0, 1, 100), np.linspace(0, 1, 100), color = 'black', label=u'X=Y')

    ax.plot(*bayes_class_multi(amph_corr_train, amph_inc_learn, amph_corr_test, amph_inc_test, atoms_to_use),label=u'Bayes, amplitudy atomów')


    ax.legend(loc=4, prop={'size':6})
    fig.savefig('./scatter_{}/rocs.png'.format(mmp), dpi=300)

    return [[hist_inc_learn, hist_corr_train, hist_inc_test, hist_corr_test],  [amph_corr_train, amph_inc_learn, amph_corr_test, amph_inc_test]]

def get_template_distr(cuts_learn, atoms, epoch_s, data, draw=False, mmp='MMP3', type='train_incor', opt_amp=True, opt_ph=True):
    ''' get statistics from template and cuts (one channel cuts), needs - cuts as returned from eeg.cuts_from***, 
    atoms, as atoms from read_book from book importer, same for epoch_s and data'''
    hist_inc_learn = []
    amph_inc_learn = []
    drawmean=np.mean(cuts_learn,axis=0)
    for at, cut in enumerate(cuts_learn):
        print(type)
        prod, amps, phases, param3, param4 = optimal_prod(cut[0], atoms[1], data[5]['Fs'], 1., epoch_s, amp=opt_amp,
                                                    ph=opt_ph, draw=draw, 
                                                    drawmean=np.mean(cuts_learn,axis=0), saveto='./{}/{}_{}.png'.format(mmp, type, at))
        #~ prod, amps, phases, param3, param4 = optimal_prod(cut[0], atoms[1], data[5]['Fs'], 1., epoch_s, amp=opt_amp, ph=opt_ph,
                                                    #~ draw=False, 
                                    #~ drawmean=np.mean(cuts_cor_train,axis=0), 
        hist_inc_learn.append(prod)
        amph_inc_learn.append([amps, phases, param3, param4])
    return np.array(hist_inc_learn), np.array(amph_inc_learn)
    
def bayes_class(hist_corr_train, hist_inc_learn, hist_corr_test,hist_inc_test):
    pb.figure()
    pb.hist(hist_inc_learn, color='b',alpha=0.5, bins = 30, label='training incorrect trials', normed=True)
    pb.hist(hist_corr_train, color='r',alpha=0.5, bins = 30, label='training correct trials', normed=True)
    THR = st.scoreatpercentile(hist_corr_train, 99)
    pb.axvline(x=THR,color='k', label=u'99 procentów triali bez ERRP')
    hist_inc_test = np.array(hist_inc_test)
    hist_corr_test = np.array(hist_corr_test)
    pb.xlabel(u'iloczyn skalarny karany pierwiastkiem błędu błędu kwadratowego')
    pb.ylabel(u'ilość wycinków')
    ### kernel-density estimate using Gaussian kernels.
    p_error = st.gaussian_kde(hist_inc_learn) #P(E|H)
    p_error.set_bandwidth()
    x_prod = np.linspace(np.min((np.min(hist_inc_learn), np.min(hist_corr_train))), np.max((np.max(hist_inc_learn), np.max(hist_corr_train))), 1000)
    pb.plot(x_prod, p_error(x_prod), color='b')
    
    p_correct = st.gaussian_kde(hist_corr_train)
    p_correct.set_bandwidth()
    pb.plot(x_prod, p_correct(x_prod), color='r')
    ##### probability of ErrR a priori P(H)
    p_H =0.1
    ### P(E) whole:
    p_all = st.gaussian_kde(np.concatenate((hist_inc_learn, hist_corr_train)))
    p_all.set_bandwidth()
    
    pb.plot(x_prod, p_all(x_prod), color='k', label=u'wspólny rozkład')

    #####
    pb.legend()


    # to make ROC line
    whole = np.concatenate((hist_corr_test, hist_inc_test))
    N_errors = len(hist_inc_test)
    N_corrects = len(hist_corr_test)
    for THR in np.linspace(0, 1, 100):

        # bayes probability a posteri P(H|E): 
        p_h_e = p_error(hist_inc_test)*p_H/p_all(hist_inc_test)
        TP = len(p_h_e[p_h_e>=THR])*1.
        FN = len(p_h_e[p_h_e<THR])*1.
        p_h_c = p_error(hist_corr_test)*p_H/p_all(hist_corr_test)
        TN = len(p_h_c[p_h_c<THR])*1.
        FP = len(p_h_c[p_h_c>=THR])*1.
        if abs((TN/N_corrects*100)-99)<1:
            print('THR', THR)
            print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(TN/N_corrects*100, TP, N_errors, float(TP)/N_errors*100))
            print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
        try:
            TPR.append(TP/(TP+FN))
            FPR.append(FP/(FP+TN))
        except Exception:
            TPR = [TP/(TP+FN), ]
            FPR = [FP/(FP+TN), ]
    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)# dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral

def bayes_class_multi(amph_corr_train, amph_inc_learn, amph_corr_test, amph_inc_test, atoms_to_use):
    p_error_atoms = {}
    p_all_atoms = {}
    for atom in atoms_to_use:
        hist_corr_train = amph_corr_train[:, 0,atom].copy()
        hist_inc_learn = amph_inc_learn[:, 0,atom].copy()
        hist_corr_test= amph_corr_test[:, 0,atom].copy()
        hist_inc_test =  amph_inc_test[:, 0,atom].copy()
        pb.figure()
        pb.hist(hist_inc_learn, color='b',alpha=0.5, bins = 30, label='training incorrect trials', normed=True)
        pb.hist(hist_corr_train, color='r',alpha=0.5, bins = 30, label='training correct trials', normed=True)
        THR = st.scoreatpercentile(hist_corr_train, 99)
        pb.axvline(x=THR,color='k', label=u'99 procentów triali bez ERRP')
        hist_inc_test = np.array(hist_inc_test)
        hist_corr_test = np.array(hist_corr_test)
        pb.xlabel(u'iloczyn skalarny karany pierwiastkiem błędu błędu kwadratowego')
        pb.ylabel(u'ilość wycinków')
        pb.title(u'Atom nr {}'.format(atom))

        ### kernel-density estimate using Gaussian kernels.
        p_error = st.gaussian_kde(hist_inc_learn) #P(E|H)
        p_error.set_bandwidth()
        x_prod = np.linspace(np.min((np.min(hist_inc_learn), np.min(hist_corr_train))), np.max((np.max(hist_inc_learn), np.max(hist_corr_train))), 1000)
        pb.plot(x_prod, p_error(x_prod), color='b')
        
        p_correct = st.gaussian_kde(hist_corr_train)
        p_correct.set_bandwidth()
        pb.plot(x_prod, p_correct(x_prod), color='r')
        ##### probability of ErrR a priori P(H)
        p_H =0.1
        ### P(E) whole:
        p_all = st.gaussian_kde(np.concatenate((hist_inc_learn, hist_corr_train)))
        p_all.set_bandwidth()
        
        pb.plot(x_prod, p_all(x_prod), color='k', label=u'wspólny rozkład')
    
        #####
        pb.legend()
        p_all_atoms[atom] = p_all
        p_error_atoms[atom] = p_error
    
    # to make ROC line
    whole = np.concatenate((hist_corr_test, hist_inc_test))
    N_errors = len(hist_inc_test)
    N_corrects = len(hist_corr_test)
    for THR in np.linspace(0, 1*10**(1-len(atoms_to_use)), 100):
        print(THR)
        # bayes probability a posteri P(H|E): 
        p_h_e = []
        for atom in atoms_to_use:
            p_h_e.append(p_error_atoms[atom](amph_inc_test[:, 0,atom])*p_H/p_all_atoms[atom](amph_inc_test[:, 0,atom]))
        p_h_e = np.prod(p_h_e, axis=0)
        
        TP = len(p_h_e[p_h_e>=THR])*1.
        FN = len(p_h_e[p_h_e<THR])*1.
        
        p_h_c = []
        for atom in atoms_to_use:
            p_h_c.append(p_error_atoms[atom](amph_corr_test[:, 0,atom])*p_H/p_all_atoms[atom](amph_corr_test[:, 0,atom]))
        p_h_c= np.prod(p_h_c, axis=0)

        TN = len(p_h_c[p_h_c<THR])*1.
        FP = len(p_h_c[p_h_c>=THR])*1.
        if abs((TN/N_corrects*100)-99)<0.2:
            print('THR', THR)
            print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(TN/N_corrects*100, TP, N_errors, float(TP)/N_errors*100))
            print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
        try:
            TPR.append(TP/(TP+FN))
            FPR.append(FP/(FP+TN))
        except Exception:
            TPR = [TP/(TP+FN), ]
            FPR = [FP/(FP+TN), ]
    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)# dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral

def testuj_rozklad(hist_inc_learn, hist_corr_train, hist_inc_test, hist_corr_test):
    pb.figure()
    
    pb.hist(hist_inc_test, color='b',alpha=0.5, bins = 30, label='test incorrect trials')
    pb.hist(hist_corr_test, color='r',alpha=0.5, bins = 30, label='correct trials')
    THR = st.scoreatpercentile(hist_corr_train, 99)
    pb.axvline(x=THR,color='k', label=u'99 procentów triali bez ERRP')
    hist_inc_test = np.array(hist_inc_test)
    hist_corr_test = np.array(hist_corr_test)
    pb.xlabel(u'iloczyn skalarny karany pierwiastkiem błędu błędu kwadratowego')
    pb.ylabel(u'ilość wycinków')
    pb.legend()
    # to make ROC line
    whole = np.concatenate((hist_corr_test, hist_inc_test))
    N_errors = len(hist_inc_test)
    N_corrects = len(hist_corr_test)
    for THR in np.linspace(np.min(whole), np.max(whole), 1000):
        #~ THR = ss.scoreatpercentile(whole, percent)
       
        TP = len(hist_inc_test[hist_inc_test>=THR])*1.
        FN = len(hist_inc_test[hist_inc_test<THR])*1.
        
        TN = len(hist_corr_test[hist_corr_test<THR])*1.
        FP = len(hist_corr_test[hist_corr_test>=THR])*1.
        #~ print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))

        try:
            TPR.append(TP/(TP+FN))
            FPR.append(FP/(FP+TN))
        except Exception:
            TPR = [TP/(TP+FN), ]
            FPR = [FP/(FP+TN), ]
    
    percentiles = [99, 95, 90]
    print('threshold is got from training corr set (estimation of statistic distribution is from limited "training" set')
    for percentile in percentiles:
        THR = st.scoreatpercentile(hist_corr_train, percentile)
        N_errors = len(hist_inc_test )
        N_corrects = len(hist_corr_test )
        TP = len(hist_inc_test [hist_inc_test>=THR])*1.
        FN = len(hist_inc_test [hist_inc_test <THR])*1.
        
        TN = len(hist_corr_test [hist_corr_test <THR])*1.
        FP = len(hist_corr_test [hist_corr_test >=THR])*1.
        
        print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(percentile, TP, N_errors, float(TP)/N_errors*100))
        print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)  # dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral
        
        
        
        
def siec_tries(corr, incorr, test_corr, test_incorr, atoms_to_use):
    from pybrain.datasets            import ClassificationDataSet
    from pybrain.utilities           import percentError
    from pybrain.tools.shortcuts     import buildNetwork
    from pybrain.supervised.trainers import BackpropTrainer
    from pybrain.structure.modules   import SoftmaxLayer, SigmoidLayer
    from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot, subplot
    from scipy import diag, arange, meshgrid, where
    from numpy.random import multivariate_normal
    import numpy as np
    N_class = 2
    alldata = ClassificationDataSet(len(atoms_to_use), 1, nb_classes=N_class)
    for klasa, dane in enumerate([corr, incorr]):
        print(dane.shape)
        for i in xrange(len(dane[:,0, 0])):
            
            input = dane[i,0,atoms_to_use].flatten()

            
            alldata.addSample(input, [klasa])

    
    tstdata, trndata = alldata.splitWithProportion( 0.25 )
    #~ 
    trndata._convertToOneOfMany( )
    tstdata._convertToOneOfMany( )
    
    print ("Ilość przykładów treningowych: ", len(trndata))
    print("Rozmiary wejścia i wyjścia: ", trndata.indim, trndata.outdim)
    #~ print ("Pierwszy przykład (wejście , wyjście, klasa):")
    #~ print (trndata['input'][200], trndata['target'][200], trndata['class'][200])
    #~ sys.exit()
    
    fnn = buildNetwork( trndata.indim, 15,5,trndata.outdim, outclass=SoftmaxLayer, hiddenclass=SigmoidLayer,)
    # the best?? fnn = buildNetwork( trndata.indim, 5, trndata.outdim, outclass=SoftmaxLayer, hiddenclass=SigmoidLayer,)
    
    trainer = BackpropTrainer( fnn, dataset=trndata, learningrate=0.001, momentum=0.7, verbose=False, weightdecay=0.000001)
    
    
    #~ ticks = arange(-10.,10.,0.5)
    #~ X, Y = meshgrid(ticks, ticks)
    #~ # need column vectors in dataset, not arrays
    #~ griddata = ClassificationDataSet(2,1, nb_classes=N_class)
    #~ for i in xrange(X.size):
        #~ griddata.addSample([X.ravel()[i],Y.ravel()[i]], [0])
    #~ griddata._convertToOneOfMany()
    
    

    epochs = 20
    pars = np.zeros((epochs,len(fnn.params)))
    figure(900)
    
    err1 = np.zeros(epochs)
    err2 = np.zeros(epochs)
    ion()
    show()
    for i in range(epochs):
        trainer.trainEpochs( 10 )
    
    
        trnresult = percentError( trainer.testOnClassData(dataset=trndata), trndata['class'] )
        tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
        err1[i] = trnresult
        err2[i] = tstresult
        #~ print ("krok: %4d" % trainer.totalepochs, 
              #~ "  błąd na zbiorze uczącym:  %5.2f%%" % trnresult, 
              #~ "  błąd na zbiorze testowym: %5.2f%%" % tstresult)
        pars[i,:] = fnn.params
        #~ out = fnn.activateOnDataset(griddata)
        #~ out = out.argmax(axis=1)  # the highest output activation gives the class
        #~ out = out.reshape(X.shape)
        
        ioff()  # wyłączamy tryb interaktywny grafiki
        clf()   # czyścimy rysunek
        hold(True) # włączamy opcję dopisywania do bieżącego rysunku
        subplot(211)
    
        plot(err1)
        plot(err2)
        subplot(212)
        #~ plot(pars)
        ion()   # przełączamy grafikę w tryb interaktywny 
        draw()  # odświeżamy rysunek
        #~ show()
    ioff()
    
    # roc
    for thr in np.linspace(0, 1, 100):
        #~ print(thr)
        TP, FP, TN, FN = 0, 0, 0, 0
        N_corrects = len(test_corr)
        N_errors = len(test_incorr)
        for klasa, dane in enumerate([test_corr, test_incorr]):
            for  i in xrange(len(dane[:,0,0])):
                act = fnn.activate(dane[i,0,atoms_to_use].flatten() )
                #~ print(act, klasa)
                #~ print(klasa)
                if klasa < 1:
                    TN += int(act[1]<thr)*1.
                    FP += int(act[1]>thr)*1.
                else:
                    TP += int(act[1]>thr)*1.
                    FN += int(act[1]<thr)*1.
        if abs((TN/N_corrects*100)-99)<1:
            print('thr', thr)
            print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(TN/N_corrects*100, TP, N_errors, float(TP)/N_errors*100))
            print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
        try:
            TPR.append(TP/(TP+FN))
            FPR.append(FP/(FP+TN))
        except Exception:
            TPR = [TP/(TP+FN), ]
            FPR = [FP/(FP+TN), ]

    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)  # dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral

def siec_tries_timeline(corr, incorr, test_corr, test_incorr):
    from pybrain.datasets            import ClassificationDataSet
    from pybrain.utilities           import percentError
    from pybrain.tools.shortcuts     import buildNetwork
    from pybrain.supervised.trainers import BackpropTrainer
    from pybrain.structure.modules   import SoftmaxLayer, SigmoidLayer
    from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot, subplot
    from scipy import diag, arange, meshgrid, where
    from numpy.random import multivariate_normal
    import numpy as np
    
    print(corr[0,0,12:57].shape)
    #~ sys.exit()
    N_class = 2
    alldata = ClassificationDataSet(45, 1, nb_classes=N_class)
    for klasa, dane in enumerate([corr, incorr]):
        print(dane.shape)
        for i in xrange(len(dane[:,0, 0])):
            
            input = dane[i,0,12:57].flatten()

            
            alldata.addSample(input, [klasa])

    
    tstdata, trndata = alldata.splitWithProportion( 0.25 )
    #~ 
    trndata._convertToOneOfMany( )
    tstdata._convertToOneOfMany( )
    
    print ("Ilość przykładów treningowych: ", len(trndata))
    print("Rozmiary wejścia i wyjścia: ", trndata.indim, trndata.outdim)
    #~ print ("Pierwszy przykład (wejście , wyjście, klasa):")
    #~ print (trndata['input'][200], trndata['target'][200], trndata['class'][200])
    #~ sys.exit()
    
    fnn = buildNetwork( trndata.indim, 5,trndata.outdim, outclass=SoftmaxLayer, hiddenclass=SigmoidLayer,)
    # the best?? fnn = buildNetwork( trndata.indim, 5, trndata.outdim, outclass=SoftmaxLayer, hiddenclass=SigmoidLayer,)
    
    trainer = BackpropTrainer( fnn, dataset=trndata, learningrate=0.001, momentum=0.7, verbose=False, weightdecay=0.000001)
    
    
    #~ ticks = arange(-10.,10.,0.5)
    #~ X, Y = meshgrid(ticks, ticks)
    #~ # need column vectors in dataset, not arrays
    #~ griddata = ClassificationDataSet(2,1, nb_classes=N_class)
    #~ for i in xrange(X.size):
        #~ griddata.addSample([X.ravel()[i],Y.ravel()[i]], [0])
    #~ griddata._convertToOneOfMany()
    
    

    epochs = 20
    pars = np.zeros((epochs,len(fnn.params)))
    figure(900)
    
    err1 = np.zeros(epochs)
    err2 = np.zeros(epochs)
    ion()
    show()
    for i in range(epochs):
        trainer.trainEpochs( 10 )
    
    
        trnresult = percentError( trainer.testOnClassData(dataset=trndata), trndata['class'] )
        tstresult = percentError( trainer.testOnClassData(dataset=tstdata ), tstdata['class'] )
        err1[i] = trnresult
        err2[i] = tstresult
        #~ print ("krok: %4d" % trainer.totalepochs, 
              #~ "  błąd na zbiorze uczącym:  %5.2f%%" % trnresult, 
              #~ "  błąd na zbiorze testowym: %5.2f%%" % tstresult)
        pars[i,:] = fnn.params
        #~ out = fnn.activateOnDataset(griddata)
        #~ out = out.argmax(axis=1)  # the highest output activation gives the class
        #~ out = out.reshape(X.shape)
        
        ioff()  # wyłączamy tryb interaktywny grafiki
        clf()   # czyścimy rysunek
        hold(True) # włączamy opcję dopisywania do bieżącego rysunku
        subplot(211)
    
        plot(err1)
        plot(err2)
        subplot(212)
        #~ plot(pars)
        ion()   # przełączamy grafikę w tryb interaktywny 
        draw()  # odświeżamy rysunek
        #~ show()
    ioff()
    
    # roc
    for thr in np.linspace(0, 1, 1000):
        #~ print(thr)
        TP, FP, TN, FN = 0, 0, 0, 0
        N_corrects = len(test_corr)
        N_errors = len(test_incorr)
        for klasa, dane in enumerate([test_corr, test_incorr]):
            for  i in xrange(len(dane[:,0,0])):
                act = fnn.activate(dane[i,0,12:57].flatten() )
                #~ print(act, klasa)
                #~ print(klasa)
                if klasa < 1:
                    TN += int(act[1]<thr)*1.
                    FP += int(act[1]>thr)*1.
                else:
                    TP += int(act[1]>thr)*1.
                    FN += int(act[1]<thr)*1.
        if abs((TN/N_corrects*100)-99)<2:
            print('THR', thr)
            print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(TN/N_corrects*100, TP, N_errors, float(TP)/N_errors*100))
            print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
        try:
            TPR.append(TP/(TP+FN))
            FPR.append(FP/(FP+TN))
        except Exception:
            TPR = [TP/(TP+FN), ]
            FPR = [FP/(FP+TN), ]
    #~ pb.figure()
    #~ pb.plot(FPR, TPR)
    #~ pb.plot([0,0.5,1], [0,0.5,1])
    #~ pb.ylabel(u'TPR')
    #~ pb.xlabel(u'FPR')
    #~ pb.title('siec roc')
    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)  # dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral
    #~ show()
    
def lda_test(corr, incorr, test_corr, test_incorr, ):
    
    
    import mlpy
    x_ucz = np.concatenate([incorr, corr])
    
    y_ucz = np.concatenate([np.ones(len(incorr)), -np.ones(len(corr))])
    print(x_ucz.shape)
    print(y_ucz.shape)
    
    x_test = np.concatenate([test_incorr, test_corr])
    y_test = np.concatenate([np.ones(len(test_incorr)), -np.ones(len(test_corr))])

    lda = mlpy.Fda()
    lda.compute(x_ucz, y_ucz)
    cl = lda.predict(x_test)

    w = lda.realpred


    TP, FP, TN, FN = 0, 0, 0, 0
    N_errors = N_incor = len(test_incorr)
    N_corrects = len(test_corr)
    for trh in np.linspace(w.min()-w.min()/10, w.max()+w.max()/10, 100):
        TP = np.sum(w[:N_incor]>trh)
        FP = np.sum(w[N_incor:]>trh)
        TN = np.sum(w[N_incor:]<=trh)
        FN = np.sum(w[:N_incor]<=trh)
        try:
            TPR.append(float(TP)/(TP+FN))
            FPR.append(float(FP)/(FP+TN))
        except Exception:
            TPR = [float(TP)/(TP+FN), ]
            FPR = [float(FP)/(FP+TN), ]
        #~ print( float(TN)/N_corrects*100)
        #~ print(float(TP)/(TP+FN))
        if abs((float(TN)/N_corrects*100)-99)<0.3:
            print('THR', trh)
            print('{} percent of correct trials: {} detected incorrect of {}, {} percent'.format(float(TN)/N_corrects*100, TP, N_errors, float(TP)/N_errors*100))
            print('TP {}, FP {}, TN {}, FN {}'.format(TP, FP, TN, FN))
    TPR=np.array(TPR) 
    FPR=np.array(FPR)
    sortedind = np.argsort(FPR)
    roc_integral = si.trapz(y=TPR[sortedind], x=FPR[sortedind])
    print('ROC integral:', roc_integral)  # dx=1, axis=-1, even='avg')[source]¶
    print('\n\n')
    return FPR, TPR, roc_integral
if __name__ == '__main__':




    stats = hist_statistic(cuts_corr, cuts_incorr, chnl, eeg)

    
    #~ for file in sys.argv[1:]:
       
            
        #~ if porownanie_oddball(file, ref)==None:
            #~ continue

    # dla analizy 30 blokow po 15 serii:
    #eeg = Eeg('30_serii_po15/sygnal')
    #~ 
    #usrednianie_blokowe(eeg, chnls)
    
    pb.show()
