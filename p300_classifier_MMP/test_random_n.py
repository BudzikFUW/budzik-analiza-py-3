#!/usr/bin/env python3
# coding: utf8
from classify_score import boot_strap_aucs
import numpy as np


from balancer import FirstNTooBigException

AVERAGE = 3

n_trials = np.arange(10, 100, 1)

for nr, i in enumerate(n_trials):
    try:
        boot_strap_aucs(average=AVERAGE, first_n=i, random_n=True)
    except FirstNTooBigException:
        print "FINISHED"
        break
    except (ValueError, UserWarning, RuntimeWarning) as e:
        print
        print
        print
        print "Skipping ", i, 'Error:',  e
        pass

