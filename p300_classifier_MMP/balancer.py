import random

import mne
from sklearn.base import TransformerMixin
import numpy as np

def balance(data, classes):
    classes_counts = []
    for i in np.unique(classes):
        classes_counts.append(np.sum(classes==i))
    minimum = np.min(classes_counts)
    print("BALANCED CLASSES COUNT", minimum)
    new_data = []
    new_classes = []

    print(data.shape, classes.shape)

    for i in np.unique(classes):
        new_data.append(data[classes==i][:minimum])
        ncl = np.zeros(minimum)
        ncl[:] = i
        new_classes.append(ncl)
    new_data = np.vstack(new_data)
    new_classes = np.hstack(new_classes)
    print("new_data, after balancing", new_data.shape)
    print("new_classes, after balancing", new_classes.shape)
    return new_data, new_classes


def balance_epochs(epochs, classes):
    event_ids = list(epochs.event_id.keys())
    event_ids_numbers = [len(epochs[i]) for i in event_ids]
    balanced_epochs_list = []
    balanced_no = min(event_ids_numbers)

    for i in event_ids:
        epochs_new = epochs[i]
        skip_by = (1.0*len(epochs_new))/balanced_no
        indexes = [int(i*skip_by) for i in range(balanced_no)]
        epochs_new_balanced = epochs_new[indexes]
        balanced_epochs_list.append(epochs_new_balanced)


    balanced_epochs = mne.concatenate_epochs(balanced_epochs_list)


    new_classes = balanced_epochs.events[:, 2]

    print("new_data, after balancing", balanced_epochs)
    print("new_classes, after balancing", new_classes.shape)
    return balanced_epochs, new_classes


class FirstNTooBigException(Exception):
    pass


def select_first_n_examples(epochs, classes, first_n=None, first_n_offset=0):
    """Select first N (or with offset) occurences of epochs (by class)"""
    event_ids = epochs.event_id.keys()
    new_epochs = []
    for event_id in event_ids:
        new_epochs.append(epochs[event_id][first_n_offset:first_n_offset + first_n])
    epochs_l = np.array([len(i) for i in new_epochs])
    if np.any(epochs_l < first_n):
        raise FirstNTooBigException
    new_epochs = mne.epochs.concatenate_epochs(new_epochs)
    new_classes = new_epochs.events[:, 2]

    left_epochs = []
    for event_id in event_ids:
        e = epochs[event_id][first_n_offset + first_n:]
        if len(e):
            left_epochs.append(e)
        e = epochs[event_id][:first_n_offset]
        if len(e):
            left_epochs.append(e)

    left_epochs = mne.epochs.concatenate_epochs(left_epochs)
    left_classes = left_epochs.events[:, 2]

    return new_epochs, new_classes, left_epochs, left_classes

def select_random_n_examples(epochs, classes, first_n=None, first_n_offset=0):
    """Select first N (or with offset) occurences of epochs (by class)"""
    event_ids = epochs.event_id.keys()
    left_ids = {}
    new_epochs = []
    for event_id in event_ids:
        epochs_event_id = epochs[event_id]
        epochs_ids = list(range(len(epochs_event_id)))
        # selecting n random
        random.shuffle(epochs_ids)
        to_take = epochs_ids[:first_n]
        left = epochs_ids[first_n:]
        left_ids[event_id] = left

        new_epochs.append(epochs_event_id[to_take])
    epochs_l = np.array([len(i) for i in new_epochs])
    if np.any(epochs_l < first_n):
        raise FirstNTooBigException
    new_epochs = mne.epochs.concatenate_epochs(new_epochs)
    new_classes = new_epochs.events[:, 2]

    left_epochs = []
    for event_id in event_ids:
        epochs_event_id = epochs[event_id]
        left = left_ids[event_id]
        if len(left):
            left_epochs.append(epochs_event_id[left])

    left_epochs = mne.epochs.concatenate_epochs(left_epochs)
    left_classes = left_epochs.events[:, 2]

    return new_epochs, new_classes, left_epochs, left_classes

