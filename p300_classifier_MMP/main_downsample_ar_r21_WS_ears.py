#!/usr/bin/env python3
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=3, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_WS_r21_errs/downsample/results_WS_40_1000_r21_ears', pipeline_type='downsampling')
boot_strap_aucs(average=1, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_WS_r21_ears/downsample/results_WS_40_1000_r21_ears', pipeline_type='downsampling')
