
import matplotlib
matplotlib.use("Agg")
import pylab
import pandas as pd
import numpy as np
import sys
import os

FILE = 'results_table.csv'
try:
    print(sys.argv)
    FILE = sys.argv[1]
    print('USING FILE:', FILE)
except IndexError:
    pass

def draw_boxplots(FILE):

    FILE = os.path.abspath(FILE)
    try:
        SUFFIX = os.path.splitext(os.path.basename(FILE))[0].split('_')[-1]
    except IndexError:
        SUFFIX = 'unkwn'

    WORKFOLDER = os.path.dirname(FILE)


    d = pd.read_csv(FILE)
    print(d)
    print(d.columns)

    colsP = sorted([i for i in d.columns if 'P' in i])
    colsAUC = sorted([i for i in d.columns if 'AUC' in i])
    colsMannn = sorted([i for i in d.columns if 'ManWhitU' in i])
    print('colsP', colsP)
    print('colsAUC', colsAUC)
    d[colsP] /= 100.

    def expand_xticks(d, ax):

        new_labels = []
        for label in ax.get_xticklabels():
            text = label.get_text()
            n = d[text].count()


            #~ import IPython
            #~ IPython.embed()


            text = '{} N={}'.format(text, n)
            label.set_text(text)
            new_labels.append(label)
        ax.set_xticklabels(new_labels)




    if colsP:
        pylab.figure(figsize=(15, 10))
        ax = d.boxplot(column = colsP)
        expand_xticks(d, ax)
        pylab.axhline(0.5)
        pylab.ylim([0,1])

        pylab.savefig(os.path.join(WORKFOLDER, 'P_values_{}.png'.format(SUFFIX)))

    if colsAUC:
        pylab.figure(figsize=(15,10))
        ax = d.boxplot(column = colsAUC)

        expand_xticks(d, ax)

        pylab.axhline(0.5)
        pylab.ylim([0,1])

        pylab.savefig(os.path.join(WORKFOLDER, 'AUC_values_{}.png'.format(SUFFIX)))

    if colsMannn:
        pylab.figure(figsize=(20,15))
        ax = d.boxplot(column = colsMannn)

        expand_xticks(d, ax)

        pylab.axhline(0.05)
        pylab.ylim([1e-8, 1])
        pylab.yscale('log')
        pylab.savefig(os.path.join(WORKFOLDER, 'MannnWhit_values_{}.png'.format(SUFFIX)))

    #corrected (throwing out strange invverted outliers)
    d2 = d.copy()

    d2[d2<0.5] = np.NaN
    if colsAUC:
        pylab.figure(figsize=(15,10))

        ax = d2.boxplot(column = colsAUC)

        expand_xticks(d2, ax)

        pylab.axhline(0.5)
        pylab.ylim([0, 1])

        pylab.title('Corrected')
        pylab.savefig(os.path.join(WORKFOLDER, 'AUC_values_{}_no_strange.png'.format(SUFFIX)))

    d2[d2<0.5] = np.NaN
    if colsP:
        pylab.figure(figsize=(15, 10))
        ax = d2.boxplot(column = colsP)
        expand_xticks(d2, ax)
        pylab.axhline(0.5)
        pylab.ylim([0, 1])
        pylab.title('Corrected')
        pylab.savefig(os.path.join(WORKFOLDER, 'P_values_{}_no_strange.png'.format(SUFFIX)))

for i in sys.argv[1:]:
    draw_boxplots(i)