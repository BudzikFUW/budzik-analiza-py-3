#coding: utf-8
import matplotlib
matplotlib.use("Agg")
import pylab
import pandas as pd
import numpy as np
import sys
import matplotlib.pyplot as plt
import os
from itertools import product
import seaborn as sns

from collections import defaultdict

FILE = 'results_database.csv'
try:
    print(sys.argv)
    FILE = sys.argv[1]
    print('USING FILE:', FILE)
except IndexError:
    pass


def expand_xticks(d, ax):

    new_labels = []
    for label in ax.get_xticklabels():
        text = label.get_text()
        n = d[text].count()

        text = '{} N={}'.format(text, n)
        label.set_text(text)
        new_labels.append(label)
    ax.set_xticklabels(new_labels)

def draw_boxplots_min_P(FILE):

    FILE = os.path.abspath(FILE)
    try:
        SUFFIX = os.path.splitext(os.path.basename(FILE))[0].split('_')[-1]
    except IndexError:
        SUFFIX = 'unkwn'

    WORKFOLDER = os.path.dirname(FILE)


    d = pd.read_csv(FILE)

    d['crs_group'] = d.diagnosis
    # d.crs_group.loc[(d.crs_group == 'MCS-') | (d.crs_group == 'MCS+')] = 'MCS'

    subjects = d['id'].unique()
    rounds = d['round'].unique()
    paradigms = d['paradigm'].unique()
    diagnoses = d['crs_group'].unique()

    measure = 'ManWhitU'

    independent_measurements = defaultdict(list)
    independent_measurements_min_P = defaultdict(list)

    # itertools product zamiast 4 forów
    for subject, round, paradigm, diagnosis in product(subjects, rounds, paradigms, diagnoses):
        mask = ((d['id'] == subject)
                & (d['round'] == round)
                & (d['paradigm'] == paradigm)
                & (d['crs_group'] == diagnosis)
                & (d['measure'] == measure)
                )
        values = d[mask]['value']
        if len(values)==1:
            independent_measurements[str(subject) + str(round) + str(diagnosis)].append(float(values))
        elif len(values) == 0:
            pass
        else:
            print d[mask]['value']
            print subject, round, paradigm, diagnosis
            raise Exception("SOMETHING WRONG WITH DATABASE")


    for key in independent_measurements.keys():
        for diagnosis in diagnoses:
            if diagnosis in key:
                independent_measurements_min_P[diagnosis].append(min(independent_measurements[key]))

    df = pd.DataFrame.from_dict(independent_measurements_min_P, orient='index').transpose()
    df2 = df[sorted(df.keys())]


    pylab.figure(figsize=(15,15))
    ax = df2.boxplot()

    expand_xticks(df2, ax)

    pylab.axhline(0.05)
    pylab.ylim([1e-8, 1])
    pylab.yscale('log')
    pylab.savefig(os.path.join(WORKFOLDER, 'MannnWhit_values_minimum_per_measure.png'))

    #pylab.figure(figsize=(15, 15))
    for nr, diagnosis in enumerate(sorted(diagnoses)):
        value = independent_measurements_min_P[diagnosis]
        x = [nr+1] * len(value)
        ax.scatter(x, value, 2000, alpha=0.3)
    pylab.yscale('log')

    pylab.savefig(os.path.join(WORKFOLDER, 'MannnWhit_values_minimum_per_measure_scatter.png'))



if __name__ == '__main__':
    for i in sys.argv[1:]:
        draw_boxplots_min_P(i)