# coding: utf-8
import os
import sys

import matplotlib
matplotlib.use("Agg")
import json
import pylab as pb
import numpy
import numpy as np



import scipy.stats as st
from scipy.stats import mannwhitneyu

WORK_DIR = sys.argv[1].rstrip(r'\/')

def sanitize_work_dir(work_dir):
    WORK_DIR = work_dir.rstrip(r'\/')
    if WORK_DIR.startswith('file:'):
        WORK_DIR = WORK_DIR[7:]
    return WORK_DIR


def p_val_from_auc(n1, n2, auc):
    sd = np.sqrt(n1 * n2 * (n1 + n2 + 1) / 12.0)  # bez tiecorrection, ale powinno być ok
    u = auc * n1 * n2
    z = (u - 0.5 - n1 * n2 / 2) / sd
    p = st.norm.cdf(z)  # test jednostronny
    return p


def draw_aucs_p_vals_hists(WORK_DIR):
    print "RYSUJE ROZKLAD AUC DLA", WORK_DIR
    WORK_DIR = sanitize_work_dir(WORK_DIR)

    folders = os.listdir(WORK_DIR)

    work_dirname = os.path.basename(WORK_DIR)

    aucs = []
    p_vals = []
    p_vals_from_aucs = []
    n_targets = []
    n_nontargets = []

    for folder in folders:
        try:
            aucs.append(json.load(open(os.path.join(WORK_DIR, folder, 'data.json')))['auc'])
        except IOError, OSError:
            pass
        try:
            preds = numpy.loadtxt(os.path.join(WORK_DIR, folder, 'predictions.txt'))
            
            preds0 = preds[:, 0][preds[:, 1] == 0]
            preds1 = preds[:, 0][preds[:, 1] == 1]

            n_targets.append(np.sum(preds[:, 1] == 0))
            n_nontargets.append(np.sum(preds[:, 1] == 1))
            U, mwu_p_value = mannwhitneyu(preds1, preds0, alternative='less')
            p_vals.append(mwu_p_value)
            p_vals_from_aucs.append(p_val_from_auc(n_targets[-1], n_nontargets[-1], json.load(open(os.path.join(WORK_DIR, folder, 'data.json')))['auc']))
        except IOError, OSError:
            pass


     
    pb.figure()
    pb.hist(aucs, bins=np.linspace(0,1, 100))
    percentile = st.scoreatpercentile(aucs, 95)
    mean_targets = np.mean(n_targets)
    mean_nontargets = np.mean(n_nontargets)

    print("95 Percentyl AUC: {}, N: {}".format(st.scoreatpercentile(aucs, 95), mean_targets))
    print("99 Percentyl AUC: {}, N: {}".format(st.scoreatpercentile(aucs, 99), mean_targets))

    result_dict = {'99_perc_auc': st.scoreatpercentile(aucs, 99),
                   '95_perc_auc': st.scoreatpercentile(aucs, 95),
                   '90_perc_auc': st.scoreatpercentile(aucs, 90),
                   'n_targets': n_targets,
                   'n_nontargets': n_nontargets,
                   'aucs': aucs,
                   'p_vals': p_vals,
                   'mean_n_targets': mean_targets,
                   'mean_n_nontargets': mean_nontargets,
                   }

    with open(os.path.join(WORK_DIR, '..', work_dirname+'_result.json'), 'w') as f:

        json.dump(result_dict, f)


    pb.title('AUCS: {} Mean {}, std {} N{}'.format(percentile, np.mean(aucs), np.std(aucs), mean_targets))
    pb.axvline(percentile)
    pb.xlim([0, 1])

    pb.savefig(os.path.join(WORK_DIR, '..', work_dirname+'_AUCS.png'))

    pb.figure()
    pb.hist(p_vals, bins=np.linspace(0,1, 100))
    percentile = st.scoreatpercentile(p_vals, 5)
    pb.title('P VALUE: {} {} {} Percentile at 0.05 {}'.format(percentile, percentile*mean_targets, mean_targets, st.percentileofscore(p_vals, 0.05)))
    pb.axvline(percentile)
    pb.xlim([0, 1])

    pb.savefig(os.path.join(WORK_DIR, '..', work_dirname+'_p_vals.png'))


    pb.figure()
    pb.hist(p_vals_from_aucs, bins=np.linspace(0,1, 100))
    percentile = st.scoreatpercentile(p_vals_from_aucs, 5)
    pb.title('P VALUE FROM AUC: {} {} {} Percentile at 0.05 {}'.format(percentile, percentile*mean_targets, mean_targets, st.percentileofscore(p_vals_from_aucs, 0.05)))
    pb.axvline(percentile)
    pb.xlim([0, 1])

    pb.savefig(os.path.join(WORK_DIR, '..', work_dirname+'_p_vals_from_auc.png'))


    pb.figure()
    try:
        pb.scatter(p_vals, numpy.abs(np.array(aucs)-0.5)+0.5)
    except ValueError:
        pass
    pb.xlabel('P val')
    pb.ylabel('Auc')
    pb.xlim([0, 1])
    pb.ylim([0, 1])

    pb.savefig(os.path.join(WORK_DIR, '..', work_dirname+'_auc_vs_p.png'))
    return result_dict
if __name__ == '__main__':
    draw_aucs_p_vals_hists(WORK_DIR)

    #~ pb.show()
        
     
     
