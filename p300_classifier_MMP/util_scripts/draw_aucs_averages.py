import sys
import os
import matplotlib

from sorted_result import get_sorted_result

matplotlib.use("Agg")
from subprocess import check_call
import re

import numpy
import pandas
import pandas as pd
import numpy as np
import pylab as pb
from process_folder_pre import process_folder_pre
from scipy.stats import mannwhitneyu

from collections import defaultdict

import scipy.stats

try:

    cwd = sys.argv[1]
    paradigm = sys.argv[2]
except IndexError:
    print "Please provide working dir and paradigm name"
    sys.exit(1)


PARADIGMS = {'p300_wz': 'p300_wz', 'p300_cz': 'p300_cz',
             'p300_sl': ['p300_sl', 'tony'],
             'p300_sim': 'p300_sim',
             'p300_sl_tony': 'p300_sl.*tony'}

def get_groups_for_paradigm(paradigm, workfolder):

    groups = [i.upper() for i in ['control_adult', 'emcs', 'mcs-', 'mcs+', 'uws', 'AR200_UWS']]

    data = defaultdict(list)

    for g in groups:
        items = os.listdir('{}/{}'.format(workfolder, g))

        for item in items:
            print item, paradigm, type(paradigm)
            if isinstance(paradigm[1], (list, tuple)):
                pos_re = paradigm[1][0]
                neg_re = paradigm[1][1]
                if re.search(pos_re, item.lower()) is None:
                    print 'SKIPPED'
                    continue
                if re.search(neg_re, item.lower()):
                    print 'SKIPPED'
                    continue
            else:
                if re.search(paradigm[1], item.lower()) is None:
                    print 'SKIPPED'
                    continue
            print 'Reading:', item
            with open(os.path.join(workfolder, g, item, 'RESULTS.TXT')) as f:
                data_p = f.read()
                try:
                    print(g, item)
                    AUC = float(data_p.splitlines()[6])
                    try:
                        preds = numpy.loadtxt(os.path.join(workfolder, g, item, 'predictions.txt'))
                        preds0 = preds[:, 0][preds[:, 1] == 0]
                        preds1 = preds[:, 0][preds[:, 1] == 1]
                        U, mwu_p_value = mannwhitneyu(preds0, preds1)
                    except ValueError:
                        mwu_p_value = 1
                    yield AUC, item, g, mwu_p_value
                except IndexError:
                    pass

                try:
                    P_line = data_p.splitlines()[11]
                    P = float(P_line.strip(', ').split(':')[1])
                    print('P value', P)
                    data[g+' P'].append(P)
                except IndexError:
                    pass

averages = range(1, 30, 1)
nfirsts = [None,]
#nfirsts = range(-1, -100, -1)

def OUTPUT_DIR(average=3, first_n=None):
    OUTPUT_DIR = os.path.join(os.path.expanduser(cwd), 'average_n_{}_first_n_{}'.format(average, first_n), os.path.basename(sys.argv[1]).split('.')[0])
    return OUTPUT_DIR

aucs = defaultdict(list)
Pvalues = defaultdict(list)

persons = set()


for avr in averages:
    for n in nfirsts:
        aucs_local = {}
        Pvalues_local = {}
        if not os.path.exists(OUTPUT_DIR(average=avr, first_n=n)):
            continue

        try:
            get_sorted_result(os.path.join(OUTPUT_DIR(average=avr, first_n=n)), )
        except OSError:
            pass

        try:
            for auc_per_person, person, group, mannwittnP in get_groups_for_paradigm(PARADIGMS[paradigm], os.path.join(OUTPUT_DIR(average=avr, first_n=n), '_sorted')):

                if 'control' in group.lower() or 'ar' in group.lower():
                    # import IPython
                    #
                    # IPython.embed()

                    aucs_local[person] = auc_per_person
                    Pvalues_local[person] = mannwittnP
                    persons.add(person)
        except KeyError:
            import traceback
            traceback.print_exc()
            continue
        aucs[avr] = (aucs_local)
        Pvalues[avr] = (Pvalues_local)


for n_first in nfirsts:

    pb.figure(figsize=(10,10))
    pb.title("AUCS per person")

    for person in persons:
        x_data = []
        y_data = []

        for avr in averages:
            try:
                y_data.append(aucs[avr][person])
                x_data.append(avr)
            except KeyError:
                pass
        pb.plot(x_data, y_data, label=person.split('_')[0])
    pb.legend(loc=4)
    pb.xlabel('N epochs averaged')
    pb.ylabel('AUCS')
    pb.xlim([0, 20])
    pb.ylim([0, 1])
    pb.savefig(os.path.join(cwd, 'aucs_per_n_avr_first_{}_{}.png'.format(n_first, paradigm)))



    pb.figure(figsize=(10, 10))
    data = aucs[avr]
    dataP = Pvalues[avr]
    pb.title("P values per person")

#    import IPython
#    IPython.embed()


    for person in persons:
        x_data = []
        y_data = []

        for avr in averages:
            try:                
                

                y_data.append(Pvalues[avr][person])
                x_data.append(avr)
            except KeyError:
                pass
        pb.plot(x_data, y_data, label=person.split('_')[0])
    pb.legend(loc=1)
    pb.xlabel('N epochs averaged')
    pb.ylabel('P_value')
    pb.xlim([1, 30])
    # pb.ylim([0.000000, .2])
    pb.axhline(0.05)
    pb.axhline(0.01)
    pb.axhline(0.001)
    pb.yscale('log')
    pb.savefig(os.path.join(cwd, 'P_values_per_n_avr_first_{}_{}.png'.format(n_first, paradigm)))


    pb.figure(figsize=(10, 10))

    x_data = []
    y_data = []
    y_err_data = []

    for avr in averages:
        x_data.append(avr)
        data = aucs[avr].values()
        y_data.append(np.mean(data))
        y_err_data.append(scipy.stats.sem(data))
    pb.title("Mean AUCS")
    pb.xlabel('N epochs averaged')
    pb.ylabel('AUCS, with std')
    pb.errorbar(x_data, y_data, yerr=y_err_data)
    pb.xlim([1, 30])
    pb.ylim([0, 1])
    pb.savetxt(os.path.join(cwd, 'mean_aucs_per_n_avr_first_{}_{}_x_y_y_std.txt'.format(n_first, paradigm)), np.array([x_data, y_data, y_err_data]))
    pb.savefig(os.path.join(cwd, 'mean_aucs_per_n_avr_first_{}_{}.png'.format(n_first, paradigm)))


    pb.figure(figsize=(10, 10))
    x_data = []
    y_data = []
    y_err_data = []

    for avr in averages:
        x_data.append(avr)
        data = Pvalues[avr].values()
        y_data.append(np.mean(data))
        y_err_data.append(scipy.stats.sem(data))
    pb.title("Mean P values")
    pb.xlabel('N averaged epochs')
    pb.ylabel('P_values, with std')
    pb.errorbar(x_data, y_data, yerr=y_err_data)
    pb.xlim([1, 30])
    pb.axhline(0.05)
    pb.axhline(0.01)
    pb.axhline(0.001)
    # pb.ylim([0, 0.20])
    pb.yscale('log')
    pb.savefig(os.path.join(cwd, 'mean_Pvals_per_n_avr_first_{}_{}.png'.format(n_first, paradigm)))








