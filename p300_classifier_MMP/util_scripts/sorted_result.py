import glob
from shutil import move
import pandas as pd
import os
import sys

status_DOC = ['EMCS', 'UWS', 'MCS-', 'MCS+w', 'MCS+', 'CONTROL_ADULT', 'CONTROL_CHILDREN', 'AR200_UWS']

def get_folders_names(dir):
    all_paths = glob.glob("{}/*".format(dir))
    all_folders_names = [n_path.split("/")[-1] for n_path in all_paths] 
    return all_folders_names

def get_status_info(info_file):
    info_data = pd.read_csv(info_file)
    info = {i:[] for i in status_DOC}
    for i in info.keys():
        info[i] = list(info_data[info_data['status']==i]['name'].values)
    return info 
        
def get_file_status(open_file, file_name):
    return open_file[open_file['name'] == file_name]['status'].values[0]


def get_sorted_result(dir_result):
    dir_result = os.path.expanduser(dir_result)
    output_path = os.path.expanduser('{}/_sorted'.format(dir_result))
    info_file = os.path.expanduser("~/tabelka_CRS.csv")
    all_folders_names = get_folders_names(dir_result)
    #print all_folders_names
    directory_dict ={}
    os.mkdir(output_path)
    for status in status_DOC:
        directory = '{}/{}'.format(output_path, status)
        os.mkdir(directory)
        directory_dict[status]=directory
    #print directory_dict
    info_data = pd.read_csv(info_file)
    for folder_name in all_folders_names:
        try:
            file_status = get_file_status(info_data, folder_name)
            os.mkdir("{}/{}/{}".format(output_path, file_status, folder_name))

            file_list_dir = os.path.join(dir_result, folder_name)
            file_list = os.listdir(file_list_dir)
            file_list = [os.path.join(file_list_dir, i) for i in file_list]
            print "to glob glob", os.path.join(dir_result, folder_name, '*')
            print folder_name, file_status, file_list
            print 'aaaaaaa', file_list
            for i in file_list:
                file_dir_new = "{}/{}/{}/".format(output_path, file_status ,folder_name)
                print "move ", i, file_dir_new
                move(i, file_dir_new)
            os.rmdir(("{}/{}".format(dir_result, folder_name)))
        except IndexError:
            print folder_name
            import traceback
            traceback.print_exc()
            #print folder_name
        
if __name__ == '__main__':
    dir_result = os.path.expanduser(sys.argv[1])
    get_sorted_result(dir_result)
             
        
    
    
