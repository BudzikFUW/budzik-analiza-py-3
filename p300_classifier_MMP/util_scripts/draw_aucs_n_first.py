import sys
import os

import matplotlib
matplotlib.use("Agg")
from subprocess import check_call
import re

import numpy
import pandas
import pandas as pd
import numpy as np
import pylab as pb
from process_folder_pre import process_folder_pre
from scipy.stats import mannwhitneyu

from collections import defaultdict

import scipy.stats

try:

    cwd = sys.argv[1]
    paradigm = sys.argv[2]
except IndexError:
    print "Please provide working dir and paradigm name"
    sys.exit(1)


PARADIGMS = {'p300_wz': 'p300_wz', 'p300_cz': 'p300_cz',
             'p300_sl': ['p300_sl', 'tony'],
             'p300_sl_tony': 'p300_sl.*tony'}

def get_groups_for_paradigm(paradigm, workfolder):

    groups = [i.upper() for i in ['control', 'emcs', 'mcs-', 'mcs+', 'uws', 'AR200_UWS']]

    data = defaultdict(list)

    for g in groups:
        items = os.listdir('{}/{}'.format(workfolder, g))

        for item in items:
            print item, paradigm, type(paradigm)
            if isinstance(paradigm[1], (list, tuple)):
                pos_re = paradigm[1][0]
                neg_re = paradigm[1][1]
                if re.search(pos_re, item.lower()) is None:
                    print 'SKIPPED'
                    continue
                if re.search(neg_re, item.lower()):
                    print 'SKIPPED'
                    continue
            else:
                if re.search(paradigm[1], item.lower()) is None:
                    print 'SKIPPED'
                    continue
            print 'Reading:', item
            with open(os.path.join(workfolder, g, item, 'RESULTS.TXT')) as f:
                data_p = f.read()
                try:
                    print(g, item)
                    AUC = float(data_p.splitlines()[6])
                    try:
                        preds = numpy.loadtxt(os.path.join(workfolder, g, item, 'predictions.txt'))
                        preds0 = preds[:, 0][preds[:, 1] == 0]
                        preds1 = preds[:, 0][preds[:, 1] == 1]
                        U, mwu_p_value = mannwhitneyu(preds0, preds1)
                    except ValueError:
                        mwu_p_value = 1
                    yield AUC, item, g, mwu_p_value
                except IndexError:
                    pass

                try:
                    P_line = data_p.splitlines()[11]
                    P = float(P_line.strip(', ').split(':')[1])
                    print('P value', P)
                    data[g+' P'].append(P)
                except IndexError:
                    pass

averages = [3]
nfirsts = range(1, 100)
#nfirsts = range(-1, -100, -1)

def OUTPUT_DIR(average=3, first_n=None):
    OUTPUT_DIR = os.path.join(os.path.expanduser(cwd), 'average_n_{}_first_n_{}'.format(average, first_n), os.path.basename(sys.argv[1]).split('.')[0])
    return OUTPUT_DIR

aucs = defaultdict(list)
Pvalues = defaultdict(list)

for avr in averages:
    persons = set()
    for n in nfirsts:
        aucs_local = {}
        Pvalues_local = {}
        if not os.path.exists(OUTPUT_DIR(average=avr, first_n=n)):
            continue
        process_folder_pre([os.path.join(OUTPUT_DIR(average=avr, first_n=n)), ])

        fname = os.path.join(OUTPUT_DIR(average=avr, first_n=n), '_sorted', 'results_table_{}.csv'.format(paradigm))
        try:
            d = pandas.read_csv(fname)
        except:
            continue
        try:
            for auc_per_person, person, group, mannwittnP in get_groups_for_paradigm(PARADIGMS['p300_wz'], os.path.join(OUTPUT_DIR(average=avr, first_n=n), '_sorted')):
                if 'control' in group.lower():
                    aucs_local[person] = auc_per_person
                    Pvalues_local[person] = mannwittnP
                    persons.add(person)
        except KeyError:
            import traceback
            traceback.print_exc()
            continue
        aucs[avr].append([abs(n), aucs_local])
        Pvalues[avr].append([abs(n), Pvalues_local])


for avr in averages:


    pb.figure(figsize=(10,10))
    data = aucs[avr]
    dataP = Pvalues[avr]
    pb.title("AVR = {}".format(avr))

    for person in persons:
        x_data = []
        y_data = []

        for n_trials, auc in data:
            try:
                y_data.append(auc[person])
                x_data.append(n_trials)
            except KeyError:
                pass
        pb.plot(x_data, y_data, label=person)
    pb.legend(loc=4)
    pb.xlabel('N epochs per example')
    pb.ylabel('AUCS')
    pb.xlim([0, 100])
    pb.ylim([0, 1])
    pb.savefig(os.path.join(cwd, 'aucs_per_first_n_epochs_avr_{}_{}.png'.format(avr, paradigm)))



    pb.figure(figsize=(10, 10))
    data = aucs[avr]
    dataP = Pvalues[avr]
    pb.title("P_values = {}".format(avr))

    for person in persons:
        x_data = []
        y_data = []

        for n_trials, auc in dataP:
            try:
                y_data.append(auc[person])
                x_data.append(n_trials)
            except KeyError:
                pass
        pb.plot(x_data, y_data, label=person.split('_')[0])
    pb.legend(loc=1)
    pb.xlabel('N epochs per example')
    pb.ylabel('P_value')
    pb.xlim([0, 100])
    # pb.ylim([0.000000, .2])
    pb.axhline(0.05)
    pb.axhline(0.01)
    pb.axhline(0.001)
    pb.yscale('log')
    pb.savefig(os.path.join(cwd, 'P_values_per_first_n_epochs_avr_{}_{}.png'.format(avr, paradigm)))

    pb.figure(figsize=(10, 10))


    x_data = []
    y_data = []
    y_err_data = []

    for n_trials, auc in data:
        x_data.append(n_trials)
        y_data.append(np.mean(auc.values()))
        y_err_data.append(scipy.stats.sem(auc.values()))
    pb.title("AVR = {}".format(avr))
    pb.xlabel('N epochs per example')
    pb.ylabel('AUCS, with std')
    pb.errorbar(x_data, y_data, yerr=y_err_data)
    pb.xlim([0, 100])
    pb.ylim([0, 1])
    pb.savefig(os.path.join(cwd, 'aucs_per_first_n_epochs_avr_{}_{}_std.png'.format(avr, paradigm)))


    pb.figure(figsize=(10, 10))
    x_data = []
    y_data = []
    y_err_data = []

    for n_trials, auc in dataP:
        x_data.append(n_trials)
        y_data.append(np.mean(auc.values()))
        y_err_data.append(scipy.stats.sem(auc.values()))
    pb.title("P_values = {}".format(avr))
    pb.xlabel('N epochs per example')
    pb.ylabel('P_values, with std')
    pb.errorbar(x_data, y_data, yerr=y_err_data)
    pb.xlim([0, 100])
    pb.axhline(0.05)
    pb.axhline(0.01)
    pb.axhline(0.001)
    # pb.ylim([0, 0.20])
    pb.yscale('log')
    pb.savefig(os.path.join(cwd, 'P_values_per_first_n_epochs_avr_{}_{}_std.png'.format(avr, paradigm)))








