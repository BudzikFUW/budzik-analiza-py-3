import sys
import os
import glob
from subprocess import check_call
from sorted_result import get_sorted_result
from get_groups import get_groups

def process_folder_pre(folders):

    for folder in folders:

        try:
            get_sorted_result(folder)
        except OSError:
            pass


        get_groups(os.path.join(folder, '_sorted'))

if __name__ == '__main__':
    cwd = os.path.dirname(os.path.abspath(__file__))
    folders = sys.argv[1:]
    process_folder_pre(folders)