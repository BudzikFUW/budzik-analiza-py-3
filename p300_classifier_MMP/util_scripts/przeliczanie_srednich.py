
import numpy as np
import numpy 
import sys, os

def sanitize_work_dir(work_dir):
    WORK_DIR = work_dir.rstrip(r'\/')
    if WORK_DIR.startswith('file:'):
        WORK_DIR = WORK_DIR[7:]
    return WORK_DIR


WORK_DIR = sys.argv[-1]
WORK_DIR = sanitize_work_dir(WORK_DIR)

folders = os.listdir(WORK_DIR)

work_dirname = os.path.basename(WORK_DIR)

aucs = []
p_vals = []
p_vals_from_aucs = []
n_targets = []
n_nontargets = []


all_preds = []
preds_per_patient = []

for folder in folders:

    preds = numpy.loadtxt(os.path.join(WORK_DIR, folder, 'predictions.txt'))
    all_preds.extend(preds[:, 0])
    preds_per_patient.append(preds[:, 0])
    
print "Srednia ze wszystkich:", np.mean(all_preds)
print "Odchylenie standardowe", np.std(all_preds, ddof=1), "odchylenie standardowe sredniej", np.std(all_preds, ddof=1)/np.sqrt(len(preds_per_patient[0])), "N =", len(preds_per_patient[0])

preds_per_patient = np.array(preds_per_patient)
print preds_per_patient.shape
print preds_per_patient.mean(axis=1).shape
print "Srednia srednich po pacjentach", preds_per_patient.mean(axis=1).mean(), "odchylenie standardowe srednich", np.std(preds_per_patient.mean(axis=1), ddof=1)
