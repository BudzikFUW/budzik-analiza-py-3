import pylab as pb
import numpy as np
import sys

f = sys.argv[-1]

data = np.loadtxt(f)

targets = 1-data[data[:, 1]==0][:,0]
nontargets = 1-data[data[:, 1]==1][:,0]

fig = pb.figure(figsize=(4,4), dpi=600)
ax = fig.add_subplot(111)
colors = ['black', 'lightgray']
ax.hist([targets, nontargets], bins=np.linspace(0,1,20), normed=0, histtype='bar', color=colors, label=['targets', 'nontargets'], linestyle=None)
pb.ylabel('Counts')
pb.xlabel('Probability')
pb.legend(loc=9)
fig.tight_layout()
fig.savefig('dec_hist_fancy.png', dpi=600)

