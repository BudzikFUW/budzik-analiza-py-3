
import matplotlib
matplotlib.use("Agg")
import pylab
import pandas as pd
import numpy as np
import sys
import matplotlib.pyplot as plt
import os
from itertools import product
import seaborn as sns

from collections import defaultdict

FILE = 'results_database.csv'
WORKFOLDER = os.path.dirname(FILE)

try:
	print(sys.argv)
	FILE = sys.argv[1]
	print('USING FILE:', FILE)
except IndexError:
	pass

WORKFOLDER = os.path.abspath(os.path.expanduser(os.path.dirname(FILE)))


def expand_xticks(d, ax):

	new_labels = []
	for label in ax.get_xticklabels():
		text = label.get_text()
		n = d[text].count()

		#~ import IPython
		#~ IPython.embed()

		text = '{} N={}'.format(text, n)
		label.set_text(text)
		new_labels.append(label)
	ax.set_xticklabels(new_labels)


def draw_boxplots_min_P(FILE):

	FILE = os.path.abspath(FILE)
	try:
		SUFFIX = os.path.splitext(os.path.basename(FILE))[0].split('_')[-1]
	except IndexError:
		SUFFIX = 'unknown'

	if not os.path.exists(WORKFOLDER):
		os.makedirs(WORKFOLDER)

	d = pd.read_csv(FILE)
	d['crs_group'] = d.diagnosis
	# d.crs_group.loc[(d.crs_group=='MCS-') | (d.crs_group=='MCS+')] = 'MCS'

	patients = d["id"].unique()
	measures = d["measure"].unique()
	d_max = pd.DataFrame(columns=d.columns)
	for p in patients:
		rounds = d["round"].loc[(d["id"]==p)].unique()
		for r in rounds:
			for measure in measures:
				p_vals = d["value"].loc[(d["id"]==p) & (d["round"]==r) & (d["measure"]==measure)]
				if len(p_vals) >= 1:
					if measure=="AUC":
						temp = d.loc[p_vals.argmax()]
					elif measure=="ManWhitU":
						temp = d.loc[p_vals.argmin()]
					elif measure=="min_cluster_p":
						temp = d.loc[p_vals.argmin()]
					to_append = temp.append(pd.Series({'nb_paradigms': len(p_vals)}))
					d_max = d_max.append(to_append, ignore_index=True)


				# s = set(d["paradigm"].loc[(d["id"]==p) & (d["round"]==r)].tolist())
				# if len(s & set(['p300_sl', 'p300_wz', 'p300_sl_tony', 'p300_cz']))==4:
				#     print "4 types: ", p, r
				# elif len(s & set(['p300_sl', 'p300_wz', 'p300_cz']))==3:
				#     print "3 types: ", p, r

	data_man = d_max[d_max['measure']=='ManWhitU']
	data_auc = d_max[d_max['measure']=='AUC']
	data_clust_p = d_max[d_max['measure']=='min_cluster_p']


	patients = d_max["id"].unique()
	l_labels = dict()
	for p in patients:
		try:
			nb_paradigms = data_auc.loc[data_auc["id"]==p].paradigm.values
			paradigm_names = ' '.join([str(i) for i in nb_paradigms]					)
			l_labels[p] = p + " " + paradigm_names
		except:
			pass

	sns.set_context("poster", font_scale=1.5)
	sns.axes_style("whitegrid")
	a = ["#e6194b","#3cb44b","#ffe119","#0082c8","#f58231","#911eb4","#46f0f0","#f032e6","#d2f53c","#008080",
		 "#342D7E","#aa6e28","#254117","#800000","#64E986","#808000","#EE9A4D","#000080","#808080","#000000",
		 "#8A4117"]
	order=['CONTROL_ADULT', 'CONTROL_CHILDREN', 'EMCS', 'MCS+', 'MCS-', 'UWS']

	if data_clust_p.size:
		f = plt.figure(figsize=(40, 22))
		ax = f.add_subplot(111)
		plt.title('AUC')
		sns.swarmplot(x='crs_group', y='value', data=data_clust_p, hue='id', palette=a, size=18, order=order)
		L = plt.legend(prop={'size': 18}, loc='center left', bbox_to_anchor=(0.92, 0.5))
		for t in L.get_texts():
			t.set_text(l_labels[t.get_text()])
		plt.axhline(y=0.5, linewidth=1.5, color='k')
		f.savefig(os.path.join(WORKFOLDER, "fig_CLUST_P_MIN.png"))


	f = plt.figure(figsize=(40, 22))
	ax = f.add_subplot(111)
	plt.title('AUC')
	sns.swarmplot(x='crs_group', y='value', data=data_auc, hue='id', palette=a, size=18, order=order)
	L = plt.legend(prop={'size': 18}, loc='center left', bbox_to_anchor=(0.92, 0.5))
	for t in L.get_texts():
		t.set_text(l_labels[t.get_text()])
	plt.axhline(y=0.5, linewidth=1.5, color='k')
	f.savefig(os.path.join(WORKFOLDER, "fig_AUC_max.png"))
	data_auc.to_csv(os.path.join(WORKFOLDER, "fig_AUC_max.csv"))

	f = plt.figure(figsize=(40, 22))
	ax = f.add_subplot(111)
	plt.title('ManWhitU')
	sns.swarmplot(x='crs_group', y='value', data=data_man, hue='id', palette=a, size=18, order=order)
	L = plt.legend(prop={'size': 18}, loc='center left', bbox_to_anchor=(0.92, 0.5))
	for t in L.get_texts():
		t.set_text(l_labels[t.get_text()])
	plt.axhline(y=0.05, linewidth=1.5, color='k')
	f.savefig(os.path.join(WORKFOLDER, "fig_MAN_max.png"))

	paradigms = d['paradigm'].unique()
	data_man = d[d['measure']=='ManWhitU']
	data_auc = d[d['measure']=='AUC']

	for exp in ['p300_sl', 'p300_wz', 'p300_sl_tony', 'p300_cz']:
		data_man_tmp = data_man[data_man['paradigm'] == exp]
		data_auc_tmp = data_auc[data_auc['paradigm'] == exp]
		f = plt.figure(figsize=(40, 18))
		ax = f.add_subplot(111)
		plt.title('AUC {}'.format(exp))
		sns.swarmplot(x='crs_group', y='value', data=data_auc_tmp, hue='id', palette=a, size=18, order=order)
		plt.legend(markerscale=2)
		plt.axhline(y=0.5, linewidth=1.5, color='k')
		f.savefig(os.path.join(WORKFOLDER, "fig_AUC_{}.png".format(exp)))

		f = plt.figure(figsize=(40, 18))
		ax = f.add_subplot(111)
		plt.title('ManWhitU {}'.format(exp))
		sns.swarmplot(x='crs_group', y='value', data=data_man_tmp, hue='id', palette=a, size=18, order=order)
		plt.legend(markerscale=2)
		plt.axhline(y=0.05, linewidth=1.5, color='k')
		f.savefig(os.path.join(WORKFOLDER, "fig_MAN_{}.png".format(exp)))

	for exp in ['p300_sl', 'p300_wz', 'p300_sl_tony', 'p300_cz']:
		data_man_tmp = data_man[data_man['paradigm'] == exp]
		data_auc_tmp = data_auc[data_auc['paradigm'] == exp]

		f = plt.figure(figsize=(40, 18))
		ax = f.add_subplot(111)
		plt.title('AUC {}'.format(exp))
		sns.boxplot(x='crs_group', y='value', data=data_auc_tmp, palette="PRGn", order=order)
		sns.swarmplot(x="crs_group", y="value", data=data_auc_tmp, color=".25", size=18, order=order)
		plt.axhline(y=0.5, linewidth=1.5, color='k')
		f.savefig(os.path.join(WORKFOLDER, "fig2_AUC_{}.png".format(exp)))

		f = plt.figure(figsize=(40, 18))
		ax = f.add_subplot(111)
		plt.title('ManWhitU {}'.format(exp))
		sns.boxplot(x='crs_group', y='value', data=data_man_tmp, palette="PRGn", order=order)
		sns.swarmplot(x="crs_group", y="value", data=data_man_tmp, color=".25", size=18, order=order)
		plt.axhline(y=0.05, linewidth=1.5, color='k')
		f.savefig(os.path.join(WORKFOLDER, "fig2_MAN_{}.png".format(exp)))


if __name__ == '__main__':
	for i in sys.argv[1:]:
		draw_boxplots_min_P(i)
	sys.exit(0)
	