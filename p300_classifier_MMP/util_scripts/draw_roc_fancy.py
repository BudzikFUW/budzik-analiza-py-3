import pylab as pb
import numpy as np
import sys

import sklearn.metrics
f = sys.argv[-1]
f2 = sys.argv[-2]

data = np.loadtxt(f)
data2 = np.loadtxt(f2)

fpr1, tpr1, trshlods1 = sklearn.metrics.roc_curve(data[:,1], data[:,0])
fpr2, tpr2, trshlods2 = sklearn.metrics.roc_curve(data2[:,1], data2[:,0])

fig = pb.figure(figsize=(4,4), dpi=600)
ax = fig.add_subplot(111)
colors = ['black', 'gray']
pb.plot(fpr1, tpr1, color=colors[0])
pb.plot(fpr2, tpr2, '--', color=colors[0])
pb.plot([0, 1], [0, 1], ':', color=colors[0])
pb.xlabel('FPR')
pb.ylabel('TPR')
fig.tight_layout()
fig.savefig('roc_fancy.png', dpi=600)

