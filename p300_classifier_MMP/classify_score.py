#coding: utf8
import json
import sys
import matplotlib
import time

from mne import concatenate_epochs
from sklearn.model_selection import StratifiedKFold

from clusterisation_selector import EEGClusterisationSelector
from custom_xdawn import CustomXdawn

matplotlib.use("Agg")
import mne

from sklearn.model_selection import LeaveOneOut
from sklearn.pipeline import make_pipeline
from balancer import balance_epochs, select_first_n_examples, select_random_n_examples
from averager import averager, averager_epochs
from downsample_transformer import DownsampleTransformer
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import roc_auc_score, roc_curve

from sklearn.svm import LinearSVC
import scipy.stats as st

from sklearn.utils import shuffle
import numpy as np
import os


BASELINE = -0.1

GLOBAL_DSSES = None
GLOBAL_PREPARED_DATA = None

class OUTPUT_DIR_CLASS(object):
    prefix = ''
    def __call__(self, average=3, first_n=None, random_n=False,):
        OUTPUT_DIR = os.path.join(os.path.expanduser(self.prefix), 'average_n_{}_first_n_{}'.format(average, first_n), os.path.basename(sys.argv[1]).split('.')[0])
        try:
            os.makedirs(OUTPUT_DIR)
        except OSError:
            pass
        return OUTPUT_DIR

    @classmethod
    def set_output_dir(cls, dir):
        cls.prefix = dir

OUTPUT_DIR = OUTPUT_DIR_CLASS()


def prepare_data(channels_to_use=None):
    global GLOBAL_DSSES
    global GLOBAL_PREPARED_DATA
    dses = sys.argv[1:]

    if dses == GLOBAL_DSSES:
        return GLOBAL_PREPARED_DATA

    datasets = []
    for ds in dses:
        dataset = mne.read_epochs(ds)

        try:
            dataset.drop_channels(['eog'])
        except:
            pass

        try:
            dataset.drop_channels(['Fpx'])
        except:
            pass

        dataset.apply_baseline(baseline=(0, 0.100))
        fs = dataset.info['sfreq']
        datasets.append(dataset)

    assert len(set([dataset.info['sfreq'] for dataset in datasets]))==1

    # normalise channels

    all_channels = [dataset.ch_names for dataset in datasets]
    channel_intersection = set.intersection(*[set(l) for l in all_channels])

    for dataset in datasets:
        dataset.pick_channels(list(channel_intersection))

    dataset = concatenate_epochs(datasets)
    classes = dataset.events[:, 2]

    if GLOBAL_DSSES is None:
        GLOBAL_DSSES = dses
        GLOBAL_PREPARED_DATA = (dataset, classes, fs, dataset.ch_names)

    if channels_to_use is not None:
        dataset.pick_channels(channels_to_use)

    return dataset, classes, fs, dataset.ch_names


def pre_process_epochs(data, classes, average):
    # pre processing
    if average>1:
        data, classeses = averager_epochs(data, classes, N=average)
    data, classes = balance_epochs(data, classes)
    return data, classes


def get_auc_roc(data_train, classes_train, data_test, classes_test, pipeline):
    pipeline.fit(data_train, classes_train)
    prediction = pipeline.predict_proba(data_test)[:, 1]

    auc = roc_auc_score(classes_test, prediction)
    return auc, prediction, classes_test, pipeline

def get_auc_roc_leave_one_out(data, classes, pipeline, average = 3, first_n=None, first_n_offset=0, validation ='loo'):
    if validation == 'loo':
        loo = LeaveOneOut()
    elif validation == 'kfold':
        loo = StratifiedKFold(n_splits=2)
    # loo.get_n_splits(data)

    predictions = []
    true_classes = []

    i = 0
    for train_index, test_index in loo.split(data, classes):
        data_train, data_test = data[train_index], data[test_index]
        classes_train, classes_test = classes[train_index], classes[test_index]


        pipeline.fit(data_train, classes_train)
        prediction = pipeline.predict_proba(data_test)
        print("prediction: ", prediction)
        print("real class: ", classes_test)

        predictions.extend(prediction[:, 1])
        true_classes.extend(classes_test)
        print("Done ", i, "out of", len(data))
        i += 1

    true_classes = np.array(true_classes)
    predictions = np.array(predictions)

    auc = roc_auc_score(true_classes, predictions)

    return auc, predictions, true_classes, pipeline


def draw_roc(average, true_classes, predictions, pipeline, auc, first_n, random_n):
    fpr, tpr, thrs = roc_curve(true_classes, predictions)
    import pylab
    f = pylab.figure()
    ax = f.add_subplot(111)
    ax.plot(fpr, tpr)
    ax.plot([0, 1], [0, 1])
    ax.set_title('AUC: {},\npipeline: {}'.format(auc, pipeline), fontsize=8)
    ax.set_xlabel('fpr')
    ax.set_ylabel('tpr')
    f.savefig(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n), 'ROC.png'))
    pylab.close(f)


def combine_predictions_rocs(predictions, true_classes):
    """Gets list of different predictions and true classes, combines to one AUC"""
    predictions = np.concatenate(predictions)
    true_classes = np.concatenate(true_classes)
    auc = roc_auc_score(true_classes, predictions)
    return auc, predictions, true_classes


def process_clusterisation_pipeline(fs, chnames, data, classes, average, first_n=None, first_n_offset=0, random_n=False,
                                    validation='loo'
                                    ):

    if first_n and not random_n:
        data, classes, additional_test_data, additional_test_classes = select_first_n_examples(data, classes, first_n, first_n_offset)
    if first_n and random_n:
        data, classes, additional_test_data, additional_test_classes = select_random_n_examples(data, classes, first_n,
                                                                                               first_n_offset)

    classifier = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')

    clusterization_selector = EEGClusterisationSelector(use_global_cache=True, chnames=chnames, Fs=fs, bas=BASELINE,
                                                        cluster_time_channel_mask=True)



    pipeline = make_pipeline(clusterization_selector, classifier)

    # fitting clusterisator on RAW data
    clusterization_selector.fit(data, classes)

    data, classes = pre_process_epochs(data, classes, average)

    # check for data vialability
    if clusterization_selector.fit_succesfull:

        print("CALCULATING REAL AUC")
        auc, predictions, true_classes, pipeline = get_auc_roc_leave_one_out(data, classes, pipeline, average=average, first_n=first_n, first_n_offset=first_n_offset, validation=validation)

        try:
            data_add_test, classes_add_test = pre_process_epochs(additional_test_data, additional_test_classes, average)
            auc_add, predictions_add, true_classes_add, pipeline = get_auc_roc(data,
                                                                                classes,
                                                                                data_add_test,
                                                                                classes_add_test,
                                                                                pipeline)
            auc, predictions, true_classes = combine_predictions_rocs([predictions, predictions_add],
                                                                      [true_classes, true_classes_add])
        except NameError:
            pass


    else:
        print("CANNOT DIFFERENTIATE! AUC == 0.5")
        auc = 0.5
        predictions = np.ones(len(classes)) * .5
        true_classes = classes

    return auc, predictions, true_classes, clusterization_selector, pipeline

class GlobalClassifier(LinearDiscriminantAnalysis):
    def fit(self,*args, **kwargs):
        import pickle
        if os.path.exists(os.path.expanduser('~/test_classifier_one_big.dump')):
            newObj = pickle.load(open(os.path.expanduser('~/test_classifier_one_big.dump')))
            self.__dict__.update(newObj.__dict__)
        else:
            LinearDiscriminantAnalysis.fit(self, *args, **kwargs)
            with open(os.path.expanduser('~/test_classifier_one_big.dump'), 'w') as dump:
                pickle.dump(self, dump)
        print("CLASSIFIER PARAMS:", self.get_params(), self.coef_, self.intercept_)

def process_downsampling_pipeline(fs, chnames, data, classes, average, first_n=None, first_n_offset=0, random_n=False,
                                  validation='loo', use_additional_data = True, window=[0, 1.1]
                                    ):

    if first_n and not random_n:
        data, classes, additional_test_data, additional_test_classes = select_first_n_examples(data, classes, first_n, first_n_offset)
    if first_n and random_n:
        data, classes, additional_test_data, additional_test_classes = select_random_n_examples(data, classes, first_n,
                                                                                               first_n_offset)
    if not use_additional_data:
        try:
            del additional_test_classes
            del additional_test_data
        except:
            pass


    # classifier = GlobalClassifier(solver='lsqr', shrinkage='auto')
    classifier = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    # classifier = LinearDiscriminantAnalysis(solver='svd', shrinkage=None)

    downsampler = DownsampleTransformer(Fs=fs, window=window)

    pipeline = make_pipeline(downsampler, classifier)

    # fitting clusterisator on RAW data

    data, classes = pre_process_epochs(data, classes, average)

    # check for data vialability

    auc, predictions, true_classes, pipeline = get_auc_roc_leave_one_out(data, classes, pipeline, average=average, first_n=first_n, first_n_offset=first_n_offset, validation=validation)

    try:
        data_add_test, classes_add_test = pre_process_epochs(additional_test_data, additional_test_classes, average)
        auc_add, predictions_add, true_classes_add, pipeline = get_auc_roc(data,
                                                                            classes,
                                                                            data_add_test,
                                                                            classes_add_test,
                                                                            pipeline)
        auc, predictions, true_classes = combine_predictions_rocs([predictions, predictions_add],
                                                                  [true_classes, true_classes_add])
    except NameError:
        pass



    return auc, predictions, true_classes, None, pipeline

def boot_strap_aucs(N=0, draw = True, average=3, first_n=None, first_n_offset=0, random_n=False, pipeline_type = 'clusterisation',
                    output_prefix='~/home_bach/results_for_clusterisation_ears_or_fz_cz_car_{}_exclude_bad_new_balancer_clusterisation_preanalysed_averager_fix_avr_control',
                    validation='loo', use_additional_data=True, window=[0, 1.1], channels_to_use=None):
    """Channels - to use None - for all, list of strings to select"""
    OUTPUT_DIR.set_output_dir(output_prefix)

    with open(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n, ), 'RESULTS.TXT'), 'w') as f:
        f.write('{}'.format(sys.argv))
        f.write('\n\r')
        data, classes, fs, chnames = prepare_data(channels_to_use)

        if pipeline_type == 'clusterisation':
            auc, predictions, true_classes, clusterization_selector, pipeline = process_clusterisation_pipeline(fs, chnames, data, classes, average,
                                                                                                                first_n=first_n,
                                                                                                                first_n_offset=first_n_offset,
                                                                                                                random_n = random_n,
                                                                                                                validation=validation

                                                                                                                )
        elif pipeline_type == 'downsampling':
            auc, predictions, true_classes, clusterization_selector, pipeline = process_downsampling_pipeline(fs,
                                                                                                                chnames,
                                                                                                                data,
                                                                                                                classes,
                                                                                                                average,
                                                                                                                first_n=first_n,
                                                                                                                first_n_offset=first_n_offset,
                                                                                                                random_n=random_n,
                                                                                                                validation=validation,
                                                                                                                use_additional_data=use_additional_data,
                                                                                                                window=window

                                                                                                                )

        draw_roc(average, true_classes, predictions, pipeline, auc, first_n=first_n, random_n=random_n)

        if pipeline_type == 'clusterisation':
            try:
                with open(os.path.join(OUTPUT_DIR(average,first_n=first_n, random_n=random_n), 'clusters.pkl'), 'wb') as fpick:
                    import pickle
                    pickle.dump(clusterization_selector.to_pickle, fpick)
            except:
                raise
            clusters = np.array(clusterization_selector.clusters, ndmin=3)
            np.save(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n), 'clusters.npy'), clusters)


        with open(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n), 'data.json'), 'w') as fdat:
            if pipeline_type == 'clusterisation':
                cluster_p_values = [float(i) for i in clusterization_selector.cluster_p_values]
            else:
                cluster_p_values = []
            class_counts = {i: len(data.events[data.events[:,2] == data.event_id[i]]) for i in data.event_id.keys()}
            json.dump({'auc': float(auc),
                       'cluster_p_values': cluster_p_values,
                       'fs': float(fs),
                       'baseline': float(BASELINE),
                       'chnames': chnames,
                       'class_counts': class_counts,
                       'window': window,
                       },

                      fdat, sort_keys=True, indent=3)


        f.write('AUC Predictions (probability of positive class):\n')
        for prediction in predictions:
            f.write('{}, '.format(prediction))
        f.write('\n')

        f.write('AUC True Classes:\n')
        for tru in true_classes:
            f.write('{}, '.format(tru))
        f.write('\n')

        print("GOT AUC: ", auc)
        f.write('{}\n\r'.format(auc))
        f.write('\n\r')
        f.flush()

        np.savetxt(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n), "predictions.txt"), np.hstack((predictions[:, None],true_classes [:, None])))

        aucs = []
        i = 0
        for shuf in range(N):
            classes_shuffled = shuffle(classes)#, random_state=42 + shuf)
            print("BOOTSTRAPING: {} out of {}".format(shuf, N))
            if pipeline_type == 'clusterisation':
                auc_boot, _, _, _, _ = process_clusterisation_pipeline(fs, chnames, data, classes_shuffled, average,
                                                                       first_n=first_n, first_n_offset=first_n_offset
                                                                       )
            if pipeline_type == 'downsampling':
                auc_boot, _, _, _, _ = process_downsampling_pipeline(fs, chnames, data, classes_shuffled, average,
                                                                       first_n=first_n, first_n_offset=first_n_offset
                                                                       )

            aucs.append(auc_boot)
            f.write('{}, '.format(auc_boot))
            f.flush()

        if aucs:
            p_value = st.percentileofscore(aucs, auc)
            f.write('\n\rP VALUE: {}, '.format(p_value))
            f.flush()

    if draw and aucs:
        import pylab
        f = pylab.figure()
        ax = f.add_subplot(111)
        ax.hist(aucs)
        ax.axvline(auc, color='red')
        f.savefig(os.path.join(OUTPUT_DIR(average, first_n=first_n, random_n=random_n), 'AUC_DISTR.png'))
        pylab.close(f)

    return auc




#calculate_roc_for_subject()
if __name__ == '__main__':
    boot_strap_aucs()

# data, classes, fs = prepare_data()
#
# mmp = MMP(fs=fs, flatten=True, cache_name='MD', natoms=10, energy_error=(0.1, 80.00))
#
# mmp.fit(data)
#
# transformed = mmp.transform(data, )
#
# print transformed.shape
#
# sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
#
# selected = sel.fit_transform(transformed)
#
# print selected.shape
