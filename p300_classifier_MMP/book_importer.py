# -*- coding: utf-8 -*-
#Marian Dovgialo 316445
from __future__ import print_function, division
import numpy as np
import collections
import pylab as pb
import matplotlib.gridspec as gridspec
import sys
import os

def get_type(ident, f):
    '''generacja dtype dla odczytania fragmentów, odpalana od razu,
    po odczytaniu identyfikatora'''
    if ident == 1:
        com_s = np.fromfile(f, '>u4', count=1)[0]
        if not com_s==0: ## comment
            return np.dtype([('comment', 'S'+str(com_s))])
        else:
            return None
            
    elif ident == 2: ## header
        head_s = np.fromfile(f, '>u4', count=1)[0]
        return None
        
    elif ident == 3: ## www adress
        www_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('www', 'S'+str(www_s))])
        
    elif ident == 4: ## data stworzenia pliku
        date_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('date', 'S'+str(date_s))])
        
    elif ident == 5: ## info o sygnale
        sig_info_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('Fs', '>f4'), ('ptspmV', '>f4'),
                        ('chnl_cnt', '>u2')])
                        
    elif ident == 6: ## info o dekompozycji
        dec_info_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('percent', '>f4'), ('maxiterations', '>u4'),
                        ('dict_size', '>u4'), ('dict_type', '>S1')])
                        
    elif ident == 10: #dirac
        atom_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('modulus', '>f4'), ('amplitude', '>f4'),
                        ('t', '>f4')])
                        
    elif ident == 11: #gauss
        atom_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('modulus', '>f4'), ('amplitude', '>f4'),
                        ('t', '>f4'), ('scale', '>f4')])
                        
    elif ident == 12: #sinus
        atom_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('modulus', '>f4'), ('amplitude', '>f4'),
                        ('f', '>f4'), ('phase', '>f4')])
                        
    elif ident == 13: #gabor
        atom_s = np.fromfile(f, '>u1', count=1)[0]
        return np.dtype([('modulus', '>f4'), ('amplitude', '>f4'),
                        ('t', '>f4'), ('scale', '>f4'),
                        ('f', '>f4'), ('phase', '>f4')])
                        
    else:
        return None
        
def get_signal(f,epoch_nr, epoch_s):
    '''zapuszczana przy odnalezieniu bloku sygnału'''
    sig_s = np.fromfile(f, '>u4', count=1)[0]
    chnl_nr = np.fromfile(f, '>u2', count=1)[0]
    signal = np.fromfile(f, '>f4', count= epoch_s)
    return chnl_nr, signal
    
def get_atoms(f):
    '''zapuszczana przy odnalezieniu bloku atomów'''
    atoms = collections.defaultdict(list)
    atoms_s = np.fromfile(f, '>u4', count=1)[0]
    a_chnl_nr = np.fromfile(f, '>u2', count=1)[0]
    ident = np.fromfile(f, '>u1', count=1)
    while ident in [10, 11, 12, 13]:
        atom = np.fromfile(f, get_type(ident[0], f), count=1)[0]
        atoms[ident[0]].append(atom)
        ident = np.fromfile(f, '>u1', count=1)
    f.seek(f.tell()-1)
    return atoms, a_chnl_nr
    
def draw_map(atoms, signal, df, dt, Fs, ptspmV, contour=0):
    ''' atoms - dict z podziałem na kanały, w których są dict z 
    podziałem na typ atomu, signal - macierz z próbkami sygnału,
    df - krok w częstościach dla dokładności rysowania, 
    dt - analogicznie dla czasu, Fs - częstość próbkowania,
    ptspmV - points per mili Volt'''
    tpr = len(signal)
    f = np.arange(0, Fs/2, df)
    t = np.arange(0, tpr/Fs, dt)
    lent = len(t)
    lenf = len(f)
    E = np.zeros((lent, lenf)).T
    t, f = np.meshgrid(t, f)
    sigt = np.arange(0, tpr/Fs, 1/Fs)
    for atom in atoms[13]:
        exp1 = np.exp(-2*(atom['scale']/Fs)**(-2)*(t-atom['t']/Fs)**2)
        exp2 = np.exp(-2*np.pi**2 *(atom['scale']/Fs)**2*(atom['f']*Fs/(2*np.pi)-f)**2) #debug Fs/(2*np.pi)
        wigners = ((atom['amplitude']/ptspmV)**2 * (2*np.pi)**0.5 * 
                    atom['scale']/Fs*exp1*exp2)
        E += atom['modulus']**2 * wigners
        #print(atom)
    for atom in atoms[12]:
        amp =  atom['modulus']**2*(atom['amplitude']/ptspmV)**2
        E[:,int(len(f)*atom['f']/(2*np.pi))] +=  amp
    for atom in atoms[11]:
        exp1 = np.exp(-2*(atom['scale']/Fs)**(-2)*(t-atom['t']/Fs)**2)
        exp2 = np.exp(-2*np.pi**2 *(atom['scale']/Fs)**2*(-f)**2)
        wigners = ((atom['amplitude']/ptspmV)**2 * (2*np.pi)**0.5 * 
                    atom['scale']/Fs*exp1*exp2)
        E += atom['modulus']**2 * wigners
    for atom in atoms[10]:
        amp =  atom['modulus']**2*(atom['amplitude']/ptspmV)**2
        E[int(lent*atom['t']/tpr)] +=  amp
        
        
    fig = pb.figure()
    gs = gridspec.GridSpec(2,1, height_ratios=[3,1])
    ax1 = fig.add_subplot(gs[0])
    ax1.set_ylabel(u'Częstotliwość, [Hz]')
    if contour:
        ax1.contour(t, f, E)
    else:
        ax1.pcolor(t, f, E)
    ax2 = fig.add_subplot(gs[1])
    ax2.plot(sigt, signal/ptspmV)
    ax2.set_ylabel(u'Amplituda, [$\\mu$V]')
    ax2.set_xlabel(u'Czas [s]')
    return E
    
def only_draw_map(t, f, E_a, sigt, signal_a, signal_recontruction_a, contour=False):
    fig = pb.figure()
    gs = gridspec.GridSpec(2,1, height_ratios=[3,1])
    ax1 = fig.add_subplot(gs[0])
    ax1.set_ylabel(u'Częstotliwość, [Hz]')
    ax1.set_title(sys.argv[-1])
    if contour:
        ax1.contour(t, f, E_a)
    else:
        ax1.pcolor(t, f, E_a)
    ax2 = fig.add_subplot(gs[1])
    ax2.plot(sigt, signal_a, 'red')
    ax2.plot(sigt, signal_recontruction_a, 'blue')
    ax2.set_ylabel(u'Amplituda, [$\\mu$V]')
    ax2.set_xlabel(u'Czas [s]')
    
def gabor(epoch_s,Fs,amplitude,position,scale,afrequency,phase):
    time = np.linspace(0, epoch_s/Fs, epoch_s)
   # time = np.arange(0, epoch_s)
    
    width = scale
    frequency = afrequency*2*np.pi
    #frequency = afrequency
    #print('position, width, frequency, amplitude, phase', position, width, frequency, amplitude, phase)
    signal = amplitude*np.exp(-np.pi*((time-position)/width)**2)*np.cos(frequency*(time-position) + phase)
    return signal
    
def reconstruct_signal(atoms, Fs, ptspmV, epoch_s, normalized = False):
    reconstruction = np.zeros(epoch_s)
    for atom in atoms[13]:
        position  = atom['t']/Fs
        width     = atom['scale']/Fs/2
        frequency = atom['f']*Fs/2
        if normalized:
            amplitude =  1
        else:
            amplitude = atom['amplitude']
        phase = atom['phase']
        
        reconstruction = reconstruction + gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
    return reconstruction
    
def reconstruct_template(atoms, Fs, ptspmV, epoch_s):
    reconstruction = np.zeros(epoch_s)
    for atom in atoms[13]:
        position  = atom['t']/Fs
        width     = atom['scale']/Fs/2
        frequency = atom['f']*Fs/2
        amplitude = 1
        phase     = atom['phase']
        
        reconstruction = reconstruction + gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
    return reconstruction
    
def calculate_map(atoms,signal, df, dt, Fs, ptspmV, contour=0, f_a = [0, 64.]):
    ''' atoms - dict z podziałem na kanały, w których są dict z 
    podziałem na typ atomu, signal - macierz z próbkami sygnału,
    df - krok w częstościach dla dokładności rysowania, 
    dt - analogicznie dla czasu, Fs - częstość próbkowania,
    ptspmV - points per mili Volt'''
    tpr = len(signal)
    print (Fs, df)
    f = np.arange(f_a[0], f_a[1], df)
    t = np.arange(0, tpr/Fs, dt)
    lent = len(t)
    lenf = len(f)
    E = np.zeros((lent, lenf)).T
    t, f = np.meshgrid(t, f)
    sigt = np.arange(0, tpr/Fs, 1/Fs)
    for atom in atoms[13]:
        exp1 = np.exp(-2*(atom['scale']/Fs)**(-2)*(t-atom['t']/Fs)**2)
        #~ exp2 = np.exp(-2*np.pi**2 *(atom['scale']/Fs)**2*(atom['f']*Fs/(2*np.pi)-f)**2) #debuggg w Hertzach czy w radianach
        exp2 = np.exp(-2*np.pi**2 *(atom['scale']/Fs)**2*(atom['f']*Fs/2-f)**2)
        wigners = ((atom['amplitude']/ptspmV)**2 * (2*np.pi)**0.5 * 
                    atom['scale']/Fs*exp1*exp2)
        E += atom['modulus']**2 * wigners
        #~ print(atom)
    for atom in atoms[12]:
        amp =  atom['modulus']**2*(atom['amplitude']/ptspmV)**2
        E[:,int(len(f)*atom['f']/(2*np.pi))] +=  amp
    for atom in atoms[11]:
        exp1 = np.exp(-2*(atom['scale']/Fs)**(-2)*(t-atom['t']/Fs)**2)
        exp2 = np.exp(-2*np.pi**2 *(atom['scale']/Fs)**2*(-f)**2)
        wigners = ((atom['amplitude']/ptspmV)**2 * (2*np.pi)**0.5 * 
                    atom['scale']/Fs*exp1*exp2)
        E += atom['modulus']**2 * wigners
    for atom in atoms[10]:
        amp =  atom['modulus']**2*(atom['amplitude']/ptspmV)**2
        E[int(lent*atom['t']/tpr)] +=  amp
        
        
    return t, f, E, 

def read_book(f):
    filename = f
    try:
        f = open(f, 'rb')
    except Exception:
        f = f

    version = np.fromfile(f, 'S6', count=1)
    # print('version: ',version[0])
    data = {} # zachowanie danych z nagłówku
    ident = np.fromfile(f, 'u1', count=1)[0]
    ct = get_type(ident, f)
    signals = collections.defaultdict(list)
    atoms = collections.defaultdict(list)
    while ident:
        if f.tell() == os.path.getsize(filename):
            break

        #~ print(ident)
        if ct:
            point = np.fromfile(f,ct, count=1)[0]
            data[ident] = point
        elif ident ==7:
            data_s = np.fromfile(f, '>u4', count=1)[0]
            epoch_nr = np.fromfile(f, '>u2', count=1)[0]
            epoch_s = np.fromfile(f, '>u4', count=1)[0]
            #print('epoch_s', epoch_s)
        elif ident == 8:
            chnl_nr, signal = get_signal(f, epoch_nr, epoch_s)
            signals[epoch_nr].append([chnl_nr, signal])
    
        elif ident == 9:
            atom, a_chnl_nr = get_atoms(f)
            atoms[a_chnl_nr] = atom

        if f.tell() == os.path.getsize(filename):
            break
        ident = np.fromfile(f, '>u1', count=1)
        if ident:
            ident = ident[0]
        if f.tell() == os.path.getsize(filename):
            break
        ct = get_type(ident, f)
    return data, signals, atoms, epoch_s

def show_results(data, signals, atoms, dt, df, contour=0):
    print('adress', data[3]['www'])
    print('decomp date:', data[4]['date'])
    
    Fs = data[5]['Fs']
    print('Fs', Fs)
    print('procent wyjaśnionych danych:', data[6]['percent'])
    print('max iteracji:', data[6]['maxiterations'])
    ptspmV = data[5]['ptspmV']
    signal = signals[1][0][1] #epocha, lista z wczytanych sygnałów, składa-
                            # jąca się z list [nr kanału, array sygnału]
   
    print(len(atoms[1][13]))
    E = draw_map(atoms[1], signal, dt, df, Fs, ptspmV,
    contour=contour)

def calculate_mean_map(atoms, signals, df = 0.05, dt = 1/128., Fs = 128., ptspmV=1, f_a = [0, 64.]):
    N = len(atoms.keys())
    print(N, atoms.keys())
    tpr = len(signals[1][0][1])
    sigt = np.arange(0, tpr/Fs, 1/Fs)
    for nr, chnl in enumerate(atoms.keys()):
        print(chnl)
        t, f, E, = calculate_map(atoms[chnl],signals[1][nr][1], df, dt, Fs, ptspmV, f_a = f_a)
        signal_reconstruction = reconstruct_signal(atoms[chnl], Fs, ptspmV, epoch_s)
        signal = signals[1][nr][1]
        print(signal)
        
        try:
            signal_a += signal
            E_a += np.log(E)
            signal_recontruction_a += signal_reconstruction
        except Exception as a:
            print(a, 'objection')
            signal_a = signal
            E_a = np.log(E)
            signal_recontruction_a = signal_reconstruction
    signal_a /= N
    signal_recontruction_a /= N
    E_a /= N
    return t, f, E_a, sigt, signal_a, signal_recontruction_a
    

if __name__ == '__main__':
    args = sys.argv
    if len(sys.argv) == 1:
        fstr = '../sygnaly_moje/ERRP_Ala_1/matching_pursuits/SMP_grand_average_100_atomow/mp_output/mp_cache_type_incor_chnl_Fz_smp.b'
        fstr = '/home/marian/Uniwersteckie studia/licencjat/sygnaly_moje/ERRP_Ala_1/matching_pursuits/grand_aberage -0.5 - 1 _mmp3/dillation 1.1/mp_cache_type_incor_chnl_Fz_mmp.b'
        fstr = '/home/marian/Uniwersteckie studia/licencjat/sygnaly_moje/ERRP_Ala_1/matching_pursuits/grand_average_1000_atomow_-05.---1_SMP/mp_cache_type_incor_chnl_Fz_smp.b'
        #~ fstr = '/home/marian/Uniwersteckie studia/licencjat/sygnaly_moje/ERRP_Ala_1/matching_pursuits/50_Hz_test.b'

        print('opening predefined file:', fstr)
    elif len(sys.argv) == 2:
        fstr = sys.argv[-1]
    else:
        print(' ##### \n too much arguments \n #####')
    f = open(fstr, 'rb')
    data, signals, atoms, epoch_s = read_book(f)
    print(data)
    #~ print(signals[1][0])
    #~ sys.exits()
    df = 1
    dt = 0.01
    
    Fs = data[5]['Fs']
    ptspmV = 1.
    f_a = [5, 12]
    t, f, E_a, sigt, signal_a, signal_reconstruction_a = calculate_mean_map(atoms, signals, df = 0.2, dt = 0.008, f_a = f_a, Fs = 128.)
    #~ t, f, E_a, sigt, signal_a = calculate_map(atoms, signals, df = 0.05, dt = 0.01, f_a = [0, 50])
    #~ signal_recontruction_a = 
    #~ t, f, E_a, = calculate_map(atoms[1],signals[1][0][1], df, dt, Fs, ptspmV, f_a = f_a)
    #~ atoms,signal, df, dt, Fs, ptspmV, contour=0, f_a = [0, 64.]
    #~ tpr = len(signals[1][0][1])
    #print('sig epochs/Fs', epoch_s/Fs, Fs)
    #~ sigt = np.arange(0, epoch_s/Fs, 1/Fs)
    #~ signal_a = signals[1][0][1]
    #~ 
    #~ signal_reconstruction_a = reconstruct_signal(atoms[1], Fs, ptspmV, epoch_s)
    #~ 
    only_draw_map(t, f, E_a, sigt, signal_a, signal_reconstruction_a)
    #~ 
    #~ show_results(data, signals, atoms, 0.01,
                #~ 1/(data[5]['Fs']/2),  contour=1) # mapa conturowa z
                                                #powodów szybkości
    pb.show()
