#!/usr/bin/python2
# coding: utf-8
import numpy as np
import scipy.stats as st


# poziom p, dla którego obliczamy teoretyczny AUC
p = 0.05  # p = 5%

# liczebności grup (liczba targetów dzielona na 3, bo uśredniamy po 3)
n = 600  # liczba targetów
n1 = int(np.floor(n))
n2 = int(np.floor(n))

meanrank = n1*n2 / 2
sd = np.sqrt(n1 * n2 * (n1+n2+1) / 12.0)  # bez tiecorrection, ale powinno być ok


z = st.norm.ppf(1-p)  # test dwustronny
u = z*sd + meanrank + 0.5
print u

auc = u / (n1 * n2)

print z, auc

r3 = 2*u/(n1*n2)-1

# niekierkunkowa rangowo-dwuseryjna korelacja (wielkość efektu)
# H.W. Wendt. Dealing with a common problem in social science: A simplified rank-biserial coefficient of correlation based on the U statistic. „European Journal of Social Psychology”. 2 (4), s. 463–465, 1999. DOI: 10.1002/ejsp.2420020412.
u = meanrank - z*sd
r = 1 - 2*u/(n1*n2)

print r


r2 = 2*auc-1

print r2



print r3