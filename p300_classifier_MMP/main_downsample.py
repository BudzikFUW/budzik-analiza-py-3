#!/usr/bin/env python3
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=1, pipeline_type='downsampling',
                output_prefix='/repo/coma/results/mdovgialo/Tue-Jan-18-17:25:09-2022/n400-slowa/p300_AUC/')
