import os
import uuid

import numpy as np
import subprocess
from sklearn.base import TransformerMixin
from collections import namedtuple
from book_importer import read_book, gabor
import tempfile
OLDMP = True


Book = namedtuple('MMPBook', ['data', 'signals', 'atoms', 'epoch_s'])
AMPS = 0
PHASES = 1
FREQUENCIES = 2
WIDTHS = 3
POSITIONS = 4


def optimal_prod(cut, atoms, Fs, ptspmV, epoch_s, amp=True, ph=False,):
    atoms_opt_parameters = []
    time = np.linspace(0, epoch_s / Fs, epoch_s)
    # dopasowanie
    amps = []
    phases = []
    frequencies = []
    widths = []
    positions = []
    for nr, atom in enumerate(atoms[13]):
        position = atom['t'] / Fs
        width = atom['scale'] / Fs
        frequency = atom['f'] * Fs / 2
        # ~ print('atom nr', nr)
        # ~ print('position, width, frequency', position, width, frequency)
        if ph:
            C = gabor(epoch_s, Fs, 1, position, width, frequency, 0, )
            S = gabor(epoch_s, Fs, 1, position, width, frequency, np.pi / 2, )
            phase = np.arctan((np.dot(cut, S) / np.dot(S, S)) / (np.dot(cut, C) / np.dot(C, C)))
            # ~ phase = np.linspace(-2*np.pi, 2*np.pi, 10000)[:,None]
            # ~ gs = gabor(epoch_s,Fs,1,position,width,frequency,phase,)
            # ~ phase = phase[np.sum((gs-cut)**2, axis=1).argmin()]
        else:
            phase = atom['phase']
            # ~ phases.append(phase/(2*np.pi*frequency)+position)

        if amp:

            g = gabor(epoch_s, Fs, 1, position, width, frequency, phase, )
            amplitude = 1 / np.sum(g ** 2)
            # ~ print('amp', amplitude)
            g = gabor(epoch_s, Fs, amplitude, position, width, frequency, phase, )
            amplitude = np.dot(cut, g)
            # ~

            # ~ amplitude = np.linspace(0, np.max(np.abs(cut))*1.5, 1000)[:,None]
            # ~ gs = gabor(epoch_s,Fs,amplitude,position,width,frequency,phase,)
            # ~ amplitude = amplitude[np.sum((gs-cut)**2, axis=1).argmin()]
        else:
            amplitude = atom['amplitude']
        amps.append(amplitude)
        # ~ phases.append(phase)
        phases.append(phase)
        frequencies.append(frequency)
        widths.append(width)
        positions.append(position)

        cut -= gabor(epoch_s, Fs, amplitude, position, width, frequency, phase,)
        atoms_opt_parameters.append({'amplitude': amplitude, 'phase': phase})
    return amps, phases, frequencies, widths, positions


class MMP(TransformerMixin, object):

    cache_folder = os.path.expanduser('~/mmp_cache/')

    def __init__(self, mmp='MMP3', energy_error=(0.1, 90.00), fs=128, natoms=10, chnls=None, cache_name=None,
                 opt_amp=True, opt_ph=False, flatten=True, use_temp = True, hide = True):
        self.fitted = False
        self.mmp = mmp
        self.energy_error = energy_error
        self.fs = fs
        self.natoms = natoms
        self.chnls = chnls
        self.cache_name = cache_name
        self.opt_amp = opt_amp
        self.opt_ph = opt_ph
        self.flatten = flatten
        self.use_temp = use_temp
        self.hide = hide
    def __repr__(self):
        return "<MP Transformaer {}>".format(self.mmp)

    def fit(self, data, classes):
        # data array (epochs, channels, sammple)
        self.cache_id = self.cache_name if self.cache_name else str(uuid.uuid4())
        if self.use_temp:
            cache_folder = str(tempfile.mkdtemp())
            base_path = os.path.join(cache_folder, self.cache_id)
        else:
            base_path = os.path.join(self.cache_folder, self.cache_id)
        mp_cache_path = os.path.join(base_path, 'mp_cache')

        self.books_per_chnls = []
        self.books_per_chnls_filenames = []

        N_realisations = data.shape[0]
        len_cut = data.shape[2]
        self.len_cut = len_cut
        self.nchnls = data.shape[1]
        print("NCHNLS:", self.nchnls)
        print("N_realisations:", N_realisations)



        if self.chnls is None:
            self.chnls = list(range(data.shape[1]))

        if not os.path.exists(mp_cache_path):

            mp_out_path = mp_cache_path

            if not os.path.exists(mp_cache_path):
                os.makedirs(mp_cache_path)
            if not os.path.exists(mp_out_path):
                os.makedirs(mp_out_path)

            self._processes = []

            for nr, chnl in enumerate(self.chnls):
                cache_mp_filename = os.path.join(mp_cache_path, 'mp_cache_' + 'type_{}_chnl_{}'.format(self.mmp, chnl) + '.bin')
                flatten = np.resize(data[classes==0][:, nr, :].T, (len_cut * N_realisations, 1))

                flatten.astype(np.float32).tofile(cache_mp_filename)
                mp_config = '''# OBLIGATORY PARAMETERS
            nameOfDataFile         {}
            nameOfOutputDirectory  {}
            writingMode            CREATE
            samplingFrequency      {}
            numberOfChannels       {}
            selectedChannels       1-{}
            numberOfSamplesInEpoch {}
            selectedEpochs         1
            typeOfDictionary       OCTAVE_FIXED
            energyError         {} {}
            randomSeed             auto
            reinitDictionary       NO_REINIT_AT_ALL
            maximalNumberOfIterations {}
            energyPercent             95.0
            MP                        {}
            scaleToPeriodFactor       1.0
            pointsPerMicrovolt        1.0

            # ADDITIONAL PARAMETERS

            diracInDictionary         NO
            gaussInDictionary         NO
            sinCosInDictionary        NO
            gaborInDictionary         YES
            progressBar               ON
            '''.format(os.path.basename(cache_mp_filename), mp_out_path+'/', self.fs, int(N_realisations), int(N_realisations),
                       len_cut, self.energy_error[0], self.energy_error[1], self.natoms, self.mmp)
                config_filename = os.path.join(mp_cache_path, 'mp_config_{}.txt'.format(nr))
                configfile = open(config_filename, 'w')
                configfile.write(mp_config)
                configfile.close()
                _comm = "mp5-amd64 -f {}".format(config_filename)
                _comm2 = "empi -f {}".format(config_filename)

                if OLDMP:
                    comm = _comm
                else:
                    comm = _comm2
                if self.hide:
                    comm += ' > /dev/null'
                self._processes.append(subprocess.Popen(comm, shell=True, cwd=os.path.dirname(cache_mp_filename)))
                cached_book_path = cache_mp_filename[:-4] + '_mmp.b'
                self.books_per_chnls_filenames.append(cached_book_path)

            for chnl, i in enumerate(self._processes):
                i.wait()
            for chnl, i in enumerate(self._processes):
                #self.books_per_chnls.append(BookImporter(self.books_per_chnls_filenames[chnl]))
                try:
                    self.books_per_chnls.append(Book(*read_book(self.books_per_chnls_filenames[chnl])))
                except IndexError as e:
                    import traceback
                    traceback.print_exc()
                    print e
                    print("WARNING INDEX ERROR")
                    print(base_path)
                    os.system("rm -rf %s" % base_path )
                    return self.fit(data, classes)



        else:
            for nr, chnl in enumerate(self.chnls):
                cache_mp_filename = os.path.join(mp_cache_path,
                                                 'mp_cache_' + 'type_{}_chnl_{}'.format(self.mmp, chnl) + '.bin')
                cached_book_path = cache_mp_filename[:-4] + '_mmp.b'
                self.books_per_chnls_filenames.append(cached_book_path)

                #self.books_per_chnls.append(BookImporter(self.books_per_chnls_filenames[chnl]))
                try:
                    self.books_per_chnls.append(Book(*read_book(cached_book_path)))
                except IndexError:
                    print(base_path)
                    os.system("rm -rf %s" % base_path)
                    return self.fit(data, classes)
        if self.use_temp:
            os.system("rm -rf %s" % cache_folder)
        self.fitted = True
        return self


    def transform(self, data, ):
        # data array (epochs, channels, sammple)
        # must have the same channels and samples as fitted
        # returns transformed data:
        # shape (epochs x channels x PARAMs x atoms]) if flatten == False
        # if flatten == True, it will be 2d array:
        # (epoch x (channels * PARAMS * atoms))
        # there are 5 params - amplitude, phase, frequency, width and positions

        assert self.fitted == True
        assert data.shape[1] == self.nchnls
        assert data.shape[2] == self.len_cut
        # amps, phases, frequencies, widths, positions
        t_data = np.zeros((data.shape[0], self.nchnls, 5, self.natoms))

        for chnl in range(self.nchnls):
            for cut_nr in range(data.shape[0]):
                cut = data[cut_nr, chnl, :]
                # 'data', 'signals', 'atoms', 'epoch_s'
                book = self.books_per_chnls[chnl]
                atoms = book.atoms
                data_book = book.data
                epoch_s = book.epoch_s
                amps, phases, frequencies, widths, positions = optimal_prod(cut, atoms[1], data_book[5]['Fs'], 1., epoch_s, amp=self.opt_amp,
                                                                  ph=self.opt_ph)
                for atom_nr in range(self.natoms):
                    t_data[cut_nr, chnl, AMPS, atom_nr] = amps[atom_nr]
                    t_data[cut_nr, chnl, PHASES, atom_nr] = phases[atom_nr]
                    t_data[cut_nr, chnl, FREQUENCIES, atom_nr] = frequencies[atom_nr]
                    t_data[cut_nr, chnl, WIDTHS, atom_nr] = widths[atom_nr]
                    t_data[cut_nr, chnl, POSITIONS, atom_nr] = positions[atom_nr]
        if self.flatten:
            flattened_features = np.zeros((data.shape[0], 5 * self.natoms * self.nchnls))
            for i in range(data.shape[0]):
                flattened_features[i] = t_data[i, :, :, :].flatten()
            return flattened_features
        else:
            return t_data
