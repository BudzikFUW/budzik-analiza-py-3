#!/usr/bin/env python3
# coding: utf8
from classify_score import boot_strap_aucs
import numpy as np


from balancer import FirstNTooBigException

AVERAGE = 1

for nr, i in enumerate(range(1, 6, 1)):
    try:
        boot_strap_aucs(average=i, first_n=None, output_prefix="/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_n_usrednien/wyniki_dane/downsampling/dane_wszyscy_shrinkage_true", pipeline_type='downsampling')
    except FirstNTooBigException:
        print "FINISHED"
        break
    except (ValueError, UserWarning, RuntimeWarning) as e:
        print
        print
        print
        print "Skipping ", i, 'Error:',  e
        pass

