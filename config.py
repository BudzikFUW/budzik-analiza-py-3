# coding: utf-8

import os
import time

if os.path.exists("/repo/coma/results"):
    main_outdir = os.path.join("/repo/coma/results", os.path.basename(os.path.expanduser("~")), time.ctime().replace(" ", "-"))
else:
    main_outdir = os.path.join(os.path.expanduser("~/budzik_results").replace("dmj/fizmed", "home"))

bad_chnls_db = './bad_channels_db.json'

# przypisanie nazw katologów do tur
tour_dates = {"tura_1": ('2016-07', '2016-10'),
              "tura_2": ('2016-12', '2017-01', '2017-02'),
              "tura_3": ('2017-05', '2017-06'),
              "tura_4": ('2017-08', '2017-09'),
              "tura_5": ('2017-10', '2017-11')
              }

fontsize = 26
figure_scale = 10


def create_outdir_name(filepath):
    patient_name_path = os.path.dirname(filepath)
    paradigm_name_path = os.path.dirname(patient_name_path)

    patient_name = os.path.basename(patient_name_path)
    paradigm_name = os.path.basename(paradigm_name_path)
    date = os.path.basename(os.path.dirname(paradigm_name_path))

    for tour in tour_dates:
        if date in tour_dates[tour]:
            break  # szukam tury, która zawiera i wtedy kończę przeszukiwanie
        else:  # w przeciwnym razie pozostaje mi:
            tour = 'nieznana_tura'

    outdir = ""
    if os.path.exists("outdir_path.temp"):
        with open("outdir_path.temp", "r") as f:
            outdir = f.readline().strip()

    if not outdir:
        outdir = main_outdir

    return os.path.join(outdir, paradigm_name, patient_name, tour)

