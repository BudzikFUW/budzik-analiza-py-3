import sys
import tqdm
import numpy as np
from eeg_artifacts.obci.analysis.obci_signal_processing import read_manager
from eeg_artifacts.helper_functions import montage_ears
from eeg_artifacts.helper_functions import mgr_filter, get_microvolt_samples
from eeg_artifacts.artifacts_mark import artifact_detection
from eeg_artifacts.artifacts_mark import art_tags_saver
# https://gitlab.com/BudzikFUW/eeg_artifacts/
selected_channels = [u'Fp1', u'Fp2', u'AFz', u'F7', u'F3', u'Fz', u'F4', u'F8', u'T7', u'C3', u'Cz',
                         u'C4', u'T8', u'P7', u'P3', u'Pz', u'P4', u'P8', u'O1', u'O2', u'T3', u'T4', u'T5', u'T6']


def main(files):
    for path_to_raw in tqdm.tqdm(files, desc='marking artifacts'):
        core_of_path = path_to_raw.strip()[:-4]
        print("reading file...")
        print(path_to_raw)
        eeg_rm = read_manager.ReadManager(core_of_path + '.xml', core_of_path + '.raw', core_of_path + '.tag')
        channels_names = eeg_rm.get_param('channels_names')

        print("preprocessing...")
        fs = float(eeg_rm.get_param('sampling_frequency'))

        eeg_rm.set_samples(get_microvolt_samples(eeg_rm), eeg_rm.get_param('channels_names'))
        eeg_rm.set_param('channels_gains', np.ones(len(channels_names)))

        drop_chnls = [u'EOGL', u'EOGR', u'EMG', u'EKG', u'RESP', u'RESP_UP', u'RESP_U', u'RESP_DOWN', u'RESP_D',
                      u'TERM', u'SaO2', u'Pleth', u'HRate', u'Saw', u'Driver_Saw', u'Sample_Counter']
        try:
            x_ref = montage_ears(eeg_rm, 'A1', 'A2', exclude_from_montage=drop_chnls)
        except ValueError:
            try:
                x_ref = montage_ears(eeg_rm, 'M1', 'M2', exclude_from_montage=drop_chnls)
            except ValueError:
                print("WARNING! Cannot mount to ears for file {}".format(path_to_raw))
                continue
        eeg_rm = x_ref

        available_channels = eeg_rm.get_param('channels_names')

        print("marking artifacts...")
        art_tagfile_path = core_of_path.rstrip(".obci") + '_artifacts.obci.tag'

        tags = artifact_detection(eeg_rm, selected_channels, outpath=art_tagfile_path)

        print('tagfile_path:', art_tagfile_path)
        art_tags_saver(art_tagfile_path, tags)


if __name__ == "__main__":
    files = sys.argv[1:]
    if sys.argv[-2] == '-l':
        files = [line for line in open(sys.argv[-1]).readlines()]
    main(files)