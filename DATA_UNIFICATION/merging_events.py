import pandas
import numpy as np


def merge_measurements(frame):
    """Merges the same measuerments split into different bloks (mainly p300)"""
    frame = frame.copy()
    frame['measurement_date'] = pandas.to_datetime(frame.measurement_date)

    max_time_delta = 15

    columns = ['person_id', 'diag', 'measurement_type', 'measurement_date', 'files_paths', 'fif_file_paths']
    compressed_measurements = []

    for person in set(frame.person_id):
        for measurement_type in set(frame.measurement_type):
            subframe_measurement_type = frame[frame.measurement_type==measurement_type]
            subframe = subframe_measurement_type[subframe_measurement_type.person_id==person]
            last = None
            to_compress = []

            for value in np.array(subframe.sort_values('measurement_date', ascending=True)):
                if last is None:
                    last = value
                    to_compress.append(value)
                    continue
                delta = value[3] - last[3]
                if delta.days < max_time_delta:
                    to_compress.append(value)
                    last = value
                else:
                    last = value
                    first = to_compress[0]
                    file_paths = [i[4] for i in to_compress]
                    fifs_paths = sum([i[5] for i in to_compress], [])
                    compressed = [first[0], first[1], first[2], first[3], file_paths, fifs_paths]
                    compressed_measurements.append(compressed)
                    to_compress = [value]
            if len(to_compress) > 0:
                first = to_compress[0]
                file_paths = [i[4] for i in to_compress]
                fifs_paths = sum([i[5] for i in to_compress], [])
                compressed = [first[0], first[1], first[2], first[3], file_paths, fifs_paths]

                compressed_measurements.append(compressed)
    return pandas.DataFrame.from_records(compressed_measurements, columns=columns)
