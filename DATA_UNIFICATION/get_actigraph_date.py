import sys
from datetime import datetime


def search_first_pagetime(actigraph_file):
    searching = True
    while searching:
        line = actigraph_file.readline()
        if line.startswith('Page Time'):
            return line.split(':', 1)[1]


def get_actigraph_date(filename):
    with open(filename) as actigraph:
        date = search_first_pagetime(actigraph)
        return datetime.strptime(date.rsplit(':', 1)[0], '%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    filename = sys.argv[-1]
    print(get_actigraph_date(filename))