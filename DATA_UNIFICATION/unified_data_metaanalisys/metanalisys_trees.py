import pandas
import sys

import numpy as np
import pandas as pd
import pylab as pb
from collections import defaultdict

from help_functions import merge_measurements_records
import xgboost as xgb
from os import makedirs
from sklearn.model_selection import LeaveOneOut
from tqdm import tqdm
import seaborn
from pandas_ml import ConfusionMatrix


dtf_rs_features = ['posterior_out_theta',
 'frontal_out_beta',
 'frontal_in_delta',
 'back2front_delta',
 'front2back_delta',
 'left_in_alpha',
 'posterior_in_delta',
 'left_in_delta',
 'frontal_out_alpha',
 'right2left_alpha',
 'right2left_delta',
 'back2front_beta',
 'right_out_beta',
 'right_out_delta',
 'posterior_out_alpha',
 'left_in_theta',
 'posterior_in_theta',
 'left_out_theta',
 'skull',
 'central_in_delta',
 'left_out_beta',
 'left2right_alpha',
 'left_out_delta',
 'frontal_in_alpha',
 'central_out_alpha',
 'posterior_out_beta',
 'posterior_in_alpha',
 'back2front_theta',
 'back2front_alpha',
 'right_out_theta',
 'right_in_theta',
 'posterior_in_beta',
 'posterior_out_delta',
 'right2left_theta',
 'right2left_beta',
 'right_in_beta',
 'front2back_alpha',
 'front2back_theta',
 'frontal_in_beta',
 'central_in_alpha',
 'central_in_theta',
 'left2right_beta',
 'right_in_alpha',
 'central_out_theta',
 'left_in_beta',
 'left2right_delta',
 'front2back_beta',
 'frontal_in_theta',
 'left2right_theta',
 'central_in_beta',
 'frontal_out_theta',
 'right_in_delta',
 'frontal_out_delta',
 'central_out_beta',
 'left_out_alpha',
 'central_out_delta',
 'right_out_alpha']

active_features = ['auc_global_local_local_early',
 'auc_global_local_local_early_perc',
 'auc_global_local_global_late',
 'auc_global_local_global_late_perc',
 'auc_sluchowe_slowa',
 'auc_sluchowe_slowa_perc',
 'auc_czuciowe',
 'auc_czuciowe_perc',
 'auc_wzrokowe',
 'auc_wzrokowe_perc',
 'auc_sluchowe_tony',
 'auc_sluchowe_tony_perc',
 'C3_foot_erd_alpha_p',
 'C4_foot_erd_alpha_p',
 'C4_hand_erd_alpha_p',
 'C3_hand_erd_alpha_p',
 'Cz_foot_erd_alpha_p',
 'Cz_hand_erd_alpha_p']


def learn_trees(dataframe_path, percentiles_path=None, crs_convertion_dict={}):

    crs_convertion_dict_back = defaultdict(str)
    for key, value in sorted(crs_convertion_dict.items(), key=lambda x: (x[1], x[0])):
        crs_convertion_dict_back[value] += key + ' '

    categories = [i[1].strip() for i in sorted(crs_convertion_dict_back.items())]
    folder_name = '---'.join(categories)
    outfolder = 'out_trees_pool_{}'.format(folder_name).replace('/', '+')

    dataframe = pd.DataFrame.from_csv(dataframe_path)

    features_to_drop_due_to_no_correlations = []

    if percentiles_path:
        percentiles = pd.DataFrame.from_csv(percentiles_path)
        percentiles = percentiles.drop('diag')['diag']
        good_percentile_lower = (percentiles <= 2.5)
        good_percentile_upper = (percentiles >= 97.5)
        drop = list(percentiles[~(good_percentile_upper | good_percentile_lower)].axes[0])
        features_to_drop_due_to_no_correlations.extend(drop)

    makedirs('{}'.format(outfolder), exist_ok=True)

    dataframe = merge_measurements_records(dataframe)

    dataframe_no_drops = dataframe.copy()

    drop_global_local_dtf = [ i for i in dataframe.columns if i.startswith('gl_')]

    columns_to_drop = ['measurement_type', 'measurement_date', 'files_paths', 'fif_file_paths', 'person_id']

    # leaving only percentiles for active paradigms
    columns_to_drop.extend(['auc_global_local_local_early',
                            'auc_global_local_global_late',
                            'auc_sluchowe_slowa',
                            'auc_czuciowe',
                            'auc_wzrokowe',
                            'auc_sluchowe_tony',
                            'skull',
                            ])
    # leaving only sleep and active paradigms:
    #columns_to_drop.extend(dtf_rs_features)

    # dropping active paradigms:
    #columns_to_drop.extend(active_features)

    columns_to_drop.extend(drop_global_local_dtf)

    columns_to_drop.extend(features_to_drop_due_to_no_correlations)
    for column in columns_to_drop:
        dataframe.drop(column, axis=1, inplace=True, errors='ignore')


    #drop unneeded records
    masks_exist = []
    for i in crs_convertion_dict.keys():
        masks_exist.append(dataframe.diag==i)
    mask_exist = np.sum(masks_exist, axis=0).astype(bool)
    dataframe = dataframe[mask_exist]
    dataframe_no_drops = dataframe_no_drops[mask_exist]
    # convert CRS

    dataframe['diag'] = dataframe['diag'].apply(lambda x: crs_convertion_dict[x])

    features = list(set(dataframe.columns) - set(['diag']))

    feature_matrix = dataframe[features].values # wpisy, features
    label = dataframe['diag'].values

    available_feature_count = np.sum(np.isfinite(feature_matrix), axis=1)
    available_feature_fraction = available_feature_count / feature_matrix.shape[1]

    non_empty_features_mask = (available_feature_count > 0)

    # dropping empty records
    dataframe = dataframe[non_empty_features_mask]
    feature_matrix = dataframe[features].values  # wpisy, features
    label = dataframe['diag'].values
    available_feature_count = np.sum(np.isfinite(feature_matrix), axis=1)
    available_feature_fraction = available_feature_count / feature_matrix.shape[1]
    dataframe_no_drops = dataframe_no_drops[non_empty_features_mask]

    loo = LeaveOneOut()


    predictions = []

    for train_index, test_index in tqdm(loo.split(feature_matrix), total=loo.get_n_splits(feature_matrix)):


        feature_matrix_train = feature_matrix[train_index, :]
        label_train = label[train_index]

        feature_matrix_test = feature_matrix[test_index]
        label_test = label[test_index]

        dtrain = xgb.DMatrix(feature_matrix_train, label=label_train, feature_names=features)
        dtest = xgb.DMatrix(feature_matrix_test, label=label_test, feature_names=features)


        param = {'max_depth': 100, 'eta': 0.1, 'silent': 1, 'objective': 'multi:softprob', 'num_class': 8}
        param['nthread'] = 8
        param['eval_metric'] = 'merror'

        evallist = [(dtest, 'eval'), (dtrain, 'train')]

        num_round = 100
        bst = xgb.train(param, dtrain, num_round, evallist, verbose_eval=False)

        prediction = np.argmax(bst.predict(dtest), axis=1)
        predictions.append(prediction[0])
    predictions = np.array(predictions)

    prediction_error_level = (predictions - label)


    for crit_feature_fraction in [0.0,]:
        error_rate_per_label_y = []
        error_rate_per_label_x = []
        error_distance = []
        error_distance_weighted_by_feature_completeness = []

        suffix = 'using_correlations_{}_crit_feature_fraction_for_test_{}'.format(percentiles_path is not None, crit_feature_fraction)

        categories_for_summary = list(sorted(crs_convertion_dict_back.keys()))[1:]

        for l in categories_for_summary:
            mask = (label == l)
            mask_complete_features = (available_feature_fraction >= crit_feature_fraction)
            mask = (mask & mask_complete_features)
            error_rate_per_label = np.sum(prediction_error_level[mask] != 0) / np.sum(mask)
            error_rate_per_label_x.append(l)
            error_rate_per_label_y.append(error_rate_per_label)
            error_distance.append(prediction_error_level[mask])
            error_distance_weighted_by_feature_completeness.append(available_feature_fraction[mask] * prediction_error_level[mask])


        fig = pb.figure(figsize=(10, 12))
        ax = fig.add_subplot(111)
        ax.bar(error_rate_per_label_x, error_rate_per_label_y)
        pb.title("Error rate per diag")
        x_tick_position = categories_for_summary
        x_tick_labels = [crs_convertion_dict_back[i] for i in x_tick_position]
        pb.xticks(x_tick_position, x_tick_labels, rotation=45)
        pb.grid()
        pb.ylim([0,1])
        fig.tight_layout()
        fig.savefig('{}/{}_error_rate_per_diag.png'.format(outfolder, suffix),bbox_inches='tight')
        pb.close(fig)

        fig = pb.figure(figsize=(10, 12))
        ax = fig.add_subplot(111)
        pb.title("Error miss distance per diag")
        seaborn.swarmplot(data=error_distance, ax=ax)
        seaborn.boxplot(data=error_distance, saturation=0, ax=ax)
        x_tick_position = np.array(categories_for_summary) - 1
        x_tick_labels = [crs_convertion_dict_back[i] for i in (x_tick_position + 1)]
        pb.xticks(x_tick_position, x_tick_labels, rotation=45)
        pb.grid()
        fig.tight_layout()
        fig.savefig('{}/{}_error_miss_distance.png'.format(outfolder, suffix), bbox_inches='tight')
        pb.close(fig)

        fig = pb.figure(figsize=(10, 12))
        ax = fig.add_subplot(111)
        pb.title("Error miss distance per diag, weighted by feature completeness")
        seaborn.swarmplot(data=error_distance_weighted_by_feature_completeness, ax=ax)
        seaborn.boxplot(data=error_distance_weighted_by_feature_completeness, saturation=0, ax=ax)
        pb.grid()
        x_tick_position = np.array(categories_for_summary) - 1
        x_tick_labels = [crs_convertion_dict_back[i] for i in (x_tick_position + 1)]
        pb.xticks(x_tick_position, x_tick_labels, rotation=45)
        fig.tight_layout()
        fig.savefig('{}/{}_error_miss_distance_weighted.png'.format(outfolder, suffix), bbox_inches='tight')
        pb.close(fig)

    columns = ['person_id', 'measurement_date', 'diag', 'diag_rank', 'diag_rank_predicted', 'prediction_error_level', 'available_feature_fraction',
               'available_feature_count']
    rows = []
    for i in range(len(label)):
        rows.append([dataframe_no_drops.iloc[i].person_id, dataframe_no_drops.iloc[i].measurement_date,
                     dataframe_no_drops.iloc[i].diag,
                     label[i], predictions[i], prediction_error_level[i], available_feature_fraction[i], available_feature_count[i]])
    summary_frame = pandas.DataFrame.from_records(data=rows, columns=columns)
    summary_frame.to_csv('{}/summary_percentiles_used_{}.csv'.format(outfolder, percentiles_path is not None))

    feature_importance = bst.get_score()
    columns = ['feature', 'importance']
    rows = [i for i in sorted(feature_importance.items(), key=lambda x: x[1], reverse=True)]
    feature_importance = pandas.DataFrame(data=rows, columns=columns)
    feature_importance.to_csv('{}/feature_importance_percentiles_used_{}.csv'.format(outfolder, percentiles_path is not None))

    #  summary
    try:
        confusion_matrix = ConfusionMatrix([crs_convertion_dict_back[i] for i in label]
                                           ,[crs_convertion_dict_back[i] for i in predictions])
        confusion_matrix.to_dataframe().to_csv('{}/confusion_matrix_percentiles_used_{}.csv'.format(outfolder, percentiles_path is not None))
    except AssertionError:
        pass

    with open('{}/summary_percentiles_used_{}.txt'.format(outfolder, percentiles_path is not None), 'w') as summary:
        accuracy_global = np.sum(prediction_error_level == 0) / len(prediction_error_level)
        line = "Global Accuracy: {}\n".format(accuracy_global)
        print(crs_convertion_dict)
        print("Using percentiles to cull features", percentiles_path is not None)
        print("Using features", ' '.join(bst.feature_names))
        print(line)
        summary.write(line)
        for l in sorted(crs_convertion_dict_back.keys()):
            mask = (label == l)
            local_prediction_error = prediction_error_level[mask]
            local_accuracy = np.sum(local_prediction_error == 0) / len(local_prediction_error)
            line = "Accuracy for label {}: {}\n".format(crs_convertion_dict_back[l], local_accuracy)
            summary.write(line)
            print(line)




            # summary_frame.to_csv('out_trees/confusion_matrix_percentiles_used_{}.csv'.format(percentiles_path is not None))


if __name__ == '__main__':
    crs_convertion_dicts = [{'CONTROL ADULT': 2,
                             'CONTROL CHILD': 2,
                             'EMCS': 2,
                             'MCS+/EMCS': 2,
                             'MCS+': 2,
                             'MCS-': 2,
                             'MCS-/UWS': 2,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 3,
                             'CONTROL CHILD': 3,
                             'EMCS': 2,
                             'MCS+/EMCS': 2,
                             'MCS+': 2,
                             'MCS-': 2,
                             'MCS-/UWS': 2,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 3,
                             'CONTROL CHILD': 3,
                             'EMCS': 3,
                             'MCS+/EMCS': 2,
                             'MCS+': 2,
                             'MCS-': 2,
                             'MCS-/UWS': 2,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 2,
                             'CONTROL CHILD': 2,
                             'EMCS': 1,
                             'MCS+/EMCS': 1,
                             'MCS+': 1,
                             'MCS-': 1,
                             'MCS-/UWS': 1,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 5,
                             'CONTROL CHILD': 5,
                             'EMCS': 4,
                             'MCS+/EMCS': 4,
                             'MCS+': 3,
                             'MCS-': 2,
                             'MCS-/UWS': 1,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 7,
                             'CONTROL CHILD': 7,
                             'EMCS': 6,
                             'MCS+/EMCS': 5,
                             'MCS+': 4,
                             'MCS-': 3,
                             'MCS-/UWS': 2,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'CONTROL ADULT': 4,
                             'CONTROL CHILD': 4,
                             'EMCS': 3,
                             'MCS+/EMCS': 2,
                             'MCS+': 2,
                             'MCS-': 2,
                             'MCS-/UWS': 2,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                             {'MCS+': 3,
                             'MCS-': 2,
                             'MCS-/UWS': 1,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             },
                            {'EMCS': 4,
                             'MCS+/EMCS': 4,
                             'MCS+': 3,
                             'MCS-': 2,
                             'MCS-/UWS': 1,
                             'UWS': 1,
                             'UNKNOWN': 0,
                             }
                            ]
    path = sys.argv[-1]
    percentiles_path = sys.argv[-2]
    for i in crs_convertion_dicts:
        learn_trees(path, percentiles_path, i)
        learn_trees(path, None, i)
