import pandas as pd
import seaborn as sns
import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from sklearn.neighbors.kde import KernelDensity
import scipy.stats as st
from joblib import Parallel, delayed
import itertools

from help_functions import read_database
from help_functions import crs_convertion_dict

from help_functions import merge_measurements_records


def _visualise_kde_estim(kde, data, feature):
    simulated = kde.sample(len(data))
    x_plot = np.linspace(np.min(simulated), np.max(simulated), 10000)
    y_plot = np.exp(kde.score_samples(x_plot[:, None]))
    plt.plot(x_plot, y_plot, label='simulated KDE density')
    plt.hist([data, simulated[:, 0]], density=True, label=['data', 'mc simulated'])
    plt.title(feature)
    plt.legend()
    plt.savefig("out/img/{}.png".format(feature))
    plt.close()
    # plt.show()

def correlate(data_path):
    dataframe = pd.DataFrame.from_csv(data_path)
    dataframe = merge_measurements_records(dataframe)

    columns_to_drop = ['measurement_type', 'measurement_date', 'files_paths', 'fif_file_paths', 'person_id']
    for column in columns_to_drop:
        dataframe.drop(column, axis=1, inplace=True)

    # convert CRS
    dataframe['diag'] = dataframe['diag'].apply(lambda x: crs_convertion_dict[x])

    kernel_estimation_models = {}
    for feature in dataframe.columns:
        # import IPython
        # IPython.embed()
        data = np.array(dataframe[feature])
        data = data[np.isfinite(data)]

        kde = KernelDensity(kernel='gaussian', bandwidth=(max(data) - min(data))/5)

        kde.fit(data[:, None])
        kernel_estimation_models[feature] = kde

        ####
        # _visualise_kde_estim(kde, data, feature)
        ####

    corr = dataframe.corr(method='spearman')

    # monte carlo:
    def _monte_carlo_gen(dataframe):
        dataframe_monte = dataframe.copy()
        for feature in dataframe.columns:
            mask = np.isfinite(dataframe_monte[feature])
            number_of_samples = np.sum(mask)
            samples = kernel_estimation_models[feature].sample(number_of_samples)[:, 0]
            dataframe_monte[feature][mask] = samples
        corr_monte = dataframe_monte.corr(method='spearman')
        return corr_monte.values

    correlations_mc = Parallel(n_jobs=-1, verbose=50)(delayed(_monte_carlo_gen)(dataframe) for i in range(100000))

    correlations_mc = np.array(correlations_mc)

    np.save("out/correlations_mc.npy", correlations_mc)

    corr.to_csv("out/Correlations.csv")
    corr_value = corr.values
    np.save("out/correlations_true.npy", corr_value)

    percentiles = np.empty_like(corr_value)

    def _get_percentile_nans(row, column, correlation, correlations_mc):
        correlation = correlation[row, column]
        correlations_array = correlations_mc[:, row, column]
        correlations_array = correlations_array[np.isfinite(correlations_array)]
        return st.percentileofscore(correlations_array, correlation)

    for row, column in itertools.product(range(percentiles.shape[0]), range(percentiles.shape[1])):
        percentile = _get_percentile_nans(row, column, corr_value, correlations_mc)
        percentiles[row, column] = percentile

    percentiles = pd.DataFrame(data=percentiles, index=corr.index, columns=corr.columns)
    percentiles.to_csv("out/Percentiles.csv")

    # import IPython
    # IPython.embed()


    mask = np.zeros_like(percentiles, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    f, ax = plt.subplots(figsize=(80, 80))

    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    colors = [(0.00, "red"), (0.025, "red"), (0.05, "lightgray"), (0.5, "lightgray"), (0.95, "lightgray"), (0.975, "red"), (1, "red")]
    cmap = LinearSegmentedColormap.from_list('Custom', colors, gamma=1)
    sns.heatmap(percentiles, mask=mask, cmap=cmap, vmax=100, center=50, vmin=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5}, annot=True, ax=ax, fmt='0.1f')
    f.savefig('out/Corr.png')



if __name__ == '__main__':
    path = sys.argv[-1]

    correlate(path)
