#!/usr/bin/env python
# coding: utf8
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
from obci.analysis.obci_signal_processing import read_manager
from helper_functions.helper_functions import PrepareRM, GetEpochsFromRM, get_tagfilters, shift_tags_relatively_to_signal_beginnig



montage = ['ears', "M1", "M2"]

start_offset = -0.2
duration = 1.

# tagi czasami rozpoznajemy po blok_type, a czasami po tag_name, to zależy od paradygmatu
blok_type = 1
tag_name = None

channel2draw = "Cz"

# tu można podać kilka różnych filtrów, zostaną zastosowane kolejno:
#    filters = [[[2, 29.6], [1, 45], 3, 20, "cheby2"]]      # filtr all-in-one

freq_dump = 18.34
filters = [[1., 0.5, 3, 6.97, "butter"],  # górnoprzepustowy

#           [30, 60, 3, 12.4, "butter"],  # dolnoprzepustowy
           [10, 20, 3, 12.3, "butter"],  # dolnoprzepustowy
           
           [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"],  # pasmowy (sieć 50 Hz)

#           [[freq_dump - 2.5, freq_dump + 2.5], [freq_dump - 0.1, freq_dump + 0.1], 3.0, 25., "cheby2"]  # ekstra filtr dla AS
           ]


drop_chnls = ['l_reka', 'p_reka', 'l_noga', 'p_noga', 'haptic1', 'haptic2',
              'phones', 'Driver_Saw', 'Saw', 'Sample_Counter', 'TSS']  # kanały, które nie zostaną użyte, bo nie zawierają zapiu EEG

filename = sys.argv[1]

fname = filename[:-4]
eeg_rm = read_manager.ReadManager(fname + '.xml', fname + '.raw', fname + '.tag')
Fs = float(eeg_rm.get_param('sampling_frequency'))
channel_no = eeg_rm.get_param('channels_names').index(channel2draw)
shift_tags_relatively_to_signal_beginnig(eeg_rm, 0.00005918)


filtered_mounted_rm = PrepareRM(eeg_rm, drop_chnls, filters = filters, montage = montage, gains = True)

tags_function_list, epoch_labels = get_tagfilters(blok_type)

epochs_list_local = GetEpochsFromRM(filtered_mounted_rm, tags_function_list, start_offset, duration, tag_name)  # lista epok dla bieżącego pliku


plt.figure()
erp_ffts = []
erps = []
for tagtype_e in epochs_list_local:
    channel_epochs = []
    len_list = []
    for epoch in tagtype_e:
        channel_epochs.append(epoch.get_samples()[channel_no])
        len_list.append(len(channel_epochs[-1]))
    length = min(len_list)
    ch_epochs = []
    erp_fft = np.zeros(np.fft.rfft(channel_epochs[0][:length]).size)
    for e in channel_epochs:
        ch_epochs.append(e[:length])
        erp_fft += np.abs(np.fft.rfft(e[:length]))
    erp_fft /= len(channel_epochs)
    erp_ffts.append(erp_fft)
    channel_epochs = np.array(ch_epochs)
    erp = channel_epochs.mean(axis = 0)
    if start_offset < 0:
        erp -= erp[:int(start_offset*Fs)].mean()
    T = (np.arange(erp.size)/Fs + start_offset) * 1e3
    erps.append(erp)
    plt.subplot(311)
    plt.title(u"uśrednione eeg")
    plt.plot(T, erp, label = str(channel_epochs.shape[0]))
    plt.legend()
    plt.subplot(312)
    plt.title(u"widmo uśrednionego eeg")
    plt.plot(np.fft.rfftfreq(erp.size, 1/Fs), np.abs(np.fft.rfft(erp)))
    plt.subplot(313)
    plt.title(u"uśrednione widmo eeg")
    plt.plot(np.fft.rfftfreq(erp.size, 1/Fs), erp_fft)
plt.show()
    