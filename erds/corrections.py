import abc
import numpy


class Correction:
    @staticmethod
    def choices():
        return {
            'none': None,
            'fdr': CorrectionFDR(),
        }

    @abc.abstractmethod
    def compute_alpha_level(self, alpha: float, p_values: numpy.array):
        """
        Compute alpha (significance) level corrected for multiple comparisons.

        :param alpha:  initial alpha value, e.g. 0.05
        :param p_values:  1-D array of p-values
        :return: corrected alpha value
        """
        return ...


class CorrectionFDR(Correction):
    def __init__(self, use_harmonic_sum: bool = False):
        self.use_harmonic_sum = use_harmonic_sum

    def compute_alpha_level(self, alpha: float, p_values: numpy.array):
        count = len(p_values)
        sorted = numpy.sort(p_values)
        result = 0
        harmonic_sum = numpy.sum(
            1.0 / numpy.arange(1, count+1)
        ) if self.use_harmonic_sum else 1.0

        for i in range(count):
            if sorted[i] <= (i+1) / (count * harmonic_sum) * alpha:
                result = sorted[i]
        return result
