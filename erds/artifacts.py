import abc
import glob
import numpy
import os.path
import scipy.signal
from xml.etree import ElementTree
from .io import Filterable


class ArtifactMethod:
    @staticmethod
    def choices():
        return {
            'none': None,
            'lowfreq': ArtifactMethodLowFreq(),
            'tags': ArtifactMethodFromTags(),
        }

    @abc.abstractmethod
    def replace_artifacts(self, signal: Filterable) -> Filterable:
        return ...


class ArtifactMethodLowFreq(ArtifactMethod):

    def __init__(self, window_duration = 1.0, median_threshold = 5.0):
        self.window_duration = window_duration
        self.median_threshold = median_threshold

    def __str__(self):
        return 'lowfreq'

    def replace_artifacts(self, signal: Filterable) -> Filterable:
        window_length = int(signal.freq_sampling * self.window_duration)
        channel_dimensions = len(signal.data.shape) - 1
        window_shape = [1] * channel_dimensions + [window_length]
        window = numpy.hamming(window_length).reshape(window_shape)
        channel_low_power = signal.filtered(3.0, 5.0, 1.0, 15.0, 'butter')
        windowed = scipy.signal.convolve(channel_low_power.data ** 2, window, mode='same')
        windowed /= numpy.median(windowed, axis=channel_dimensions, keepdims=True)

        replaced = signal.clone()
        exceeded = (windowed > self.median_threshold).astype('float')
        exceeded = scipy.signal.convolve(exceeded, numpy.ones(shape=window_shape), mode='same')
        replaced.data[exceeded >= 1] = numpy.nan
        return replaced


class ArtifactMethodFromTags(ArtifactMethod):

    def __init__(self):
        self.artifacts = []

    def __str__(self):
        return 'tags'

    def initialize(self, path_to_raw):
        path_parts = os.path.split(path_to_raw)
        path_parts = path_parts[0:-1] + (path_parts[-1].split('.')[0]+'*artifacts*.tag',)

        self.artifacts = []
        tag_pattern = os.path.join(*path_parts)
        artifact_files = glob.glob(tag_pattern)
        if len(artifact_files) != 1:
            raise Exception('could not find file with artifact tags')
        xml_tags = ElementTree.parse(artifact_files[0]).getroot()
        for tag in xml_tags.iter('tag'):
            position = float(tag.attrib['position'])
            length = float(tag.attrib['length'])
            channel = int(tag.attrib['channelNumber'])
            self.artifacts.append((position, length, channel))

    def replace_artifacts(self, signal: Filterable):
        replaced = signal.clone()
        for position, length, channel in self.artifacts:
            offset_start = int(signal.freq_sampling * position)
            offset_end = int(signal.freq_sampling * (position + length))
            replaced.data[channel, offset_start:offset_end] = numpy.nan
        return replaced
