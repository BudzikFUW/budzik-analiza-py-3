import abc
import numpy
import scipy.stats


class Statistic:
    @staticmethod
    def choices():
        return {
            'none': None,
            'bootstrap': StatisticBootstrap(),
            'mannwhitney': StatisticMannWhitney(),
            'welch': StatisticWelch(),
        }

    @abc.abstractmethod
    def compute_p_values(self, spec_data: numpy.ndarray, refs_data: numpy.ndarray):
        """
        Compute p-values of statistical test of difference between
        two given unrelated data sets.

        :param spec_data: numpy array of dimensions (*, frequency, time)
        :param refs_data: numpy array of dimensions (*, frequency, 1)

        First dimension of two given array does not have to be equal.
        NAN values will be ignored.
        """
        return ...


class StatisticBootstrap(Statistic):

    DEFAULT_REPEATS = 2000

    def __init__(self, repeats=DEFAULT_REPEATS):
        self.repeats = repeats

    def statistic(self, spec_data: numpy.ndarray, refs_data: numpy.ndarray):
        return (numpy.nanmean(spec_data, axis=0) - numpy.nanmean(refs_data, axis=0)) / numpy.sqrt(
            numpy.nanvar(spec_data, axis=0) + numpy.nanvar(refs_data, axis=0)
        )

    def compute_p_values(self, spec_data: numpy.ndarray, refs_data: numpy.ndarray):
        spec_count = spec_data.shape[0]
        t0 = self.statistic(spec_data, refs_data)
        all_data = numpy.concatenate((spec_data, numpy.repeat(refs_data, spec_data.shape[2], axis=2)), axis=0)

        # we will count occurences of |t|>|t0|
        counts = numpy.zeros(spec_data.shape[1:], dtype='int')
        for i in range(self.repeats):
            numpy.random.shuffle(all_data)
            fake_spec_data = all_data[:spec_count]
            fake_refs_data = all_data[spec_count:]
            t = self.statistic(fake_spec_data, fake_refs_data)
            counts += (numpy.abs(t) > numpy.abs(t0))
        return counts / self.repeats


class StatisticMannWhitney(Statistic):
    def compute_p_values(self, spec_data: numpy.ndarray, refs_data: numpy.ndarray):
        f_count, t_count = spec_data.shape[1:]
        p_values = numpy.ndarray((f_count, t_count))
        for f_index in range(f_count):
            for t_index in range(t_count):
                spec_row = spec_data[:, f_index, t_index]
                refs_row = refs_data[:, f_index, 0]
                p_values[f_index, t_index] = scipy.stats.mannwhitneyu(spec_row[~numpy.isnan(spec_row)], refs_row[~numpy.isnan(refs_row)], alternative='two-sided')[1]
        return p_values


class StatisticWelch(Statistic):
    def compute_p_values(self, spec_data: numpy.ndarray, refs_data: numpy.ndarray):
        return scipy.stats.ttest_ind(spec_data, refs_data, equal_var=False, axis=0, nan_policy='omit')[1]
