import abc
import numpy
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import LinearSVC


class Classifier:

    @staticmethod
    def choices():
        return {
            'none': None,
            'lda': ClassifierLDA(),
            'svc': ClassifierSVC(),
            'voting': ClassifierVoting(),
        }

    @abc.abstractmethod
    def fit(self, features_a, features_b):
        ...

    @abc.abstractmethod
    def classify(self, features):
        ...

    def test(self, features_a, features_b):
        correct_count = 0
        all_count = len(features_a) + len(features_b)
        for i in range(len(features_a)):
            learn_features_a = numpy.concatenate((features_a[:i], features_a[i+1:]))
            test_feature_a = features_a[i]
            self.fit(learn_features_a, features_b)
            if self.classify(test_feature_a) < 0:
                correct_count += 1
        for i in range(len(features_b)):
            learn_features_b = numpy.concatenate((features_b[:i], features_b[i+1:]))
            test_feature_b = features_b[i]
            self.fit(features_a, learn_features_b)
            if self.classify(test_feature_b) > 0:
                correct_count += 1
        return correct_count / all_count


class ClassifierFromSklearn(Classifier):

    def __init__(self, classifier):
        self.classifier = classifier

    def fit(self, features_a, features_b):
        X = numpy.concatenate((features_a, features_b), axis=0)
        y = numpy.concatenate((
            -numpy.ones(len(features_a)),
            +numpy.ones(len(features_b))
        ))
        self.classifier.fit(X, y)

    def classify(self, features):
        return self.classifier.predict(features.reshape((1,-1)))[0]


class ClassifierSVC(ClassifierFromSklearn):

    def __init__(self):
        super().__init__(LinearSVC())


class ClassifierLDA(ClassifierFromSklearn):

    def __init__(self):
        super().__init__(LinearDiscriminantAnalysis())


class ClassifierVoting(Classifier):

    def __init__(self):
        self.classifiers = []

    def fit(self, features_a, features_b):
        feature_count = features_a.shape[1]
        if features_b.shape[1] != feature_count:
            raise Exception('data differ in the number of features')

        # finding best features
        self.classifiers = []
        max_mismatch = (len(features_a) + len(features_b)) // 4
        for f_index in range(feature_count):
            values_a = features_a[:, f_index]
            values_b = features_b[:, f_index]

            separator, mismatch, inverted = self._calc_both(
                values_a[~numpy.isnan(values_a)],
                values_b[~numpy.isnan(values_b)]
            )
            if separator is not None and mismatch <= max_mismatch:
                # TODO: uwzględnić maksymalizację odległości pomiędzy granicznymi
                # także gdy best_mismatch = 0
                self.classifiers.append((f_index, separator, inverted))

    def classify(self, features):
        count_for_a = count_for_b = 0
        for f_index, separator, inverted in self.classifiers:
            value = features[f_index]
            if (value < separator and not inverted) or (value > separator and inverted):
                count_for_a += 1
            if (value > separator and not inverted) or (value < separator and inverted):
                count_for_b += 1
        return -1 if count_for_a > count_for_b else +1 if count_for_b > count_for_a else 0

    @classmethod
    def _calc_one(cls, a, b):
        # return: (separator, mismatches)
        # separator may be None
        steps = [(x, +1) for x in a] + [(x, -1) for x in b]
        steps.sort()
        sums = numpy.cumsum([step[1] for step in steps])
        index = numpy.argmax(sums)
        if index + 1 >= len(sums) or steps[index][0] == steps[index+1][0]:
            return None, len(a) + len(b)
        separator = (steps[index][0] + steps[index + 1][0]) / 2
        return separator, len(a) - max(sums)

    @classmethod
    def _calc_both(cls, a, b):
        sep_ab, mis_ab = cls._calc_one(a, b)
        sep_ba, mis_ba = cls._calc_one(b, a)
        return (sep_ba, mis_ba, True) if mis_ba < mis_ab else (sep_ab, mis_ab, False)
