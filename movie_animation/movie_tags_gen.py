#!/usr/bin/python2
#  -*- coding: utf-8 -*-
# Kreator tagów na podstawie listy i kanału ze znacznikami
# Marcin Pietrzak 2017-06-22
#
# skrypt oczekuje dwóch lub trzecg argumentów będących ścieżkami do odpowiednich plików; schemat użycia:
# python movie_tags_gen.py plik_z_lista_tagow plik_raw lista_outlierow
#
# lista tagow w pliku powinna byc dwukolumnowa (kolumny oddzielone białym znakiem)

# lista outlierow jest opcjonalna i powinna zawierać timestampy w formacie mm:ss początku i końca każdego outliera (dwie kolumny) jeden outlier na wiersz



import os
import sys
sys.path.insert(1, "..")  # żeby pobrał lokalne obci
import numpy as np
from obci.analysis.obci_signal_processing.tags.tags_file_writer import TagsFileWriter
from obci.analysis.obci_signal_processing import read_manager


def TagsWriter(TagNames, TagStartTimes, Duration = 1):
    tags = []
    assert len(TagNames) == len(TagStartTimes)
    for i in range(len(TagNames)):
        tag = {'channelNumber': -1, 'start_timestamp': TagStartTimes[i], 'name': TagNames[i], 'end_timestamp': TagStartTimes[i]+Duration,
               'desc': {}}
        tags.append(tag)
    return tags


def TagsSaver(FilePath, tags):
    try:
        os.mkdir(os.path.dirname(FilePath))
    except OSError:
        pass
    writer = TagsFileWriter(FilePath)
    for tag in tags:
        writer.tag_received(tag)
    writer.finish_saving(0.0)


def StimulusMarksFinder(trigger_chnl, thr, Fs = 1024, MarkMaxLen = 1):
    trigger_chnl = np.abs(trigger_chnl)

    TagStartTimes = []
    i=0
    while i < len(trigger_chnl):
        arg = np.argmax(trigger_chnl[i:] > thr)
        
        if np.any(trigger_chnl[i:] > thr):
            TagStartTimes.append(1.*(i+arg)/Fs)
        
        i += arg+MarkMaxLen*Fs
        
    return TagStartTimes


if __name__=="__main__":
    file = open(sys.argv[1])
    
    TagNames = []
    TagDesc = []
    for line in file:
        Name1, Name2 = line.split()  # tagi mają mieć dwuczłonowe nazwy
        TagNames.append(Name1 + "_" + Name2)

    filename = sys.argv[2]
    if ".raw" not in filename:
        print "Podaj plik z rozszerzeniem .raw, jako drugi argument"

    filename = filename[:-4]
    
    rm = read_manager.ReadManager(filename+'.xml', filename+'.raw', None)
    Fs = int(float(rm.get_param("sampling_frequency")))

    trigger_chnl = np.abs(rm.get_channel_samples("phones")) * float(rm.get_param('channels_gains')[rm.get_param('channels_names').index(u"phones")])
    # mnożę przez gain tylko dla zgodności z tym, co widać choćby w Svarogu
    
    # usuwanie outlierów:
    trigger_chnl[trigger_chnl >= 0.99 * trigger_chnl.max()] = 0
    if len(sys.argv) > 3:
        print "usuwam outliery wg podanej listy", trigger_chnl.mean(), trigger_chnl.max(), trigger_chnl.std()
        with open(sys.argv[3]) as outlier_file:
            for line in outlier_file:
                if len(line) > 6:
                    start, stop = line.split()
                    start_m, start_s = map(float, start.split(":"))
                    stop_m, stop_s = map(float, stop.split(":"))
                    
                    start = int((60 * start_m + start_s) * Fs)
                    stop = int((60 * stop_m + stop_s) * Fs)
                    
                    trigger_chnl[start:stop+1] = 0

        print "usunąłem outliery wg podanej listy", trigger_chnl.mean(), trigger_chnl.max(), trigger_chnl.std()
    
    thr = 3*np.std(trigger_chnl)+np.mean(trigger_chnl)
    maksimum = trigger_chnl.max()
    if thr > 0.5*maksimum:
        thr = 0.5*maksimum

    # ten wynalazek poniżej dopasowuje threshold, aczkolwiek w idealnej sytuacji (dobre znaczniki na sygnale i dopasowana do niej lista)
    # nie powinien w ogóle zostać użyty przez skyrpt
    #############
    last_too_low_thr = 0
    last_too_high_thr = np.max(trigger_chnl)
    print("\nWyznaczony threshold {}").format(thr)
    TagStartTimes = StimulusMarksFinder(trigger_chnl, thr, Fs, MarkMaxLen = 1)
    counter = 100
    while not len(TagNames) == len(TagStartTimes) and counter:
        counter -= 1
        print "Niezdogność liczby tagów na liście i w sygnale:"
        print "liczba tagów:", len(TagNames), "liczba znalezionych bodźców:", len(TagStartTimes)

        if len(TagNames) < len(TagStartTimes):
            last_too_low_thr = thr
            thr = (thr+last_too_high_thr)/2
        else:
            last_too_high_thr = thr
            thr = (last_too_low_thr+thr)/2

        print "\nZmieniam threshold na {} i próbuję ponownie".format(thr)
        TagStartTimes = StimulusMarksFinder(trigger_chnl, thr, Fs, MarkMaxLen = 1)
    #############
        
    print "\nZnaleziono właściwy threshold = {} i wyznaczono pozycje {} tagów".format(thr, len(TagStartTimes))
    tags = TagsWriter(TagNames, TagStartTimes)
    
    TagsSaver(filename+".tag", tags)

