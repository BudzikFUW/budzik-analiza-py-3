#!/usr/bin/env python
#coding: utf8
import sys

import numpy as np
import scipy.stats

from obci.analysis.obci_signal_processing.smart_tags_manager import SmartTagsManager

from obci.analysis.obci_signal_processing.tags.smart_tag_definition import SmartTagDurationDefinition

from obci.analysis.obci_signal_processing.read_manager import ReadManager

import pylab as pb


TRIG_THR = 20000

DEBUG = False

TRIG_CHANNEL = sys.argv[2]

START_OFFSET=-0.1




def get_latency(signal, thr):


    for n, v in enumerate(signal):
        if v**2>=thr**2:
            if DEBUG:
                pb.figure()
                pb.plot(signal)
                pb.axvline(n, color='r')
                pb.show()
            return n

    return None

def get_latencies(tags):
    latencies = []
    tags_time = []  
    fs = float(tags[0].get_params()['sampling_frequency'])
    for tag in tags:
        start_stamp = tag.get_start_timestamp()
        samples = tag.get_channel_samples(TRIG_CHANNEL)
        lat = get_latency(samples, TRIG_THR)
        if lat:
            corr_lat = lat/fs+START_OFFSET
            if corr_lat < 0.2:
                latencies.append(lat/fs+START_OFFSET)
                tags_time.append(start_stamp)


    return latencies, tags_time


def latencies(fname):
    rm = ReadManager(fname+'.xml', fname+'.raw', fname+'.tag')
    tag_def = SmartTagDurationDefinition(start_tag_name=None,
                                        start_offset=START_OFFSET,
                                        end_offset=0,
                                        duration=1.3)
    stags = SmartTagsManager(tag_def, '', '' ,'', p_read_manager=rm)
    tags = stags.get_smart_tags()
    latencies, tags_time = get_latencies(tags)

    info  = ' '.join([str(i) for i in ["Mean latency:",
                                      np.mean(latencies),
                                      "+-",
                                      np.std(latencies),
                                      "N=",
                                      len(latencies)
                                      ]]
                                    )
    pb.figure()
    pb.plot(tags_time, latencies)
    p, V = np.polyfit(tags_time, latencies, 1, cov=True)
    a, b = p
    a_err = np.sqrt(V[0][0])
    b_err = np.sqrt(V[1][1])
    trend = np.array(tags_time)*a
    offset = b
    pb.plot(tags_time, trend + offset)
    pb.title("Latencies a={:0.6f}, b={:0.6f}".format(a, b))
    
    pb.figure()
    pb.hist(latencies, bins=20)
    pb.title("Latencies\n{}".format(info))
    
    print info
    
    latencies_detrended = np.array(latencies) - trend
    info_det  = ' '.join([str(i) for i in ["Mean latency detrended:",
                                  np.mean(latencies_detrended),
                                  "+-",
                                  np.std(latencies_detrended),
                                  "N=",
                                  len(latencies_detrended)
                                  ]]
                                )
    print info_det
    print "trend: a={}+-{} b={}+-{}".format(a, a_err, b, b_err)
    pb.figure()
    pb.hist(latencies_detrended, bins=20)
    pb.title("Latencies detrended\n{}".format(info_det))
    

    
    pb.show()

def main(argv):
    try:
        f = argv[1]
    except IndexError:
        raise IOError( 'Please provide path to .raw')
    latencies(f)
if __name__ == '__main__':
    main(sys.argv)

