#!/usr/bin/env python

from __future__ import print_function

from obci.analysis.obci_signal_processing import read_manager

import fnmatch
import os
import os.path
import sys

import itertools

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA
import matplotlib.cm as cm

PROJ_DATA_DIR = '/home/alex/vol/bach/budzik/w_projekcie/dzienne'
REF_DATA_DIR = '/home/alex/vol/bach/budzik/testy_procedur'
OUT_DIR = '/home/alex/budzik_out'

def get_matches_from_dir(directory):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, '*etr*.raw'):
            matches.append(os.path.join(root, filename))
    return matches

matches = []
matches += get_matches_from_dir(PROJ_DATA_DIR)
matches += get_matches_from_dir(REF_DATA_DIR)

for f_name in matches:
    print('File:', f_name)

    f_name_path = f_name.replace('.raw', '')
    f_name_base = os.path.basename(f_name_path)

    if 'testy_procedur' in f_name:
        f_name_base = 'test_' + f_name.split('/')[-2] + '_' + f_name_base

    try:
        rm = read_manager.ReadManager(f_name_path+'.xml', f_name_path+'.raw', f_name_path+'.tag')
    except Exception as ex:
        print('Skipping:', f_name)
        print(ex)
        continue

    tags = rm.get_tags()

    samples = rm.get_samples()
    #print('samples.shape:', samples.shape)

    timestamps_target = []
    timestamps_nontarget = []

    if 'wz_' in f_name:
        print('Skipping:', f_name)
        continue

    if 'ERD_ERS' in f_name:
        print('Skipping:', f_name)
        continue

    if 'SEP' in f_name:
        print('Skipping:', f_name)
        continue

    if '_cz_' in f_name:
        print('skipping', f_name)  
        continue

    if '_sl_' in f_name:
        print('skipping', f_name)  
        continue

    if '_Global-local_' in f_name:
        print('skipping', f_name) 
        continue 

    for t in tags:
        if 'P300' in f_name:
            if 'wz1' in f_name or 'wz2' in f_name:
                try:
                    tag_type = t['desc']['type']
                    tm = t['start_timestamp']
                    if tag_type == 'target':
                        timestamps_target.append(tm)
                    elif tag_type == 'nontarget':
                        timestamps_nontarget.append(tm)
                    else:
                        print('Unknown tag:', tag_type)
                except:
                    print(t)
                    sys.exit()
            else:
                print('Error P300', f_name)
                sys.exit()
        elif 'VEP' in f_name:
            if t['name'] == u'isi':
                tm = t['start_timestamp']
                timestamps_nontarget.append(tm)
            elif t['name'] == u'zdj':
                tm = t['start_timestamp']
                timestamps_target.append(tm)
            else:
                print(t)
                sys.exit()
        else:
            print('ERROR', f_name)
            sys.exit()

    print('timestamps targets:', len(timestamps_target))
    print('timestamps nontargets:', len(timestamps_nontarget))

    first_sample_timestamp = float(rm.get_params()['first_sample_timestamp'])
    print('first_sample_timestamp:', first_sample_timestamp)
    if first_sample_timestamp < 0:
        print('Skipping (first_sample_timestamp < 0):', f_name)
        continue

    timestamps_target = sorted(timestamps_target)
    timestamps_nontarget = sorted(timestamps_nontarget)

    timestamps = timestamps_target + timestamps_nontarget
    timestamps = sorted(timestamps)

    # print(timestamps)

    data = np.fromfile(f_name, dtype=np.float64)

    # channels:
    # - timestamp - DO NOT USED THIS CHANNEL
    # - eye_detected
    # - x
    # - y
    # - TSS - offset form signal start

    data = data.reshape((-1, 5))
    data_timestamps = data[:, 4]
    eye_detected = data[:, 1]
    eye_detected = eye_detected > 0.5
    x = data[:, 2]
    y = data[:, 3]

    #print(data[0])
    #print(data[1])
    #print(data[2])
    
    #print(data_timestamps)

    x_ok = np.array([v for i, v in enumerate(x) if eye_detected[i]])
    y_ok = np.array([v for i, v in enumerate(y) if eye_detected[i]])

    data_timestamps_ok = np.array([v for i, v in enumerate(data_timestamps) if eye_detected[i]])

    x_bad = np.array([v for i, v in enumerate(x) if not eye_detected[i]])
    y_bad = np.array([v for i, v in enumerate(y) if not eye_detected[i]])

    if x_ok.shape[0] == 0:
        print('SKIP: x_ok.shape[0] == 0')
        continue

    data_ok = np.zeros((x_ok.shape[0], 2))
    data_ok[:, 0] = x_ok
    data_ok[:, 1] = y_ok

    if 1:
        plt.figure()
        plt.plot(x_bad, y_bad, 'r,', x_ok, y_ok, 'b,')
        plt.savefig(OUT_DIR + '/' + f_name_base + '_all_data_points.png')
        plt.close()

        plt.figure()
        plt.plot(x_ok, y_ok, 'b,')
        plt.savefig(OUT_DIR + '/' + f_name_base + '_eye_detected.png')
        plt.close()

        plt.figure()
        plt.scatter(data_ok[:, 0],
                    data_ok[:, 1], 
                    marker='o',
                    c=np.linspace(0, 1, len(data_ok[:, 1])))
        plt.savefig(OUT_DIR + '/' + f_name_base + '_color_time.png')
        plt.close()

    print(data_ok.shape)

    if 1:
        data_ok_pca = PCA(data_ok)
        plt.figure()
        plt.scatter(data_ok_pca.Y[:, 0],
                    data_ok_pca.Y[:, 1],
                    marker='o',
                    c=np.linspace(0, 1, len(data_ok_pca.Y[:, 1])))
        plt.savefig(OUT_DIR + '/' + f_name_base + '_color_time_pca.png')
        plt.close()

    INTERVAL_BEFORE = 0.1
    INTERVAL_AFTER = 0.5

    def get_measurements_close_to_timestamp(timestamp):
        data_indexes = []
        for i, data_tm in enumerate(data_timestamps_ok):
            if timestamp - INTERVAL_BEFORE <= data_tm <= timestamp + INTERVAL_AFTER:
                data_indexes.append(i)
        data = data_ok[data_indexes, :]
        return data

    # cluster geometrical center 
    centers_target = np.zeros((0,2))
    centers_nontarget = np.zeros((0,2))
    centers_combined = np.zeros((0,2))

    for name, tm_list in [('target', timestamps_target),
                          ('nontarget',timestamps_nontarget),
                          ('combined', timestamps)]:
        
        colors = itertools.cycle(["r", "b", "g", 'c', 'm', 'y', 'k'])

        clusters = []
        for tm in tm_list:
            data = get_measurements_close_to_timestamp(tm)
            if len(data) == 0:
                continue
            clusters.append(data)

        if len(clusters) == 0:
            continue

        print('clusters ({}):'.format(name), len(clusters))

        if 1:
            plt.figure()
            for c in clusters:
                plt.scatter(c[:, 0],
                            c[:, 1],
                            marker='.',
                            color=next(colors))
            plt.savefig(OUT_DIR + '/' + f_name_base + '_clusters_' + name + '.png')
            plt.close()

        centers = []
        for c in clusters:
            avg_x = np.mean(c[:, 0])
            avg_y = np.mean(c[:, 1])
            centers.append([avg_x, avg_y])

        centers = np.array(centers)

        if 1:
            plt.figure()
            plt.scatter(centers[:, 0],
                        centers[:, 1],
                        marker='.',
                        color='b')
            plt.savefig(OUT_DIR + '/' + f_name_base + '_centers_' + name + '.png')
            plt.close()

        if name == 'target':
            centers_target = centers
        elif name == 'nontarget':
            centers_nontarget = centers
        elif name == 'combined':
            centers_combined = centers
        else:
            print('unreachable')
            sys.exit()

    print(centers_target.shape)
    print(centers_nontarget.shape)
    print(centers_combined.shape)

    if centers_target.shape[0] == 0:
        print('skip: centers_target.shape[0] == 0')
        continue

    targets_center = [np.mean(centers_target[:, 0]), np.mean(centers_target[:, 1])]
    nontargets_center = [np.mean(centers_nontarget[:, 0]), np.mean(centers_nontarget[:, 1])]
    combined_center = [np.mean(centers_combined[:, 0]), np.mean(centers_combined[:, 1])]

    center_distances = []
    for pt in centers_target:
        dx = pt[0] - targets_center[0]
        dy = pt[1] - targets_center[1]
        center_distances.append(np.sqrt(dx*dx + dy*dy))

    cutoff_dist = np.percentile(center_distances, 90) # okolo 2sigma

    # tutaj probowalem k-means/PCA, ale poniewaz nie znamy liczby klastrow 
    # wyniki byly slabe

    if 1:
        # combined plot: tagret + nontarget
        plt.figure()
        plt.scatter(centers_target[:, 0],
                    centers_target[:, 1],
                    marker='o',
                    color='b')
        plt.scatter(centers_nontarget[:, 0],
                    centers_nontarget[:, 1],
                    marker='+',
                    color='r')
        plt.scatter([targets_center[0]],
                    [targets_center[1]],
                    marker='o',
                    color='g', s=[100])

        plt.scatter([nontargets_center[0]],
                    [nontargets_center[1]],
                    marker='+',
                    color='g', s=[100])

        circ=plt.Circle(targets_center, radius=cutoff_dist, color='b', fill=False)
        plt.gca().add_patch(circ)

        plt.savefig(OUT_DIR + '/' + f_name_base + '_centers_final.png')
        plt.close()


