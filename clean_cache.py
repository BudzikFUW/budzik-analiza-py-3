#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from config import main_outdir

filelist = sys.argv[1:]
filelist = set([filename[:-4] for filename in filelist if '.obci.raw' in filename and '.etr.' not in filename])
if not filelist:
    print "Provide a path to data file(s) which cache will be deleted."

for FNAME in filelist:
    FNAME = os.path.basename(FNAME)[:-9]
    for root, dirs, files in os.walk(os.path.join(os.path.expanduser(main_outdir))):
        for filename in files:
            if FNAME in filename:
                print FNAME
                print filename
                print
                path = root + "/" + filename
                print "deleting", path
                print os.remove(path)
