#!/usr/bin/python2
# coding: utf-8
import os
import sys
import time
from scipy.signal import iirdesign

import numpy as np

from obci.analysis.obci_signal_processing.read_manager import ReadManager

from helper_functions.helper_functions import PrepareRM, get_microvolt_samples
from patients_generator.ar_generators import fit_ar_to_data, generate_fake_eeg_using_ar, apply_filters

from scipy.signal import iirnotch

from copy import deepcopy


def ar_continous_gen(fs, no_of_channels, order, outdir = []):
    if not outdir:
        outdir = "./results"

    # kontrola MP wzrokowe ears i jego kanały:
    patient_to_use = "template/MP_P300_wz1_20170427z151346.obci['ears', 'M1', 'M2']-2001790013095909780_ICA_CLEANED.obci"
    
    rm = ReadManager(patient_to_use + '.xml', patient_to_use + '.raw', None)
    available_channels = rm.get_param('channels_names')
    
    # uszu nie chcemy wykorzystywać
    available_channels.remove("M1")
    available_channels.remove("M2")
    
    step = int(len(available_channels) / no_of_channels)
    channels_to_use = available_channels[::step][:no_of_channels]
    length = 100 * 10 * len(channels_to_use) ** 2 * order
    
    fs_org = float(rm.get_param('sampling_frequency'))
    if fs_org > fs:
        rm = PrepareRM(rm, decimate_factor = int(fs_org / fs))
    
    nyq_org = fs_org / 2
    
    f_budzik = iirdesign(1 / nyq_org, 0.5 / nyq_org, 3, 6.97, ftype = 'butter')
    
    f_lines = [iirnotch(50. * (i + 1) / nyq_org, 10. * (i + 1)) for i in range(10) if 50 * (i + 1) < fs]
    
    filters = [f_budzik, ] + f_lines
    
    data_org = get_microvolt_samples(rm, channels_to_use)[:, int(fs * 15):-int(fs * 10)]
    data = apply_filters(data_org, filters)[:, int(2 * fs):int(-2 * fs)]
    Av = fit_ar_to_data(data, order)
    
    data_mean_abs = np.mean(np.abs(data))
    
    eeg = generate_fake_eeg_using_ar(Av, int(length))
    
    eeg *= data_mean_abs / np.mean(np.abs(eeg))  # normalizacja amplitud
    
    date_time = time.strftime("%Y%m%dz%H%M%S")
    outdir = os.path.join(outdir, time.strftime("%Y%m%dz%H%M%S"))
    results_filename = "AR{}_SimCont_{}".format(order, date_time)
    
    eeg_rm = deepcopy(rm)
    eeg_rm.set_samples(eeg, channels_to_use)
    eeg_rm.set_param("channels_gains", np.ones(eeg.shape[0]))
    
    try:
        os.makedirs(outdir)
    except OSError:
        pass
    eeg_rm.save_to_file(outdir, results_filename)
    np.save(os.path.join(outdir, results_filename + "params"), Av)


if __name__ == "__main__":
    if sys.argv[4:5]:
        outdir = sys.argv[4]
    else:
        outdir = []
    
    ar_continous_gen(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), outdir)
