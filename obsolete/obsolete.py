#!/usr/bin/env python
# -*- coding: utf-8 -*-
# based on obci.analysis.p300.analysis_offline
# Marian Dovgialo
import os
from scipy import linalg, signal
import numpy as np
import pylab as pb

from copy import deepcopy
import json

from obci.analysis.obci_signal_processing import read_manager
from obci.analysis.obci_signal_processing.signal import read_data_source
from obci.analysis.obci_signal_processing.tags.tags_file_writer import TagsFileWriter

import autoreject

from mne_conversions import read_manager_to_mne, mne_to_rm_list

def get_epochs_fromfile_flist(ds, tags_function_list, start_offset=-0.1, duration=2.0,
                        filters=[], decimate_factor=0, montage=None,
                        drop_chnls = [ u'AmpSaw', u'DriverSaw', u'trig1', u'trig2', u'Driver_Saw'],
                        tag_name = None,
                        gains = True,
                        tag_correction_chnls = [],
                        thr = None,
                        correction_window=[-0.1, 0.3],
                        correction_reverse = False,
                        tag_offset = 0.0,
                        remove_eog=None,
                        remove_artifacts=True,
                        bad_chnls_method = None,
                        bad_chnls_db='./bad_channels_db.json',
                        bad_chnls=None,
                        get_last_tags=False,
                        ):
    '''For offline calibration and testing,
    Args:
        ds: dataset file name without extension.
        start_offset: baseline in negative seconds,
        duration: duration of the epoch (including baseline),
        filter: list of [wp, ws, gpass, gstop] for scipy.signal.iirdesign
                in Hz, Db, or None if no filtering is required
        montage: list of ['montage name', ...] ...-channel names if required
            montage name can be 'ears', 'csa', 'custom'
            ears require 2 channel names for ear channels
            custom requires list of reference channel names
        tags_function_list: list of tag filtering functions to get epochs for
        tag_correction_chnls: list of trigger channels, or empty list for no correction
        thr: trigger detection threshold in microvolts, None for standard deviation
        correction_window: list of 2 floats - window around tag to search in trigger channel
        tag_offset: float - seconds, to move all tags by this offset when using tag alignment
        correction_reverse: search for stimulation end in window, window MUST be long enough
        tag_name: tag name to be considered, if you want to use all
                  tags use None
        gains: True if you want to multiply signal by gains
        bad_chnls_method: method to deal with known bad channels (in some json dict or given as list)
                drop or interpolate ('drop', 'interp')
        bad_chnls_db: json dict with filenames as keys and list of channel name strings as values
        bad_chnls: if not None bad_chnls_db will be ignored and this list of strings will be used
        eog_clean_only: returns this function with cleaned readmanager
    Return: list of smarttags corresponding to tags_function_list'''
    eeg_rm = read_manager.ReadManager(ds+'.xml', ds+'.raw', ds+'.tag')
    if gains:
        eeg_rm.set_samples(get_microvolt_samples(eeg_rm),  eeg_rm.get_param('channels_names'))
    if tag_correction_chnls:
        align_tags(eeg_rm, tag_correction_chnls,
                    start_offset=correction_window[0],
                    duration=correction_window[1],
                    thr=thr,
                    reverse=correction_reverse,
                    offset=tag_offset)

    if bad_chnls_method is not None:
        if bad_chnls is None:
            with open(bad_chnls_db) as bcdbf:
                bad_channels_dict = json.load(bcdbf)
                try:
                    bad_chnls = bad_channels_dict[os.path.basename(ds) + '.raw']
                except KeyError:
                    bad_chnls = []
    if bad_chnls_method == 'drop':
        drop_chnls = drop_chnls + bad_chnls
        print '\n\n\nWILL DROP CHNLS', drop_chnls

    eeg_rm = exclude_channels(eeg_rm, drop_chnls)
    if bad_chnls_method == 'interp':
        eeg_rm = interp_bads(eeg_rm, bad_chnls)
    if decimate_factor:
        eeg_rm = mgr_decimate(eeg_rm, decimate_factor)
    for filter in filters:
        print "Filtering...", filter
        try:
            eeg_rm = mgr_filter(eeg_rm, filter[0], filter[1],filter[2],
                            filter[3], ftype=filter[4], use_filtfilt=True)
        except:
            print "Działanie filtrów uległo zmianie. Teraz należy podać listę list oraz podać typ filtru, np.:\n([[2, 29.6], [1, 45], 3, 20, 'cheby2'],[tu można podać kolejny filtr])"
            exit()
    if montage:
        if montage[0] == 'ears':
            eeg_rm = montage_ears(eeg_rm, montage[1], montage[2])
        elif montage[0] == 'csa':
            eeg_rm = montage_csa(eeg_rm)
        elif montage[0] == 'custom':
            eeg_rm = montage_custom(eeg_rm, montage[1:])
        else:
            raise Exception('Unknown montage')
    if remove_eog:
        ica, bads, eog_events = fit_eog_ica(eeg_rm, remove_eog, manual=True, montage=montage, ds=ds)
        if bads:
            eeg_rm = remove_ica_components(eeg_rm, ica, bads, eog_events, ds=ds)

    if remove_artifacts:
        pass
        #TODO dodać sprawdzenie, czy istnieje plik z artefaktami,
        #jeśli nie, to uruchomić tutaj funkcję, która go wygeneruje

        # eeg_rm = add_events_to_rm(eeg_rm, eog_events)
        # savetags_from_rm(eeg_rm, OUTDIR, postfix='EOG')

    #~ pb.plot(eeg_rm.get_samples()[5])
    #~ pb.show()
    if get_last_tags:
        tags = eeg_rm.get_tags()
        eeg_rm.set_tags(tags[-1-99:])
    tag_def = SmartTagDurationDefinition(start_tag_name=tag_name,
                                        start_offset=start_offset,
                                        end_offset=0.0,
                                        duration=duration)
    stags = SmartTagsManager(tag_def, '', '' ,'', p_read_manager=eeg_rm)
    returntags = []
    for tagfunction in tags_function_list:
        returntags.append(stags.get_smart_tags(p_func = tagfunction,))
    print 'Found epochs in defined groups:', [len(i) for i in returntags]
    return returntags


def _do_autoreject(e_mne, fifname, interactive=True):
    ''' autoreject disabled, only interactive manual rejection'''
    ts = autoreject.LocalAutoRejectCV(consensus_percs=np.linspace(0, 0.99, 11))
        
#    clean_mne = ts.fit_transform(e_mne.pick_types(eeg=True))

    clean_mne = e_mne
    if interactive:
        #autoreject.plot_epochs(e_mne, bad_epochs_idx=ts.bad_epochs_idx,
        #        fix_log=ts.fix_log.as_matrix(), scalings='auto',
        #        title='', show=True, block=True)
        #print (clean_mne)
        clean_mne.plot(show=True, block=True, title='Select Remaining bad epochs to be dropped')
        clean_mne.plot(show=True, block=True, title='Please Double check')

        print (clean_mne)
    try:
        os.makedirs(os.path.join(OUTDIR, 'cache'))
    except Exception:
        pass
    clean_mne.save(fifname)
    return clean_mne, ts



def do_autoreject(epochs_list_local, get_from_file_args, clean_epochs_list, interactive, suffix, fname, dirty_epochs_list, use_cache=True):
    """
    does manual artefact rejection with caching
    
    :param epochs_list_local: a list of smart tags in epochs
    :param get_from_file_args: parameters of get_from_file function - for cache name
    :param clean_epochs_list: list of lists of clean epochs to be filled (by condition)
    :param suffix: string - suffix for cache file
    :param fname: name of first file in filelist
    :param dirty_epochs_list: dirty epoch list to fill with
    :param use_cache: To save selected clean signal to cache or to read that cache
    """
    fifname = os.path.join(OUTDIR, 'cache', os.path.basename(fname) + suffix + '_clean_epochs-epo.fif')

    cache_used = False
    if use_cache:
        try:
            clean_mne = mne.read_epochs(fifname)
            cache_used=True
        except Exception:
            cache_used=False

    if not cache_used:
        e_mne, tag_type_slices = read_manager_to_mne(epochs=epochs_list_local,
                                                    baseline=get_from_file_args['start_offset'])
        clean_mne, ts = _do_autoreject(e_mne, fifname, interactive)

        
        #bad_epochs_ids = ts.bad_epochs_idx
        dirty = []
        for type in epochs_list_local:
            dirty.extend(type)
        #dirty = [dirty[i] for i in bad_epochs_ids]
        #dirty_epochs_list.extend(dirty)
        #segment_shape = ts.bad_segments.shape
        #fig = pb.figure(figsize=(segment_shape[0]*0.2, segment_shape[1]*0.3))
        #ax = fig.add_subplot(111)
        
        #try:
        #    print('BAD SEGMENTS\n', ts.bad_segments)
        #    ax = sns.heatmap(ts.bad_segments.T, xticklabels=2, yticklabels=True, square=False,
        #                     cbar=False, cmap='Reds', ax = ax)
        #    ax.set_ylabel('Sensors')
        #    ax.set_xlabel('Trials')
        #    savemap = os.path.join(OUTDIR, os.path.basename(fname) + suffix + '_bad_epochs_map.png')
        #    fig.savefig(savemap)
        #    pb.close(fig)
        #except TypeError as e:
        #    print(traceback.format_exc())
        #    print(e)
        #    print('No map to save')


    clean_epochs_list_local = mne_to_rm_list(clean_mne)
    for epn, clean_epochs_local in enumerate(clean_epochs_list_local):
        clean_epochs_list[epn]+=clean_epochs_local
    return clean_epochs_list




def get_clean_epochs( filelist,
                      blok_type,
                      rej_amp = +np.inf,
                      rej_grad = +np.inf,
                      custom_suffix=None,
                      use_autoreject=True,
                      interactive = True,
                      autoreject_all=True,
                      autoreject_cache = False,
                      **get_epochs_fromfile_flist_args
                    ):

    tags_function_list = get_epochs_fromfile_flist_args['tags_function_list']
    montage = get_epochs_fromfile_flist_args['montage']
    bad_chnls_method = get_epochs_fromfile_flist_args['bad_chnls_method']
    bad_chnls_db = get_epochs_fromfile_flist_args['bad_chnls_db']
    
    
    print 'blok type', blok_type, 'montage', montage
    clean_epochs_list = [list() for tagtype in tags_function_list]
    #clean epochs list - list of smart tag lists, first dimension
    #is for every smarttag filtering function, second for every epoch
    if custom_suffix:
        suffix = custom_suffix
    else:
        if len(montage)<5:
            suffix = '_{}_blok_type-{}'.format(montage, blok_type)
        else:
            suffix = '_{}_blok_type-{}'.format(montage[:4]+['...'], blok_type)

    if bad_chnls_method == 'drop':  # we need to drop same channels for all files:
        bad_chnls = []
        with open(bad_chnls_db) as bad_chnls_db_f:
            bad_chnls_dict = json.load(bad_chnls_db_f)
            for fname in filelist:
                try:
                    bad_chnls.extend(bad_chnls_dict[os.path.basename(fname)])
                except KeyError:
                    pass
        get_epochs_fromfile_flist_args['bad_chnls'] = bad_chnls
        print '\n\n\nWILL DROP CHNLS', bad_chnls
    else:
        get_epochs_fromfile_flist_args['bad_chnls'] = None

    epochs_list_global = [list() for tagtype in tags_function_list] #lista epok dla wszystkich analizowanych plików

    for fname in filelist:
        if '.etr.' in fname:
            continue
        print "Lista plików! Plik:", fname
        ds=fname[:-4]
        get_epochs_fromfile_flist_args['ds'] = ds
        epochs_list_local = get_epochs_fromfile_flist(**get_epochs_fromfile_flist_args) #lista epok dla bieżącego pliku
        # z każdego pliku są listy dla dażdej filtrującej funkcji
        # brudne zapisujemy zaraz do pliku, czyste do jednej globalnej listy
        dirty_epochs_list = []
        with np.errstate(divide='raise', invalid='raise', over='raise'):
            try:
                if use_autoreject: ### ??? ###
                    if autoreject_all: ### ??? ###
                        for nreps, eps in enumerate(epochs_list_local):
                            epochs_list_global[nreps].extend(eps)
                    else:
                        clean_epochs_list = artifacts_rejection(epochs_list_local, get_epochs_fromfile_flist_args, clean_epochs_list, interactive, suffix, fname, dirty_epochs_list, autoreject_cache)
                else:
                    for nr, epochs in enumerate(epochs_list_local):
                        #~ print epochs
                        clean, dirty = stag_amp_grad_art_rej(epochs, rej_amp, rej_grad)
                        if not clean:
                            print 'Warning, no clean epochs for defined groups'
                        clean_epochs_list[nr].extend(clean) # clean are different classes
                        dirty_epochs_list.extend(dirty) # dirty are for whole file
                    try:
                        os.mkdir(OUTDIR)
                    except Exception:
                        pass

                #zapisanie "brudnych" odcinków do pliku
                savefiletag = os.path.join(OUTDIR, os.path.basename(fname) + suffix + '_bad_epochs.tag')
                savetags(dirty_epochs_list, savefiletag,
                         get_epochs_fromfile_flist_args['start_offset'],
                         get_epochs_fromfile_flist_args['duration'])

            except Exception as e:
                print(traceback.format_exc())
                print e
                continue
    if use_autoreject and autoreject_all: # hash calculating and storing
        h = str(hex(hash(
                          str(filelist)
                        + str(get_epochs_fromfile_flist_args['montage'])
                        + str(get_epochs_fromfile_flist_args['filters'])
                        )
                    )
                )[-8:]
        fname_new = '_'.join(fname.split('_')[:4])+'_{}_'.format(h)
        clean_epochs_list = artifacts_rejection(epochs_list_global, get_epochs_fromfile_flist_args, clean_epochs_list, interactive, suffix, fname_new+"_ALL_", dirty_epochs_list, autoreject_cache)
    print 'Found clean epochs in defined groups:', [len(i) for i in clean_epochs_list]

    return clean_epochs_list




#tag filters for artifacts:
def outlier_tag_filter(tag):
    try: return tag['name']=='outlier'
    except: return False
def slope_tag_filter(tag):
    try: return tag['name']=='slope'
    except: return False
def slow_tag_filter(tag):
    try: return tag['name']=='slow'
    except: return False
def muscle_tag_filter(tag):
    try: return tag['name']=='muscle'
    except: return False


def stag_amp_grad_art_rej(stags, amp_thr = 200, grad_thr = 50):
    '''Do artifact rejection on smart tags,
    returns clean tags and dirty tags,
    rejected by amplitude threshold and difference between samples treshold
    amplutude is provided in microVolts'''
    clean_stags = []
    dirty_stags = []

    for stag in stags:
        samples = stag.get_samples()
        #~ print 'DIRTY?', np.max(np.abs(samples))
        diff = np.diff(samples, axis=0)
        if amp_thr>np.max(np.abs(samples)) and grad_thr>np.max(np.abs(diff)):
            clean_stags.append(stag)
        else:
            dirty_stags.append(stag)
    return clean_stags, dirty_stags

def standard_preprocessing(ds,
                           gains=True,
                           drop_chnls=[ u'AmpSaw', u'DriverSaw', u'trig1', u'trig2', u'Driver_Saw'],
                           montage=None,
                           filters=[]):
    eeg_rm = read_manager.ReadManager(ds+'.xml', ds+'.raw', ds+'.tag')
    if gains:
        eeg_rm.set_samples(get_microvolt_samples(eeg_rm),  eeg_rm.get_param('channels_names'))
    eeg_rm = exclude_channels(eeg_rm, drop_chnls)
    for filter in filters:
        eeg_rm = mgr_filter(eeg_rm, filter[0], filter[1],filter[2],
                            filter[3], ftype=filter[4], use_filtfilt=True)
    if montage:
        if montage[0] == 'ears':
            eeg_rm = montage_ears(eeg_rm, montage[1], montage[2])
        elif montage[0] == 'csa':
            eeg_rm = montage_csa(eeg_rm)
        elif montage[0] == 'custom':
            eeg_rm = montage_custom(eeg_rm, montage[1:])
        else:
            raise Exception('Unknown montage')
    return eeg_rm




def evoked_pair_plot_smart_tags(tags1, tags2, chnames=['O1', 'O2', 'Pz', 'PO7', 'PO8', 'PO3', 'PO4', 'Cz',],
                                start_offset=-0.1, labels=['target', 'nontarget'], show= True):
    '''debug evoked potential plot,
     pairwise comparison of 2 smarttag lists,
     blocks thread
     Args:
        chnames: channels to plot
        start_offset: baseline in seconds'''
    ev1, std1 = evoked_from_smart_tags(tags1, chnames, start_offset)
    ev2, std2 = evoked_from_smart_tags(tags2, chnames, start_offset)
    Fs = float(tags1[0].get_param('sampling_frequency'))
    time1 = np.linspace(0+start_offset, ev1.shape[1]/Fs+start_offset, ev1.shape[1])
    time2 = np.linspace(0+start_offset, ev2.shape[1]/Fs+start_offset, ev2.shape[1])
    fig = pb.figure()
    for nr, i in enumerate(chnames):
        ax = fig.add_subplot( (len(chnames)+1)/2, 2, nr+1)

        ax.plot(time1, ev1[nr], 'r',label = labels[0]+' N:{}'.format(len(tags1)))
        ax.fill_between(time1, ev1[nr]-std1[nr], ev1[nr]+std1[nr],
                            color = 'red', alpha=0.3, )
        ax.plot(time2, ev2[nr], 'b', label = labels[1]+' N:{}'.format(len(tags2)))
        ax.fill_between(time2, ev2[nr]-std2[nr], ev2[nr]+std2[nr],
                        color = 'blue', alpha=0.3)

        ax.set_title(i)
    ax.legend()

    if show:
        pb.show()
    return fig

def evoked_differential_plot_smart_tags(tags1, tags2, chnames=['O1', 'O2', 'Pz', 'PO7', 'PO8', 'PO3', 'PO4', 'Cz',],
                                start_offset=-0.1, labels=['target', 'nontarget'], show= True, size=[10,10]):
    '''debug evoked potential plot,
     pairwise diferential plot of 2 smarttag lists,
     blocks thread
     Args:
        chnames: channels to plot
        start_offset: baseline in seconds'''
    ev1, std1 = evoked_from_smart_tags(tags1, chnames, start_offset)
    ev2, std2 = evoked_from_smart_tags(tags2, chnames, start_offset)
    Fs = float(tags1[0].get_param('sampling_frequency'))
    time1 = np.linspace(0+start_offset, ev1.shape[1]/Fs+start_offset, ev1.shape[1])
    time2 = np.linspace(0+start_offset, ev2.shape[1]/Fs+start_offset, ev2.shape[1])
    fig = pb.figure(figsize=size)
    for nr, i in enumerate(chnames):
        ax = fig.add_subplot( (len(chnames)+1)/2, 2, nr+1)

        ax.plot(time1, ev1[nr]-ev2[nr], 'r',label = '{}-{} N:{}'.format(labels[0], labels[1], len(tags1)))
        std = np.sqrt(std1[nr]**2+std2[nr]**2)
        ax.fill_between(time1, ev1[nr]-ev2[nr]-std, ev1[nr]-ev2[nr]+std,
                            color = 'red', alpha=0.3, )
        ax.axhline(0, color='b')
        ax.axvline(0, color='b')
        ax.set_title(i)
    ax.legend()

    if show:
        pb.show()
    return fig



def leave_channels_array(arr, channels, available):
    '''
    :param arr: - 2d array of eeg data channels x samples
    :param channels: - channels to leave,
    :param available: - channel names in data
    '''
    exclude = list(set(available).difference(set(channels)))
    new_samples, new_channels = exclude_channels_array(
                                                arr, exclude, available)
    return new_samples, new_channels

def exclude_channels_array(arr, channels, available):
    '''arr - 2d array of eeg data channels x samples
    channels - channels to exclude,
    available - channel names in data'''
    exclude = set(channels)
    channels = list(set(available).intersection(exclude))

    ex_channels_inds = [available.index(ch) for ch in channels]
    assert(-1 not in ex_channels_inds)
    new_samples = np.zeros((len(available) - len(channels),
                         len(arr[0])))


    new_ind = 0
    new_channels = []
    for ch_ind, ch in enumerate(arr):
        if ch_ind in ex_channels_inds:
            continue
        else:
            new_samples[new_ind, :] = ch
            new_channels.append(available[ch_ind])

            new_ind += 1
    return new_samples, new_channels


def filter(mgr, wp, ws, gpass, gstop, analog=0, ftype='ellip', output='ba', unit='radians', use_filtfilt=False):
    if unit == 'radians':
        b,a = signal.iirdesign(wp, ws, gpass, gstop, analog, ftype, output)
    elif unit == 'hz':
        sampling = float(mgr.get_param('sampling_frequency'))
        try:
            wp = wp/sampling
            ws = ws/sampling
        except TypeError:
            wp = [i/sampling for i in wp]
            ws = [i/sampling for i in ws]

        b,a = signal.iirdesign(wp, ws, gpass, gstop, analog, ftype, output)
    if use_filtfilt:
        #samples_source = read_data_source.MemoryDataSource(mgr.get_samples(), False)
        for i in range(int(mgr.get_param('number_of_channels'))):
            print("FILT FILT CHANNEL "+str(i))
            mgr.get_samples()[i,:] = signal.filtfilt(b, a, mgr.get_samples()[i])
        samples_source = read_data_source.MemoryDataSource(mgr.get_samples(), False)
    else:
        print("FILTER CHANNELs")
        filtered = signal.lfilter(b, a, mgr.get_samples())
        print("FILTER CHANNELs finished")
        samples_source = read_data_source.MemoryDataSource(filtered, True)

    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    new_mgr = read_manager.ReadManager(info_source, samples_source, tags_source)
    return new_mgr

def normalize(mgr, norm):
    if norm == 0:
        return mgr
    new_mgr = deepcopy(mgr)
    for i in range(len(new_mgr.get_samples())):
        n = linalg.norm(new_mgr.get_samples()[i, :], norm)
        new_mgr.get_samples()[i, :] /= n
    return new_mgr


def downsample(mgr, factor):
    assert(factor >= 1)


    ch_num = len(mgr.get_samples())
    ch_len = len(mgr.get_samples()[0])

    # To be determined ...
    ret_ch_len = 0
    i = 0
    left_inds = []

    # Determine ret_ch_len - a size of returned channel
    while i < ch_len:
        left_inds.append(i)
        ret_ch_len += 1
        i += factor

    new_samples = np.array([np.zeros(ret_ch_len) for i in range(ch_num)])
    for i in range(ch_num):
        for j, ind in enumerate(left_inds):
            new_samples[i, j] = mgr.get_samples()[i, ind]


    info_source = deepcopy(mgr.info_source)
    info_source.get_params()['number_of_samples'] = str(ret_ch_len*ch_num)
    info_source.get_params()['sampling_frequency'] = str(float(mgr.get_param('sampling_frequency'))/factor)

    tags_source = deepcopy(mgr.tags_source)
    samples_source = read_data_source.MemoryDataSource(new_samples)
    return read_manager.ReadManager(info_source, samples_source, tags_source)

def montage(mgr, montage_type, **montage_params):
    if montage_type == 'common_spatial_average':
        return montage_csa(mgr, **montage_params)
    elif montage_type == 'ears':
        return montage_ears(mgr, **montage_params)
    elif montage_type == 'custom':
        return montage_custom(mgr, **montage_params)
    elif montage_type == 'no_montage':
        return mgr
    else:
        raise Exception("Montage - unknown montaget type: "+str(montage_type))

def montage_custom_matrix(mgr, montage_matrix):
    new_samples = get_montage(mgr.get_samples(),
                              montage_matrix)
    info_source = deepcopy(mgr.info_source)
    tags_source = deepcopy(mgr.tags_source)
    samples_source = read_data_source.MemoryDataSource(new_samples)
    return read_manager.ReadManager(info_source, samples_source, tags_source)

def get_channel_indexes(channels, toindex):
    '''get list of indexes of channels in toindex list as found in
    channels list'''
    indexes = []
    for chnl in toindex:
        index = channels.index(chnl)
        if index<0:
            raise Exception("Montage - couldn`t channel: "+str(chnl))
        else:
            indexes.append(index)
    return indexes



def add_events_to_rm(rm, events, name = 'Blink'):
    tags= rm.get_tags()
    Fs = rm.get_param('sampling_frequency')
    #for event in events:
    #    tags.append(tag_utils.pack_tag_to_dict(event[0]/Fs, event[0]/Fs, name, {}, ''))
    #rm.set_tags(tags)
    #rm.tags_source = rm.tags_source._memory_source # obci bug workaround
    return rm
    
    
def savetags_from_rm(rm, OUTPUT, postfix=''):
    basename = os.path.splitext(os.path.basename(rm.tags_source._tags_proxy._tags_file_name))[0]
    tags = rm.get_tags()
    tf = TagsFileWriter(os.path.join(OUTPUT, basename+'_{}.obci.tag'.format(postfix)))
    for tag in tags:
        tf.tag_received(tag)
    tf.finish_saving(0)
