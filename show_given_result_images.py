#!/usr/bin/python
# coding: utf8

# ten skrypt przeszukuje podane katalogi i prezentuje kolejne obrazy png zawierające podany ciąg znaków w nazwie

import os
import sys

import matplotlib.pyplot as plt
import matplotlib.image as mpimg


name_to_show = "ICA_"  # poszukiwany fragment nazwy

if len(sys.argv) == 1:
    print "Podaj scieżkę do katalogu zawierającego pliki *.png"
else:
    for catalog in sys.argv[1:]:
        for root, dirs, files in os.walk(catalog):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
                if ".png" in filename and name_to_show in filename:  # szukając plików .png zawierajcyh poszukiwany fragment nazwy
                    path = os.path.join(root, filename)  # ścieżka do pliku png
                    
                    print path
                    img = mpimg.imread(path)
                    imgplot = plt.imshow(img)

                    mng = plt.get_current_fig_manager()
                    mng.resize(1200, 675)
                    plt.show()
